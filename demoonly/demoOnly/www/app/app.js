'use strict';

var app = angular.module('appTwoFour', ['ngRoute']);

//Constant value declaration section
app.constant("cons",{
  "url":"",
  "port":"",
  "pagepath":"app/pages/"
});

//Module configuration includes routing of pages
app.config(['$routeProvider','cons',
  function($routeProvider,cons) {
    $routeProvider.
      when('/login', {
        templateUrl: cons.pagepath+'login.tpl.html'
      }).when('/createaccount', {
        templateUrl: cons.pagepath+'createaccount.tpl.html'
      }).when('/accountinfo', {
        templateUrl: cons.pagepath+'accountinfo.tpl.html'
      }).when('/contactus', {
        templateUrl: cons.pagepath+'contactus.tpl.html'
      }).when('/choosepayment', {
        templateUrl: cons.pagepath+'choosepayment.tpl.html'
      }).when('/confirmsavedpayment', {
        templateUrl: cons.pagepath+'confirmsavedpayment.tpl.html'
      }).when('/homeworkhelp', {
        templateUrl: cons.pagepath+'homeworkhelp.tpl.html'
      }).when('/onlinetutoring', {
        templateUrl: cons.pagepath+'onlinetutoring.tpl.html'
      }).when('/cardinfo', {
        templateUrl: cons.pagepath+'cardinfo.tpl.html'
      }).when('/requesttutor', {
        templateUrl: cons.pagepath+'requesttutor.tpl.html'
      }).when('/requeststudent', {
        templateUrl: cons.pagepath+'requeststudent.tpl.html'
      }).when('/reqcontentstudent', {
        templateUrl: cons.pagepath+'reqcontentstudent.tpl.html'
      }).when('/reqcontenttutor', {
        templateUrl: cons.pagepath+'reqcontenttutor.tpl.html'
      }).
      otherwise({
        redirectTo: '/login'
      });
  }
]);

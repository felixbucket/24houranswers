app.factory('apiRequest', ['$http', 'cons', 'userAcct', function($http, cons, userAcct){
	return function(payload){
		payload = angular.copy(payload);
		payload.url = cons.apibase +'/apirequest'+ payload.url;

		console.info( payload.method, payload.url, payload.params, payload.data );

		// todo: remove this when basic auth is fixed from api
		payload.url = payload.url.replace('http://', 'http://'+ userAcct.username() +':'+ userAcct.password() +'@');

		return $http(payload)
			.then(function(res){
				console.info( res.status, res.config.url, res.data );
				return res;
			}, function(res){
				console.error( res.status, res.config.url, res.data );
				res.data = res.data || {error: {code: res.status, message: res.statusText}}
				throw res;
			});
	}
}]);
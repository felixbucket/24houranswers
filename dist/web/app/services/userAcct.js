/* User Account Service

to check for user credentials:
	function(userAcct){
		console.log( userAcct.isValid() );
		console.log( userAcct.isStudent() );
		console.log( userAcct.isTutor() );
		console.log( userAcct.username() );
		console.log( userAcct.usertype() );
	}

to set login credentials:
	function(userAcct){
		userAcct().login('john', 'student');
	}
*/
app.factory('userAcct', ['$window', function($window){
	var loc = (function(){
		var s = $window.localStorage.getItem('user');
		return s ? JSON.parse(s) : null;
	})() || {
		username: null,
		type: null
	};


	function svc(){
		return {
			login: function( username, type, password ){//for now
				loc.username = username;
				loc.type = type;
				loc.password = password;//temp only
				try{
					$window.localStorage.setItem('user', JSON.stringify(loc));
				}catch(e){};
			},
			clear: function(){
				loc.username = null;
				loc.type = null;
			}
		};
	}
	svc.isValid = function(){
		return !! loc.username;
	};
	svc.isStudent = function(){
		return loc.type === 'student';
	};
	svc.isTutor = function(){
		return loc.type === 'tutor';
	};
	svc.isAdmin = function(){
		return loc.type === 'admin';
	};
	svc.username = function(){
		return loc.username;
	};
	svc.password = function(){
		return loc.password;
	};
	svc.usertype = function(){
		return loc.type;
	};

	return svc;
}])

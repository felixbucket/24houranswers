app.factory('getSubjects', function($http,cons){
    return {
      list : function(){
        return $http({
          method: 'GET',
          url: 	cons.apibase +'/apisubjects.json/'
        });
      }
    }
});

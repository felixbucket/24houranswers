'use strict';

var app = angular.module('appTwoFour', ['ngRoute', 'angularMoment']);

//Constant value declaration section
app.constant("cons",{
	"url":"",
	"port":"",
	"pagepath":"app/pages/",
	"apibase": "http://ryanec2.dev.24houranswers.com/api",
	"gradeLevel" : ['Graduate Student','College Undergraduate','High School','Junior High','Elementary'],
	"lengthSession" : ['30 minutes','45 minutes','1 hour','2 hours','3 hours','4 hours','5 hours']
});

app.constant('jquery', window.jQuery);
//app.constant('GradeLevel',['Graduate Student','College Undergraduate','High School','Junior High','Elementary']);

app.run(['moment', function(moment){
	moment.defaultFormat = 'MM/DD/YYYY h:ssA z';
	moment.tz.setDefault('EST');
}]);

//Module configuration includes routing of pages
app.config(['$routeProvider','cons',
	function($routeProvider,cons) {
		$routeProvider
			.when('/login', {
				templateUrl: cons.pagepath+'login.tpl.html',
				controller: 'loginCtrl',
				controllerAs: 'vm',
				resolve: {
					/*auth: ['$location', 'userAcct', function($location, userAcct){
						if ( userAcct.isValid() ) {
							if ( userAcct.isStudent() )
								$location.path('/requeststudent');
							else if ( userAcct.isTutor() )
								$location.path('/requesttutor');
						}
					}]*/
				}
			})
			.when('/createaccount', {
				templateUrl: cons.pagepath+'createaccount.tpl.html'
			})
			.when('/accountinfo', {
				templateUrl: cons.pagepath+'accountinfo.tpl.html'
			})
			.when('/contactus', {
				templateUrl: cons.pagepath+'contactus.tpl.html'
			})
			.when('/choosepayment', {
				templateUrl: cons.pagepath+'choosepayment.tpl.html'
			})
			.when('/confirmsavedpayment', {
				templateUrl: cons.pagepath+'confirmsavedpayment.tpl.html'
			})
			.when('/homeworkhelp', {
				templateUrl: cons.pagepath+'homeworkhelp.tpl.html',
				controller : "hwHelpCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/onlinetutoring', {
				templateUrl: cons.pagepath+'onlinetutoring.tpl.html',
				controller : "oltCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/cardinfo', {
				templateUrl: cons.pagepath+'cardinfo.tpl.html'
			})
			.when('/requesttutor', {
				templateUrl: cons.pagepath+'requesttutor.tpl.html',
				controller: 'requestsTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontenttutor/:ids', {
				templateUrl: cons.pagepath+'reqcontenttutor.tpl.html',
				controller: 'reqContentTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/requeststudent', {
				templateUrl: cons.pagepath+'requeststudent.tpl.html',
				controller: 'requestsStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontentstudent/:ids', {
				templateUrl: cons.pagepath+'reqcontentstudent.tpl.html',
				controller: 'reqContentStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.otherwise({
				redirectTo: '/login'
			});

		requireAuth.$inject = ['$location', 'userAcct'];
		function requireAuth($location, userAcct){
			if (!userAcct.isValid() )
				$location.path('/login');
		}
	}
]);

app.run(['$rootScope', function($rootScope){
	$rootScope.$on('$routeChangeStart', function(e, next, cur){
		console.info( 'routeChangeStart', cur && cur.$$route && cur.$$route.originalPath, '->', next && next.$$route && next.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeSuccess', function(e, cur, prev){
		console.info( 'routeChangeSuccess', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeError', function(e, cur, prev, reject){
		console.error( 'routeChangeError', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath, reject );
	});
}])

'use strict';

var app = angular.module('appTwoFour', ['ngRoute', 'angularMoment']);

//Constant value declaration section
app.constant("cons",{
	"url":"",
	"port":"",
	"pagepath":"app/pages/",
	"apibase": "http://ryanec2.dev.24houranswers.com/api",
	"gradeLevel" : ['Graduate Student','College Undergraduate','High School','Junior High','Elementary'],
	"lengthSession" : ['30 minutes','45 minutes','1 hour','2 hours','3 hours','4 hours','5 hours']
});

app.constant('jquery', window.jQuery);
//app.constant('GradeLevel',['Graduate Student','College Undergraduate','High School','Junior High','Elementary']);

app.run(['moment', function(moment){
	moment.defaultFormat = 'MM/DD/YYYY h:ssA z';
	moment.tz.setDefault('EST');
}]);

//Module configuration includes routing of pages
app.config(['$routeProvider','cons',
	function($routeProvider,cons) {
		$routeProvider
			.when('/login', {
				templateUrl: cons.pagepath+'login.tpl.html',
				controller: 'loginCtrl',
				controllerAs: 'vm',
				resolve: {
					/*auth: ['$location', 'userAcct', function($location, userAcct){
						if ( userAcct.isValid() ) {
							if ( userAcct.isStudent() )
								$location.path('/requeststudent');
							else if ( userAcct.isTutor() )
								$location.path('/requesttutor');
						}
					}]*/
				}
			})
			.when('/createaccount', {
				templateUrl: cons.pagepath+'createaccount.tpl.html'
			})
			.when('/accountinfo', {
				templateUrl: cons.pagepath+'accountinfo.tpl.html'
			})
			.when('/contactus', {
				templateUrl: cons.pagepath+'contactus.tpl.html'
			})
			.when('/choosepayment', {
				templateUrl: cons.pagepath+'choosepayment.tpl.html'
			})
			.when('/confirmsavedpayment', {
				templateUrl: cons.pagepath+'confirmsavedpayment.tpl.html'
			})
			.when('/homeworkhelp', {
				templateUrl: cons.pagepath+'homeworkhelp.tpl.html',
				controller : "hwHelpCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/onlinetutoring', {
				templateUrl: cons.pagepath+'onlinetutoring.tpl.html',
				controller : "oltCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/cardinfo', {
				templateUrl: cons.pagepath+'cardinfo.tpl.html'
			})
			.when('/requesttutor', {
				templateUrl: cons.pagepath+'requesttutor.tpl.html',
				controller: 'requestsTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontenttutor/:ids', {
				templateUrl: cons.pagepath+'reqcontenttutor.tpl.html',
				controller: 'reqContentTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/requeststudent', {
				templateUrl: cons.pagepath+'requeststudent.tpl.html',
				controller: 'requestsStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontentstudent/:ids', {
				templateUrl: cons.pagepath+'reqcontentstudent.tpl.html',
				controller: 'reqContentStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.otherwise({
				redirectTo: '/login'
			});

		requireAuth.$inject = ['$location', 'userAcct'];
		function requireAuth($location, userAcct){
			if (!userAcct.isValid() )
				$location.path('/login');
		}
	}
]);

app.run(['$rootScope', function($rootScope){
	$rootScope.$on('$routeChangeStart', function(e, next, cur){
		console.info( 'routeChangeStart', cur && cur.$$route && cur.$$route.originalPath, '->', next && next.$$route && next.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeSuccess', function(e, cur, prev){
		console.info( 'routeChangeSuccess', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeError', function(e, cur, prev, reject){
		console.error( 'routeChangeError', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath, reject );
	});
}])

app.controller('hwHelpCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){
    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;

    $scope.hw = {};
    $scope.hw.type = "SR";
    $scope.hw.duedate = "";
    $scope.hw.timezone = jstz().timezone_name;
    $scope.hw.subjectid = "";
    $scope.hw.grade = "";
    $scope.hw.fast = "normal";
    $scope.hw.tutorname = "";
    $scope.hw.discountcode = "";
    $scope.hw.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.hw.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.hw.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendHw = function(){
        console.log($scope.hw);
        /*
        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.hw
    		}).then(function successCallback(response){

            //todo - proper redirection if student or tutor

            if(userAcct.usertype() === "student"){
              $location.path('/requeststudent');
            } else {
              $location.path('/requeststudent');
            }
        });*/
    }

    $("#datepickerHw").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.hw.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerHw").on("dp.change",function(e){
       var selTime =  moment($scope.inittime, ["h:mm A"]).format("HH:mm");
       //$scope.inittime = moment($(this).val(), ["h:mm A"]).format("HH:mm");
       $scope.hw.duedate = $scope.initdate+" "+selTime+":00";
    });


});

app.controller('listCtrl',function($scope){
    $scope.isUsa = false;
    
    $scope.countrySelected = function(countryval){
      if(countryval=='US')$scope.isUsa = true;
      else $scope.isUsa = false;
    };
})

app.controller('loginCtrl', ['$scope', '$http', '$location', 'userAcct', 'cons',
function($scope, $http, $location, userAcct, cons){
	var vm = this,
		form = $scope.form;

	vm.error = null;
	vm.loading = false;


  userAcct().clear();

	vm.submit = function(){
		var data = {
			username: vm.email,
			password: vm.password
		};

		$http({
			method: 'POST',
			url: cons.apibase +'/apilogin.json',
			data: data
		})
			.then(function(res){
				console.info('login response', res);
				var usertype = res.data;
				userAcct().login(data.username, usertype, data.password);
				if ( usertype === 'student' )
					$location.path('/requeststudent');
				else
					$location.path('/requesttutor');
			}, function(res){
				console.warn('login response', res);
				vm.error = 'Invalid username/email or password';
			})
			.finally(function(){
				vm.loading = false;
			});
		vm.error = null;
		vm.loading = true;
	};

}
]);

app.controller('oltCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){

    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;
    $scope.lengthlist = cons.lengthSession;

    $scope.olt = {};
    $scope.olt.type = "LR";
    $scope.olt.duedate = "";
    $scope.olt.timezone = jstz().timezone_name;
    $scope.olt.length = "30 minutes";
    $scope.olt.subjectid = "";
    $scope.olt.grade = "";
    $scope.olt.fast = "normal";
    $scope.olt.tutorname = "";
    $scope.olt.discountcode = "";
    $scope.olt.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.olt.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.olt.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendOlt = function(){
        console.log($scope.olt);
        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.olt
    		}).then(function successCallback(response){
           /*
             todo - proper redirection if student or tutor
           */
           if(userAcct.usertype() === "student"){
             $location.path('/requeststudent');
           } else {
             $location.path('/requeststudent');
           }
        });
    }

    $("#datepickerOlt").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.olt.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerOlt").on("dp.change",function(e){
       $scope.inittime = moment($(this).val(), ["h:mm A"]).format("HH:mm");
       $scope.olt.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });


});

app.controller('reqContentStudentCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: ids.substr(0, 2)},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		apiRequest({
				method: 'PUT',
				url: '/archive.json',
				data: {type: reqType, requestid: reqID}
			}).then(function(res){
				vm.refresh();
			});
	};


	function normalize(data){
		data = angular.copy(data);
		if ( data.request.tabletype === 'solutionrequests' )
			data.request.ids = 'SR'+ data.request.id;
		else if ( data.request.tabletype === 'liverequests' )
			data.request.ids = 'LR'+ data.request.id;

		data.request.date = moment(data.request.date);
		data.request.dateArchived = moment(data.request.dateArchived);
		data.request.dateAssigned = moment(data.request.dateAssigned);
		data.request.dateCanceled = moment(data.request.dateCanceled);
		data.request.dateCompleted = moment(data.request.dateCompleted);
		data.request.dueDate = moment(data.request.dueDate);
		data.request.lastStudentPost = moment(data.request.lastStudentPost);
		data.request.lastTutorPost = moment(data.request.lastTutorPost);

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		data.request.additionalInfo = $sce.trustAsHtml(data.request.additionalInfo);

		angular.forEach(data.posts, function(d, i){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);

app.controller('reqContentTutorCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;
	vm.error = null;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	vm.isPendingQuote = false;
	vm.isPendingAuth = false;

	vm.dueDateDiff = function(){
		if ( vm.isLoading ) return '';
		if ( vm.data.request.dueDate.isAfter() )
			return '0 days 0 hours 0 minutes';

		var dur = moment.duration( moment().diff(vm.data.request.dueDate) );
		return dur.days() +' days '+ dur.hours() +' hours '+ dur.minutes() +' minutes';
	};


	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: reqType},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/archive.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};
	vm.releaseRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/release.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};

	vm.quote = {
		value: 0,
		visible: false,
		submit: function(){
			if ( ! /^[0-9]+$/.test(vm.quote.value) )
				return angular.element('[ng-model="vm.quote.value"]').focus() && 0;

			apiRequest({
					method: 'PUT',
					url: '/quote.json',
					data: {type: reqType, requestid: reqID, quote: parseInt(vm.quote.value)}
				}).then(function(res){
					vm.refresh();
				});
		}
	};


	function normalize(data){
		var req = data.request;
		if ( req.tabletype === 'solutionrequests' )
			req.ids = 'SR'+ req.id;
		else if ( req.tabletype === 'liverequests' )
			req.ids = 'LR'+ req.id;

		req.date = moment(req.date);
		req.dateArchived = moment(req.dateArchived);
		req.dateAssigned = moment(req.dateAssigned);
		req.dateCanceled = moment(req.dateCanceled);
		req.dateCompleted = moment(req.dateCompleted);
		req.dueDate = moment(req.dueDate);
		req.lastStudentPost = moment(req.lastStudentPost);
		req.lastTutorPost = moment(req.lastTutorPost);

		req.quote = parseFloat( req.quote );

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		req.additionalInfo = $sce.trustAsHtml(req.additionalInfo);

		$.each(data.posts, function(i, d){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);
app.controller('requestsStudentCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	vm.fields = {
		'Show all requests': {days_ago: -1, show_archived: false},
		'Show last 15 days': {days_ago: 15, show_archived: false},
		'Show last 60 days': {days_ago: 60, show_archived: false},
		'Show last 90 days': {days_ago: 90, show_archived: false},
		'Show archived requests': {days_ago: -1, show_archived: true}
	};

	var defaultParams = vm.fields['Show last 15 days'];

	var payload = {
		method: 'GET',
		url: '/studentall.json'
	};
	vm.params = defaultParams;

	(vm.refresh = function(){
		payload.params = angular.copy(vm.params);
		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data, function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();

	vm.reset = function(form){
		vm.params = defaultParams;
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm.reset.$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		return data;
	}
}
]);

app.controller('requestsTutorCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	var defaultParams = {
		order_by: 'dueDate',
		order_by_direction: 'DESC',
		pipeline: 'no-pipeline',
		table: 'all',
		status: 'active',
		archived: 'unarchived',
		acct: -1,
		keyword: ''
	};

	var payload = {
		method: 'GET',
		url: '/tutorall.json',
		params: vm.params = angular.copy(defaultParams)
	};

	(vm.refresh = function(){
		payload.params.keyword = payload.params.keyword.trim();

		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data.requests || [], function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();


	vm.pipeline = false;
	vm.togglePipeline = function(){
		vm.pipeline = !vm.pipeline;
		vm.params.pipeline = vm.pipeline ? 'pipeline-only' : 'no-pipeline';
		vm.refresh();
	};
	vm.sort = {
		order_by: {
			'time_unattended': 'Time Unattended',
			'date': 'Date',
			'dueDate': 'Due Date'
		},
		order_by_direction: {
			'DESC': 'Descending',
			'ASC': 'Ascending'
		}
	};
	vm.filter = {
		status: {
			'active': 'Active',
			'Pending Quote': 'Pending Quote',
			'Pending Authorization': 'Pending Authorization',
			'Pending Completion': 'Pending Completion',
			'Completed': 'Completed',
			'Canceled': 'Canceled'
		},
		table: {
			'all': 'All',
			'solutionrequests': 'Solutions Requests',
			'liverequests': 'Live Requests'
		},
		archived: {
			'all': 'Archived & Unarchived',
			'archived': 'Archived',
			'unarchived': 'Unarchived',
		}
	};

	vm.sortReset = function(form){
		for( var k in vm.sort )
			if ( vm.sort.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.filterReset = function(form){
		for( var k in vm.filter )
			if ( vm.filter.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm[form.$name +'Reset'].$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		data.unattended = Math.ceil(data.time_unattended / 60 / 24);
		if ( data.unattended > 1 )
			data.unattended += ' days';
		else
			data.unattended += ' day';
		return data;
	}
}
]);

app.controller('zoneCtrl', function($scope) {
   $scope.currentTZ = moment().format('Z');
 });

app.directive('addPost', ['apiRequest', function(apiRequest){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/addPost.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-add-post',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'addPost',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'POST',
					url: '/addpost.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.file = undefined;
				vm.comment = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.comment.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.comment = vm.comment.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.addPostSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);
app.directive('cancelRequest', ['apiRequest', 'cons', function(apiRequest, cons){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/cancelRequestReason.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-cancel-request',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'cancelRequest',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'PUT',
					url: '/cancel.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.reason = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.reason.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.reason = vm.reason.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.cancelRequestSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);

'use strict';

app.directive('headernav', function (cons) {
  console.log(cons);

    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        templateUrl: cons.pagepath+"headernav.tpl.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});

app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel);

                //get the value of the other password
                var e2 = scope.$eval(attrs.passwordMatch);
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {
                //set the form control to valid if both
                //passwords are the same, else invalid
                control.$setValidity("pwmatch", n);
            });
        }
    };
}]);

app.factory('apiRequest', ['$http', 'cons', 'userAcct', function($http, cons, userAcct){
	return function(payload){
		payload = angular.copy(payload);
		payload.url = cons.apibase +'/apirequest'+ payload.url;

		console.info( payload.method, payload.url, payload.params, payload.data );

		// todo: remove this when basic auth is fixed from api
		payload.url = payload.url.replace('http://', 'http://'+ userAcct.username() +':'+ userAcct.password() +'@');

		return $http(payload)
			.then(function(res){
				console.info( res.status, res.config.url, res.data );
				return res;
			}, function(res){
				console.error( res.status, res.config.url, res.data );
				res.data = res.data || {error: {code: res.status, message: res.statusText}}
				throw res;
			});
	}
}]);
app.factory('getSubjects', function($http,cons){
    return {
      list : function(){
        return $http({
          method: 'GET',
          url: 	cons.apibase +'/apisubjects.json/'
        });
      }
    }
});

/* User Account Service

to check for user credentials:
	function(userAcct){
		console.log( userAcct.isValid() );
		console.log( userAcct.isStudent() );
		console.log( userAcct.isTutor() );
		console.log( userAcct.username() );
		console.log( userAcct.usertype() );
	}

to set login credentials:
	function(userAcct){
		userAcct().login('john', 'student');
	}
*/
app.factory('userAcct', ['$window', function($window){
	var loc = (function(){
		var s = $window.localStorage.getItem('user');
		return s ? JSON.parse(s) : null;
	})() || {
		username: null,
		type: null
	};


	function svc(){
		return {
			login: function( username, type, password ){//for now
				loc.username = username;
				loc.type = type;
				loc.password = password;//temp only
				try{
					$window.localStorage.setItem('user', JSON.stringify(loc));
				}catch(e){};
			},
			clear: function(){
				loc.username = null;
				loc.type = null;
			}
		};
	}
	svc.isValid = function(){
		return !! loc.username;
	};
	svc.isStudent = function(){
		return loc.type === 'student';
	};
	svc.isTutor = function(){
		return loc.type === 'tutor';
	};
	svc.isAdmin = function(){
		return loc.type === 'admin';
	};
	svc.username = function(){
		return loc.username;
	};
	svc.password = function(){
		return loc.password;
	};
	svc.usertype = function(){
		return loc.type;
	};

	return svc;
}])

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImNvbnRyb2xsZXJzL2h3SGVscEN0cmwuanMiLCJjb250cm9sbGVycy9saXN0Q3RybC5qcyIsImNvbnRyb2xsZXJzL2xvZ2luQ3RybC5qcyIsImNvbnRyb2xsZXJzL29sdEN0cmwuanMiLCJjb250cm9sbGVycy9yZXFDb250ZW50U3R1ZGVudEN0cmwuanMiLCJjb250cm9sbGVycy9yZXFDb250ZW50VHV0b3JDdHJsLmpzIiwiY29udHJvbGxlcnMvcmVxdWVzdHNTdHVkZW50Q3RybC5qcyIsImNvbnRyb2xsZXJzL3JlcXVlc3RzVHV0b3JDdHJsLmpzIiwiY29udHJvbGxlcnMvem9uZUN0cmwuanMiLCJkaXJlY3RpdmVzL2FkZFBvc3QuanMiLCJkaXJlY3RpdmVzL2NhbmNlbFJlcXVlc3QuanMiLCJkaXJlY3RpdmVzL2hlYWRlcm5hdi5qcyIsImRpcmVjdGl2ZXMvcGFzc3dvcmRNYXRjaC5qcyIsInNlcnZpY2VzL2FwaVJlcXVlc3QuanMiLCJzZXJ2aWNlcy9nZXRTdWJqZWN0cy5qcyIsInNlcnZpY2VzL3VzZXJBY2N0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3hFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDN0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDeEhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDckVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1SEE7QUFDQTtBQUNBO0FBQ0E7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic3JjL2FwcC9hcHAtY29tcGxldGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnYXBwVHdvRm91cicsIFsnbmdSb3V0ZScsICdhbmd1bGFyTW9tZW50J10pO1xuXG4vL0NvbnN0YW50IHZhbHVlIGRlY2xhcmF0aW9uIHNlY3Rpb25cbmFwcC5jb25zdGFudChcImNvbnNcIix7XG5cdFwidXJsXCI6XCJcIixcblx0XCJwb3J0XCI6XCJcIixcblx0XCJwYWdlcGF0aFwiOlwiYXBwL3BhZ2VzL1wiLFxuXHRcImFwaWJhc2VcIjogXCJodHRwOi8vcnlhbmVjMi5kZXYuMjRob3VyYW5zd2Vycy5jb20vYXBpXCIsXG5cdFwiZ3JhZGVMZXZlbFwiIDogWydHcmFkdWF0ZSBTdHVkZW50JywnQ29sbGVnZSBVbmRlcmdyYWR1YXRlJywnSGlnaCBTY2hvb2wnLCdKdW5pb3IgSGlnaCcsJ0VsZW1lbnRhcnknXSxcblx0XCJsZW5ndGhTZXNzaW9uXCIgOiBbJzMwIG1pbnV0ZXMnLCc0NSBtaW51dGVzJywnMSBob3VyJywnMiBob3VycycsJzMgaG91cnMnLCc0IGhvdXJzJywnNSBob3VycyddXG59KTtcblxuYXBwLmNvbnN0YW50KCdqcXVlcnknLCB3aW5kb3cualF1ZXJ5KTtcbi8vYXBwLmNvbnN0YW50KCdHcmFkZUxldmVsJyxbJ0dyYWR1YXRlIFN0dWRlbnQnLCdDb2xsZWdlIFVuZGVyZ3JhZHVhdGUnLCdIaWdoIFNjaG9vbCcsJ0p1bmlvciBIaWdoJywnRWxlbWVudGFyeSddKTtcblxuYXBwLnJ1bihbJ21vbWVudCcsIGZ1bmN0aW9uKG1vbWVudCl7XG5cdG1vbWVudC5kZWZhdWx0Rm9ybWF0ID0gJ01NL0REL1lZWVkgaDpzc0Egeic7XG5cdG1vbWVudC50ei5zZXREZWZhdWx0KCdFU1QnKTtcbn1dKTtcblxuLy9Nb2R1bGUgY29uZmlndXJhdGlvbiBpbmNsdWRlcyByb3V0aW5nIG9mIHBhZ2VzXG5hcHAuY29uZmlnKFsnJHJvdXRlUHJvdmlkZXInLCdjb25zJyxcblx0ZnVuY3Rpb24oJHJvdXRlUHJvdmlkZXIsY29ucykge1xuXHRcdCRyb3V0ZVByb3ZpZGVyXG5cdFx0XHQud2hlbignL2xvZ2luJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnbG9naW4udHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAnbG9naW5DdHJsJyxcblx0XHRcdFx0Y29udHJvbGxlckFzOiAndm0nLFxuXHRcdFx0XHRyZXNvbHZlOiB7XG5cdFx0XHRcdFx0LyphdXRoOiBbJyRsb2NhdGlvbicsICd1c2VyQWNjdCcsIGZ1bmN0aW9uKCRsb2NhdGlvbiwgdXNlckFjY3Qpe1xuXHRcdFx0XHRcdFx0aWYgKCB1c2VyQWNjdC5pc1ZhbGlkKCkgKSB7XG5cdFx0XHRcdFx0XHRcdGlmICggdXNlckFjY3QuaXNTdHVkZW50KCkgKVxuXHRcdFx0XHRcdFx0XHRcdCRsb2NhdGlvbi5wYXRoKCcvcmVxdWVzdHN0dWRlbnQnKTtcblx0XHRcdFx0XHRcdFx0ZWxzZSBpZiAoIHVzZXJBY2N0LmlzVHV0b3IoKSApXG5cdFx0XHRcdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0dHV0b3InKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XSovXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NyZWF0ZWFjY291bnQnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydjcmVhdGVhY2NvdW50LnRwbC5odG1sJ1xuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvYWNjb3VudGluZm8nLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydhY2NvdW50aW5mby50cGwuaHRtbCdcblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NvbnRhY3R1cycsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ2NvbnRhY3R1cy50cGwuaHRtbCdcblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2Nob29zZXBheW1lbnQnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydjaG9vc2VwYXltZW50LnRwbC5odG1sJ1xuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvY29uZmlybXNhdmVkcGF5bWVudCcsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ2NvbmZpcm1zYXZlZHBheW1lbnQudHBsLmh0bWwnXG5cdFx0XHR9KVxuXHRcdFx0LndoZW4oJy9ob21ld29ya2hlbHAnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydob21ld29ya2hlbHAudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyIDogXCJod0hlbHBDdHJsXCIsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL29ubGluZXR1dG9yaW5nJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnb25saW5ldHV0b3JpbmcudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyIDogXCJvbHRDdHJsXCIsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NhcmRpbmZvJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnY2FyZGluZm8udHBsLmh0bWwnXG5cdFx0XHR9KVxuXHRcdFx0LndoZW4oJy9yZXF1ZXN0dHV0b3InLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydyZXF1ZXN0dHV0b3IudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAncmVxdWVzdHNUdXRvckN0cmwnLFxuXHRcdFx0XHRjb250cm9sbGVyQXM6ICd2bScsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL3JlcWNvbnRlbnR0dXRvci86aWRzJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsncmVxY29udGVudHR1dG9yLnRwbC5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ3JlcUNvbnRlbnRUdXRvckN0cmwnLFxuXHRcdFx0XHRjb250cm9sbGVyQXM6ICd2bScsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL3JlcXVlc3RzdHVkZW50Jywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsncmVxdWVzdHN0dWRlbnQudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAncmVxdWVzdHNTdHVkZW50Q3RybCcsXG5cdFx0XHRcdGNvbnRyb2xsZXJBczogJ3ZtJyxcblx0XHRcdFx0cmVzb2x2ZToge2F1dGg6IHJlcXVpcmVBdXRofVxuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvcmVxY29udGVudHN0dWRlbnQvOmlkcycsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ3JlcWNvbnRlbnRzdHVkZW50LnRwbC5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ3JlcUNvbnRlbnRTdHVkZW50Q3RybCcsXG5cdFx0XHRcdGNvbnRyb2xsZXJBczogJ3ZtJyxcblx0XHRcdFx0cmVzb2x2ZToge2F1dGg6IHJlcXVpcmVBdXRofVxuXHRcdFx0fSlcblx0XHRcdC5vdGhlcndpc2Uoe1xuXHRcdFx0XHRyZWRpcmVjdFRvOiAnL2xvZ2luJ1xuXHRcdFx0fSk7XG5cblx0XHRyZXF1aXJlQXV0aC4kaW5qZWN0ID0gWyckbG9jYXRpb24nLCAndXNlckFjY3QnXTtcblx0XHRmdW5jdGlvbiByZXF1aXJlQXV0aCgkbG9jYXRpb24sIHVzZXJBY2N0KXtcblx0XHRcdGlmICghdXNlckFjY3QuaXNWYWxpZCgpIClcblx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9sb2dpbicpO1xuXHRcdH1cblx0fVxuXSk7XG5cbmFwcC5ydW4oWyckcm9vdFNjb3BlJywgZnVuY3Rpb24oJHJvb3RTY29wZSl7XG5cdCRyb290U2NvcGUuJG9uKCckcm91dGVDaGFuZ2VTdGFydCcsIGZ1bmN0aW9uKGUsIG5leHQsIGN1cil7XG5cdFx0Y29uc29sZS5pbmZvKCAncm91dGVDaGFuZ2VTdGFydCcsIGN1ciAmJiBjdXIuJCRyb3V0ZSAmJiBjdXIuJCRyb3V0ZS5vcmlnaW5hbFBhdGgsICctPicsIG5leHQgJiYgbmV4dC4kJHJvdXRlICYmIG5leHQuJCRyb3V0ZS5vcmlnaW5hbFBhdGggKTtcblx0fSk7XG5cdCRyb290U2NvcGUuJG9uKCckcm91dGVDaGFuZ2VTdWNjZXNzJywgZnVuY3Rpb24oZSwgY3VyLCBwcmV2KXtcblx0XHRjb25zb2xlLmluZm8oICdyb3V0ZUNoYW5nZVN1Y2Nlc3MnLCBwcmV2ICYmIHByZXYuJCRyb3V0ZSAmJiBwcmV2LiQkcm91dGUub3JpZ2luYWxQYXRoLCAnLT4nLCBjdXIgJiYgY3VyLiQkcm91dGUgJiYgY3VyLiQkcm91dGUub3JpZ2luYWxQYXRoICk7XG5cdH0pO1xuXHQkcm9vdFNjb3BlLiRvbignJHJvdXRlQ2hhbmdlRXJyb3InLCBmdW5jdGlvbihlLCBjdXIsIHByZXYsIHJlamVjdCl7XG5cdFx0Y29uc29sZS5lcnJvciggJ3JvdXRlQ2hhbmdlRXJyb3InLCBwcmV2ICYmIHByZXYuJCRyb3V0ZSAmJiBwcmV2LiQkcm91dGUub3JpZ2luYWxQYXRoLCAnLT4nLCBjdXIgJiYgY3VyLiQkcm91dGUgJiYgY3VyLiQkcm91dGUub3JpZ2luYWxQYXRoLCByZWplY3QgKTtcblx0fSk7XG59XSlcbiIsImFwcC5jb250cm9sbGVyKCdod0hlbHBDdHJsJyxmdW5jdGlvbigkc2NvcGUsJGh0dHAsJGxvY2F0aW9uLGNvbnMsZ2V0U3ViamVjdHMsdXNlckFjY3Qpe1xuICAgICRzY29wZS5pbml0ZGF0ZSA9IFwiXCI7XG4gICAgJHNjb3BlLmluaXR0aW1lID0gXCJcIjtcbiAgICAkc2NvcGUudGltZXpvbmUgPSBtb21lbnQoKS5mb3JtYXQoJ1onKStcIiAtIFwiK2pzdHooKS50aW1lem9uZV9uYW1lO1xuICAgICRzY29wZS5ncmFkZWx2bGxpc3QgPSBjb25zLmdyYWRlTGV2ZWw7XG5cbiAgICAkc2NvcGUuaHcgPSB7fTtcbiAgICAkc2NvcGUuaHcudHlwZSA9IFwiU1JcIjtcbiAgICAkc2NvcGUuaHcuZHVlZGF0ZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LnRpbWV6b25lID0ganN0eigpLnRpbWV6b25lX25hbWU7XG4gICAgJHNjb3BlLmh3LnN1YmplY3RpZCA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmdyYWRlID0gXCJcIjtcbiAgICAkc2NvcGUuaHcuZmFzdCA9IFwibm9ybWFsXCI7XG4gICAgJHNjb3BlLmh3LnR1dG9ybmFtZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmRpc2NvdW50Y29kZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmNvbW1lbnQgPSBcIlwiO1xuXG4gICAgJHNjb3BlLnN1YmplY3RTZWwgPSBcIlwiO1xuICAgICRzY29wZS5zdWJqZWN0bGlzdCA9IFtdO1xuXG4gICAgZ2V0U3ViamVjdHMubGlzdCgpLnRoZW4oZnVuY3Rpb24gc3VjY2Vzc0NhbGxiYWNrKHJlc3BvbnNlKXtcbiAgICAgICRzY29wZS5zdWJqZWN0bGlzdCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAkc2NvcGUuZm9ybWF0c3ViaiA9IFtdO1xuXG4gICAgICBhbmd1bGFyLmZvckVhY2goJHNjb3BlLnN1YmplY3RsaXN0LCBmdW5jdGlvbihpdGVtcywga2V5KXtcbiAgICAgICAgZm9yKHZhciBhPTA7YSA8IGl0ZW1zLnN1YmplY3RzLmxlbmd0aDthKyspe1xuICAgICAgICAgIGl0ZW1zLnN1YmplY3RzW2FdLmdyb3VwaWQgPSBpdGVtcy5ncm91cGlkO1xuICAgICAgICAgIGl0ZW1zLnN1YmplY3RzW2FdLmdyb3VwdGl0bGUgPSBpdGVtcy5ncm91cHRpdGxlO1xuICAgICAgICAgICRzY29wZS5mb3JtYXRzdWJqLnB1c2goaXRlbXMuc3ViamVjdHNbYV0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgICRzY29wZS5zd2l0Y2hTdWJqZWN0ID0gZnVuY3Rpb24oKXtcbiAgICAgICRzY29wZS5ody5zdWJqZWN0aWQgPSBcIlwiO1xuICAgICAgaWYoJHNjb3BlLnN1YmplY3RTZWwgIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICRzY29wZS5ody5zdWJqZWN0aWQgPSAkc2NvcGUuc3ViamVjdFNlbC5pZDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgJHNjb3BlLnNlbmRIdyA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5odyk7XG4gICAgICAgIC8qXG4gICAgICAgICRodHRwKHtcbiAgICBcdFx0XHRtZXRob2Q6ICdQT1NUJyxcbiAgICBcdFx0XHR1cmw6IGNvbnMuYXBpYmFzZSArJy9hcGlyZXF1ZXN0Lmpzb24nLFxuICAgIFx0XHRcdGRhdGE6ICRzY29wZS5od1xuICAgIFx0XHR9KS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjayhyZXNwb25zZSl7XG5cbiAgICAgICAgICAgIC8vdG9kbyAtIHByb3BlciByZWRpcmVjdGlvbiBpZiBzdHVkZW50IG9yIHR1dG9yXG5cbiAgICAgICAgICAgIGlmKHVzZXJBY2N0LnVzZXJ0eXBlKCkgPT09IFwic3R1ZGVudFwiKXtcbiAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0c3R1ZGVudCcpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0c3R1ZGVudCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTsqL1xuICAgIH1cblxuICAgICQoXCIjZGF0ZXBpY2tlckh3XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgJHNjb3BlLmluaXRkYXRlID0gJCh0aGlzKS52YWwoKTtcbiAgICAgICAkc2NvcGUuaHcuZHVlZGF0ZSA9ICRzY29wZS5pbml0ZGF0ZStcIiBcIiskc2NvcGUuaW5pdHRpbWUrXCI6MDBcIjtcbiAgICB9KTtcblxuICAgICQoXCIjdGltZXBpY2tlckh3XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgdmFyIHNlbFRpbWUgPSAgbW9tZW50KCRzY29wZS5pbml0dGltZSwgW1wiaDptbSBBXCJdKS5mb3JtYXQoXCJISDptbVwiKTtcbiAgICAgICAvLyRzY29wZS5pbml0dGltZSA9IG1vbWVudCgkKHRoaXMpLnZhbCgpLCBbXCJoOm1tIEFcIl0pLmZvcm1hdChcIkhIOm1tXCIpO1xuICAgICAgICRzY29wZS5ody5kdWVkYXRlID0gJHNjb3BlLmluaXRkYXRlK1wiIFwiK3NlbFRpbWUrXCI6MDBcIjtcbiAgICB9KTtcblxuXG59KTtcbiIsImFwcC5jb250cm9sbGVyKCdsaXN0Q3RybCcsZnVuY3Rpb24oJHNjb3BlKXtcbiAgICAkc2NvcGUuaXNVc2EgPSBmYWxzZTtcbiAgICBcbiAgICAkc2NvcGUuY291bnRyeVNlbGVjdGVkID0gZnVuY3Rpb24oY291bnRyeXZhbCl7XG4gICAgICBpZihjb3VudHJ5dmFsPT0nVVMnKSRzY29wZS5pc1VzYSA9IHRydWU7XG4gICAgICBlbHNlICRzY29wZS5pc1VzYSA9IGZhbHNlO1xuICAgIH07XG59KVxuIiwiYXBwLmNvbnRyb2xsZXIoJ2xvZ2luQ3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICd1c2VyQWNjdCcsICdjb25zJyxcbmZ1bmN0aW9uKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgdXNlckFjY3QsIGNvbnMpe1xuXHR2YXIgdm0gPSB0aGlzLFxuXHRcdGZvcm0gPSAkc2NvcGUuZm9ybTtcblxuXHR2bS5lcnJvciA9IG51bGw7XG5cdHZtLmxvYWRpbmcgPSBmYWxzZTtcblxuXG4gIHVzZXJBY2N0KCkuY2xlYXIoKTtcblxuXHR2bS5zdWJtaXQgPSBmdW5jdGlvbigpe1xuXHRcdHZhciBkYXRhID0ge1xuXHRcdFx0dXNlcm5hbWU6IHZtLmVtYWlsLFxuXHRcdFx0cGFzc3dvcmQ6IHZtLnBhc3N3b3JkXG5cdFx0fTtcblxuXHRcdCRodHRwKHtcblx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuXHRcdFx0dXJsOiBjb25zLmFwaWJhc2UgKycvYXBpbG9naW4uanNvbicsXG5cdFx0XHRkYXRhOiBkYXRhXG5cdFx0fSlcblx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdGNvbnNvbGUuaW5mbygnbG9naW4gcmVzcG9uc2UnLCByZXMpO1xuXHRcdFx0XHR2YXIgdXNlcnR5cGUgPSByZXMuZGF0YTtcblx0XHRcdFx0dXNlckFjY3QoKS5sb2dpbihkYXRhLnVzZXJuYW1lLCB1c2VydHlwZSwgZGF0YS5wYXNzd29yZCk7XG5cdFx0XHRcdGlmICggdXNlcnR5cGUgPT09ICdzdHVkZW50JyApXG5cdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0c3R1ZGVudCcpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0dHV0b3InKTtcblx0XHRcdH0sIGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdGNvbnNvbGUud2FybignbG9naW4gcmVzcG9uc2UnLCByZXMpO1xuXHRcdFx0XHR2bS5lcnJvciA9ICdJbnZhbGlkIHVzZXJuYW1lL2VtYWlsIG9yIHBhc3N3b3JkJztcblx0XHRcdH0pXG5cdFx0XHQuZmluYWxseShmdW5jdGlvbigpe1xuXHRcdFx0XHR2bS5sb2FkaW5nID0gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHR2bS5lcnJvciA9IG51bGw7XG5cdFx0dm0ubG9hZGluZyA9IHRydWU7XG5cdH07XG5cbn1cbl0pO1xuIiwiYXBwLmNvbnRyb2xsZXIoJ29sdEN0cmwnLGZ1bmN0aW9uKCRzY29wZSwkaHR0cCwkbG9jYXRpb24sY29ucyxnZXRTdWJqZWN0cyx1c2VyQWNjdCl7XG5cbiAgICAkc2NvcGUuaW5pdGRhdGUgPSBcIlwiO1xuICAgICRzY29wZS5pbml0dGltZSA9IFwiXCI7XG4gICAgJHNjb3BlLnRpbWV6b25lID0gbW9tZW50KCkuZm9ybWF0KCdaJykrXCIgLSBcIitqc3R6KCkudGltZXpvbmVfbmFtZTtcbiAgICAkc2NvcGUuZ3JhZGVsdmxsaXN0ID0gY29ucy5ncmFkZUxldmVsO1xuICAgICRzY29wZS5sZW5ndGhsaXN0ID0gY29ucy5sZW5ndGhTZXNzaW9uO1xuXG4gICAgJHNjb3BlLm9sdCA9IHt9O1xuICAgICRzY29wZS5vbHQudHlwZSA9IFwiTFJcIjtcbiAgICAkc2NvcGUub2x0LmR1ZWRhdGUgPSBcIlwiO1xuICAgICRzY29wZS5vbHQudGltZXpvbmUgPSBqc3R6KCkudGltZXpvbmVfbmFtZTtcbiAgICAkc2NvcGUub2x0Lmxlbmd0aCA9IFwiMzAgbWludXRlc1wiO1xuICAgICRzY29wZS5vbHQuc3ViamVjdGlkID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmdyYWRlID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmZhc3QgPSBcIm5vcm1hbFwiO1xuICAgICRzY29wZS5vbHQudHV0b3JuYW1lID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmRpc2NvdW50Y29kZSA9IFwiXCI7XG4gICAgJHNjb3BlLm9sdC5jb21tZW50ID0gXCJcIjtcblxuICAgICRzY29wZS5zdWJqZWN0U2VsID0gXCJcIjtcbiAgICAkc2NvcGUuc3ViamVjdGxpc3QgPSBbXTtcblxuICAgIGdldFN1YmplY3RzLmxpc3QoKS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjayhyZXNwb25zZSl7XG4gICAgICAkc2NvcGUuc3ViamVjdGxpc3QgPSByZXNwb25zZS5kYXRhO1xuICAgICAgJHNjb3BlLmZvcm1hdHN1YmogPSBbXTtcblxuICAgICAgYW5ndWxhci5mb3JFYWNoKCRzY29wZS5zdWJqZWN0bGlzdCwgZnVuY3Rpb24oaXRlbXMsIGtleSl7XG4gICAgICAgIGZvcih2YXIgYT0wO2EgPCBpdGVtcy5zdWJqZWN0cy5sZW5ndGg7YSsrKXtcbiAgICAgICAgICBpdGVtcy5zdWJqZWN0c1thXS5ncm91cGlkID0gaXRlbXMuZ3JvdXBpZDtcbiAgICAgICAgICBpdGVtcy5zdWJqZWN0c1thXS5ncm91cHRpdGxlID0gaXRlbXMuZ3JvdXB0aXRsZTtcbiAgICAgICAgICAkc2NvcGUuZm9ybWF0c3Viai5wdXNoKGl0ZW1zLnN1YmplY3RzW2FdKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAkc2NvcGUuc3dpdGNoU3ViamVjdCA9IGZ1bmN0aW9uKCl7XG4gICAgICAkc2NvcGUub2x0LnN1YmplY3RpZCA9IFwiXCI7XG4gICAgICBpZigkc2NvcGUuc3ViamVjdFNlbCAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICAgJHNjb3BlLm9sdC5zdWJqZWN0aWQgPSAkc2NvcGUuc3ViamVjdFNlbC5pZDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgJHNjb3BlLnNlbmRPbHQgPSBmdW5jdGlvbigpe1xuICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUub2x0KTtcbiAgICAgICAgJGh0dHAoe1xuICAgIFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuICAgIFx0XHRcdHVybDogY29ucy5hcGliYXNlICsnL2FwaXJlcXVlc3QuanNvbicsXG4gICAgXHRcdFx0ZGF0YTogJHNjb3BlLm9sdFxuICAgIFx0XHR9KS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjayhyZXNwb25zZSl7XG4gICAgICAgICAgIC8qXG4gICAgICAgICAgICAgdG9kbyAtIHByb3BlciByZWRpcmVjdGlvbiBpZiBzdHVkZW50IG9yIHR1dG9yXG4gICAgICAgICAgICovXG4gICAgICAgICAgIGlmKHVzZXJBY2N0LnVzZXJ0eXBlKCkgPT09IFwic3R1ZGVudFwiKXtcbiAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlcXVlc3RzdHVkZW50Jyk7XG4gICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0c3R1ZGVudCcpO1xuICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgICQoXCIjZGF0ZXBpY2tlck9sdFwiKS5vbihcImRwLmNoYW5nZVwiLGZ1bmN0aW9uKGUpe1xuICAgICAgICRzY29wZS5pbml0ZGF0ZSA9ICQodGhpcykudmFsKCk7XG4gICAgICAgJHNjb3BlLm9sdC5kdWVkYXRlID0gJHNjb3BlLmluaXRkYXRlK1wiIFwiKyRzY29wZS5pbml0dGltZStcIjowMFwiO1xuICAgIH0pO1xuXG4gICAgJChcIiN0aW1lcGlja2VyT2x0XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgJHNjb3BlLmluaXR0aW1lID0gbW9tZW50KCQodGhpcykudmFsKCksIFtcImg6bW0gQVwiXSkuZm9ybWF0KFwiSEg6bW1cIik7XG4gICAgICAgJHNjb3BlLm9sdC5kdWVkYXRlID0gJHNjb3BlLmluaXRkYXRlK1wiIFwiKyRzY29wZS5pbml0dGltZStcIjowMFwiO1xuICAgIH0pO1xuXG5cbn0pO1xuIiwiYXBwLmNvbnRyb2xsZXIoJ3JlcUNvbnRlbnRTdHVkZW50Q3RybCcsIFsnJHJvdXRlUGFyYW1zJywgJyRzY2UnLCAndXNlckFjY3QnLCAnYXBpUmVxdWVzdCcsICdtb21lbnQnLFxuZnVuY3Rpb24oJHJvdXRlUGFyYW1zLCAkc2NlLCB1c2VyQWNjdCwgYXBpUmVxdWVzdCwgbW9tZW50KXtcblx0dmFyIHZtID0gdGhpcztcblxuXHR2bS5kYXRhID0gbnVsbDtcblx0dm0uaXNMb2FkaW5nID0gdHJ1ZTtcblxuXHR2bS5sb2NhbGl6ZSA9IGZ1bmN0aW9uKHVzZXIpe1xuXHRcdGlmICggdXNlciAmJiB1c2VyID09PSB1c2VyQWNjdC51c2VybmFtZSgpIClcblx0XHRcdHJldHVybiAnWW91Jztcblx0XHRyZXR1cm4gdXNlcjtcblx0fTtcblxuXHR2YXIgaWRzID0gYXBpUmVxdWVzdC5pZHMgPSAkcm91dGVQYXJhbXMuaWRzLnRvVXBwZXJDYXNlKCksXG5cdFx0cmVxSUQgPSBhcGlSZXF1ZXN0LmlkID0gaWRzLnN1YnN0cigyKSxcblx0XHRyZXFUeXBlID0gYXBpUmVxdWVzdC50eXBlID0gaWRzLnN1YnN0cigwLCAyKSxcblx0XHR1cmwgPSAnLmpzb24vJysgcmVxSUQsXG5cdFx0cGFyYW1zID0ge3R5cGU6IGlkcy5zdWJzdHIoMCwgMil9LFxuXHRcdHBheWxvYWQgPSB7bWV0aG9kOiAnR0VUJywgdXJsOiB1cmwsIHBhcmFtczogcGFyYW1zfTtcblxuXG5cdCh2bS5yZWZyZXNoID0gZnVuY3Rpb24oKXtcblx0XHRyZXR1cm4gYXBpUmVxdWVzdChwYXlsb2FkKVxuXHRcdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0dm0uZGF0YSA9IG5vcm1hbGl6ZShyZXMuZGF0YSk7XG5cdFx0XHRcdHZtLmlzUGVuZGluZ1F1b3RlID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAncGVuZGluZyBxdW90ZSc7XG5cdFx0XHRcdHZtLmlzUGVuZGluZ0F1dGggPSB2bS5kYXRhLnJlcXVlc3Quc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09ICdwZW5kaW5nIGF1dGhvcml6YXRpb24nO1xuXHRcdFx0XHR2bS5pc0NhbmNlbGxlZCA9IHZtLmRhdGEucmVxdWVzdC5zdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gJ2NhbmNlbGVkJztcblx0XHRcdFx0dm0uaXNDb21wbGV0ZWQgPSB2bS5kYXRhLnJlcXVlc3Quc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09ICdjb21wbGV0ZWQnO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdFx0cmV0dXJuIHJlcztcblx0XHRcdH0sIGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdHZtLmVycm9yID0gcmVzLmRhdGEuZXJyb3I7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0XHR0aHJvdyB2bS5lcnJvcjtcblx0XHRcdH0pO1xuXHR9KSgpO1xuXG5cdHZtLmFyY2hpdmVSZXF1ZXN0ID0gZnVuY3Rpb24oKXtcblx0XHRhcGlSZXF1ZXN0KHtcblx0XHRcdFx0bWV0aG9kOiAnUFVUJyxcblx0XHRcdFx0dXJsOiAnL2FyY2hpdmUuanNvbicsXG5cdFx0XHRcdGRhdGE6IHt0eXBlOiByZXFUeXBlLCByZXF1ZXN0aWQ6IHJlcUlEfVxuXHRcdFx0fSkudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHR2bS5yZWZyZXNoKCk7XG5cdFx0XHR9KTtcblx0fTtcblxuXG5cdGZ1bmN0aW9uIG5vcm1hbGl6ZShkYXRhKXtcblx0XHRkYXRhID0gYW5ndWxhci5jb3B5KGRhdGEpO1xuXHRcdGlmICggZGF0YS5yZXF1ZXN0LnRhYmxldHlwZSA9PT0gJ3NvbHV0aW9ucmVxdWVzdHMnIClcblx0XHRcdGRhdGEucmVxdWVzdC5pZHMgPSAnU1InKyBkYXRhLnJlcXVlc3QuaWQ7XG5cdFx0ZWxzZSBpZiAoIGRhdGEucmVxdWVzdC50YWJsZXR5cGUgPT09ICdsaXZlcmVxdWVzdHMnIClcblx0XHRcdGRhdGEucmVxdWVzdC5pZHMgPSAnTFInKyBkYXRhLnJlcXVlc3QuaWQ7XG5cblx0XHRkYXRhLnJlcXVlc3QuZGF0ZSA9IG1vbWVudChkYXRhLnJlcXVlc3QuZGF0ZSk7XG5cdFx0ZGF0YS5yZXF1ZXN0LmRhdGVBcmNoaXZlZCA9IG1vbWVudChkYXRhLnJlcXVlc3QuZGF0ZUFyY2hpdmVkKTtcblx0XHRkYXRhLnJlcXVlc3QuZGF0ZUFzc2lnbmVkID0gbW9tZW50KGRhdGEucmVxdWVzdC5kYXRlQXNzaWduZWQpO1xuXHRcdGRhdGEucmVxdWVzdC5kYXRlQ2FuY2VsZWQgPSBtb21lbnQoZGF0YS5yZXF1ZXN0LmRhdGVDYW5jZWxlZCk7XG5cdFx0ZGF0YS5yZXF1ZXN0LmRhdGVDb21wbGV0ZWQgPSBtb21lbnQoZGF0YS5yZXF1ZXN0LmRhdGVDb21wbGV0ZWQpO1xuXHRcdGRhdGEucmVxdWVzdC5kdWVEYXRlID0gbW9tZW50KGRhdGEucmVxdWVzdC5kdWVEYXRlKTtcblx0XHRkYXRhLnJlcXVlc3QubGFzdFN0dWRlbnRQb3N0ID0gbW9tZW50KGRhdGEucmVxdWVzdC5sYXN0U3R1ZGVudFBvc3QpO1xuXHRcdGRhdGEucmVxdWVzdC5sYXN0VHV0b3JQb3N0ID0gbW9tZW50KGRhdGEucmVxdWVzdC5sYXN0VHV0b3JQb3N0KTtcblxuXHRcdGRhdGEuc3ViamVjdHMgPSAkc2NlLnRydXN0QXNIdG1sKGRhdGEuc3ViamVjdHMuam9pbignOyAnKSk7XG5cdFx0ZGF0YS5yZXF1ZXN0LmFkZGl0aW9uYWxJbmZvID0gJHNjZS50cnVzdEFzSHRtbChkYXRhLnJlcXVlc3QuYWRkaXRpb25hbEluZm8pO1xuXG5cdFx0YW5ndWxhci5mb3JFYWNoKGRhdGEucG9zdHMsIGZ1bmN0aW9uKGQsIGkpe1xuXHRcdFx0ZC5kYXRlID0gbW9tZW50KGQuZGF0ZSk7XG5cdFx0XHRkLmlzX25vdGUgPSAoZC5pc19ub3RlID09PSAnMScpO1xuXHRcdH0pO1xuXG5cdFx0cmV0dXJuIGRhdGE7XG5cdH1cbn1cbl0pO1xuIiwiYXBwLmNvbnRyb2xsZXIoJ3JlcUNvbnRlbnRUdXRvckN0cmwnLCBbJyRyb3V0ZVBhcmFtcycsICckc2NlJywgJ3VzZXJBY2N0JywgJ2FwaVJlcXVlc3QnLCAnbW9tZW50JyxcbmZ1bmN0aW9uKCRyb3V0ZVBhcmFtcywgJHNjZSwgdXNlckFjY3QsIGFwaVJlcXVlc3QsIG1vbWVudCl7XG5cdHZhciB2bSA9IHRoaXM7XG5cblx0dm0uZGF0YSA9IG51bGw7XG5cdHZtLmlzTG9hZGluZyA9IHRydWU7XG5cdHZtLmVycm9yID0gbnVsbDtcblxuXHR2bS5sb2NhbGl6ZSA9IGZ1bmN0aW9uKHVzZXIpe1xuXHRcdGlmICggdXNlciAmJiB1c2VyID09PSB1c2VyQWNjdC51c2VybmFtZSgpIClcblx0XHRcdHJldHVybiAnWW91Jztcblx0XHRyZXR1cm4gdXNlcjtcblx0fTtcblxuXHR2bS5pc1BlbmRpbmdRdW90ZSA9IGZhbHNlO1xuXHR2bS5pc1BlbmRpbmdBdXRoID0gZmFsc2U7XG5cblx0dm0uZHVlRGF0ZURpZmYgPSBmdW5jdGlvbigpe1xuXHRcdGlmICggdm0uaXNMb2FkaW5nICkgcmV0dXJuICcnO1xuXHRcdGlmICggdm0uZGF0YS5yZXF1ZXN0LmR1ZURhdGUuaXNBZnRlcigpIClcblx0XHRcdHJldHVybiAnMCBkYXlzIDAgaG91cnMgMCBtaW51dGVzJztcblxuXHRcdHZhciBkdXIgPSBtb21lbnQuZHVyYXRpb24oIG1vbWVudCgpLmRpZmYodm0uZGF0YS5yZXF1ZXN0LmR1ZURhdGUpICk7XG5cdFx0cmV0dXJuIGR1ci5kYXlzKCkgKycgZGF5cyAnKyBkdXIuaG91cnMoKSArJyBob3VycyAnKyBkdXIubWludXRlcygpICsnIG1pbnV0ZXMnO1xuXHR9O1xuXG5cblx0dmFyIGlkcyA9IGFwaVJlcXVlc3QuaWRzID0gJHJvdXRlUGFyYW1zLmlkcy50b1VwcGVyQ2FzZSgpLFxuXHRcdHJlcUlEID0gYXBpUmVxdWVzdC5pZCA9IGlkcy5zdWJzdHIoMiksXG5cdFx0cmVxVHlwZSA9IGFwaVJlcXVlc3QudHlwZSA9IGlkcy5zdWJzdHIoMCwgMiksXG5cdFx0dXJsID0gJy5qc29uLycrIHJlcUlELFxuXHRcdHBhcmFtcyA9IHt0eXBlOiByZXFUeXBlfSxcblx0XHRwYXlsb2FkID0ge21ldGhvZDogJ0dFVCcsIHVybDogdXJsLCBwYXJhbXM6IHBhcmFtc307XG5cblxuXHQodm0ucmVmcmVzaCA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGFwaVJlcXVlc3QocGF5bG9hZClcblx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdHZtLmRhdGEgPSBub3JtYWxpemUocmVzLmRhdGEpO1xuXHRcdFx0XHR2bS5pc1BlbmRpbmdRdW90ZSA9IHZtLmRhdGEucmVxdWVzdC5zdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gJ3BlbmRpbmcgcXVvdGUnO1xuXHRcdFx0XHR2bS5pc1BlbmRpbmdBdXRoID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAncGVuZGluZyBhdXRob3JpemF0aW9uJztcblx0XHRcdFx0dm0uaXNDYW5jZWxsZWQgPSB2bS5kYXRhLnJlcXVlc3Quc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09ICdjYW5jZWxlZCc7XG5cdFx0XHRcdHZtLmlzQ29tcGxldGVkID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAnY29tcGxldGVkJztcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHRcdHJldHVybiByZXM7XG5cdFx0XHR9LCBmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHR2bS5lcnJvciA9IHJlcy5kYXRhLmVycm9yO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdFx0dGhyb3cgdm0uZXJyb3I7XG5cdFx0XHR9KTtcblx0fSkoKTtcblxuXHR2bS5hcmNoaXZlUmVxdWVzdCA9IGZ1bmN0aW9uKCl7XG5cdFx0aWYgKCB1c2VyQWNjdC51c2VybmFtZSgpID09PSB2bS5kYXRhLnJlcXVlc3QudHV0b3IgKVxuXHRcdFx0YXBpUmVxdWVzdCh7XG5cdFx0XHRcdFx0bWV0aG9kOiAnUFVUJyxcblx0XHRcdFx0XHR1cmw6ICcvYXJjaGl2ZS5qc29uJyxcblx0XHRcdFx0XHRkYXRhOiB7dHlwZTogcmVxVHlwZSwgcmVxdWVzdGlkOiByZXFJRH1cblx0XHRcdFx0fSkudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRcdHZtLnJlZnJlc2goKTtcblx0XHRcdFx0fSk7XG5cdH07XG5cdHZtLnJlbGVhc2VSZXF1ZXN0ID0gZnVuY3Rpb24oKXtcblx0XHRpZiAoIHVzZXJBY2N0LnVzZXJuYW1lKCkgPT09IHZtLmRhdGEucmVxdWVzdC50dXRvciApXG5cdFx0XHRhcGlSZXF1ZXN0KHtcblx0XHRcdFx0XHRtZXRob2Q6ICdQVVQnLFxuXHRcdFx0XHRcdHVybDogJy9yZWxlYXNlLmpzb24nLFxuXHRcdFx0XHRcdGRhdGE6IHt0eXBlOiByZXFUeXBlLCByZXF1ZXN0aWQ6IHJlcUlEfVxuXHRcdFx0XHR9KS50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdFx0dm0ucmVmcmVzaCgpO1xuXHRcdFx0XHR9KTtcblx0fTtcblxuXHR2bS5xdW90ZSA9IHtcblx0XHR2YWx1ZTogMCxcblx0XHR2aXNpYmxlOiBmYWxzZSxcblx0XHRzdWJtaXQ6IGZ1bmN0aW9uKCl7XG5cdFx0XHRpZiAoICEgL15bMC05XSskLy50ZXN0KHZtLnF1b3RlLnZhbHVlKSApXG5cdFx0XHRcdHJldHVybiBhbmd1bGFyLmVsZW1lbnQoJ1tuZy1tb2RlbD1cInZtLnF1b3RlLnZhbHVlXCJdJykuZm9jdXMoKSAmJiAwO1xuXG5cdFx0XHRhcGlSZXF1ZXN0KHtcblx0XHRcdFx0XHRtZXRob2Q6ICdQVVQnLFxuXHRcdFx0XHRcdHVybDogJy9xdW90ZS5qc29uJyxcblx0XHRcdFx0XHRkYXRhOiB7dHlwZTogcmVxVHlwZSwgcmVxdWVzdGlkOiByZXFJRCwgcXVvdGU6IHBhcnNlSW50KHZtLnF1b3RlLnZhbHVlKX1cblx0XHRcdFx0fSkudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRcdHZtLnJlZnJlc2goKTtcblx0XHRcdFx0fSk7XG5cdFx0fVxuXHR9O1xuXG5cblx0ZnVuY3Rpb24gbm9ybWFsaXplKGRhdGEpe1xuXHRcdHZhciByZXEgPSBkYXRhLnJlcXVlc3Q7XG5cdFx0aWYgKCByZXEudGFibGV0eXBlID09PSAnc29sdXRpb25yZXF1ZXN0cycgKVxuXHRcdFx0cmVxLmlkcyA9ICdTUicrIHJlcS5pZDtcblx0XHRlbHNlIGlmICggcmVxLnRhYmxldHlwZSA9PT0gJ2xpdmVyZXF1ZXN0cycgKVxuXHRcdFx0cmVxLmlkcyA9ICdMUicrIHJlcS5pZDtcblxuXHRcdHJlcS5kYXRlID0gbW9tZW50KHJlcS5kYXRlKTtcblx0XHRyZXEuZGF0ZUFyY2hpdmVkID0gbW9tZW50KHJlcS5kYXRlQXJjaGl2ZWQpO1xuXHRcdHJlcS5kYXRlQXNzaWduZWQgPSBtb21lbnQocmVxLmRhdGVBc3NpZ25lZCk7XG5cdFx0cmVxLmRhdGVDYW5jZWxlZCA9IG1vbWVudChyZXEuZGF0ZUNhbmNlbGVkKTtcblx0XHRyZXEuZGF0ZUNvbXBsZXRlZCA9IG1vbWVudChyZXEuZGF0ZUNvbXBsZXRlZCk7XG5cdFx0cmVxLmR1ZURhdGUgPSBtb21lbnQocmVxLmR1ZURhdGUpO1xuXHRcdHJlcS5sYXN0U3R1ZGVudFBvc3QgPSBtb21lbnQocmVxLmxhc3RTdHVkZW50UG9zdCk7XG5cdFx0cmVxLmxhc3RUdXRvclBvc3QgPSBtb21lbnQocmVxLmxhc3RUdXRvclBvc3QpO1xuXG5cdFx0cmVxLnF1b3RlID0gcGFyc2VGbG9hdCggcmVxLnF1b3RlICk7XG5cblx0XHRkYXRhLnN1YmplY3RzID0gJHNjZS50cnVzdEFzSHRtbChkYXRhLnN1YmplY3RzLmpvaW4oJzsgJykpO1xuXHRcdHJlcS5hZGRpdGlvbmFsSW5mbyA9ICRzY2UudHJ1c3RBc0h0bWwocmVxLmFkZGl0aW9uYWxJbmZvKTtcblxuXHRcdCQuZWFjaChkYXRhLnBvc3RzLCBmdW5jdGlvbihpLCBkKXtcblx0XHRcdGQuZGF0ZSA9IG1vbWVudChkLmRhdGUpO1xuXHRcdFx0ZC5pc19ub3RlID0gKGQuaXNfbm90ZSA9PT0gJzEnKTtcblx0XHR9KTtcblxuXHRcdHJldHVybiBkYXRhO1xuXHR9XG59XG5dKTsiLCJhcHAuY29udHJvbGxlcigncmVxdWVzdHNTdHVkZW50Q3RybCcsIFsnYXBpUmVxdWVzdCcsICdtb21lbnQnLFxuZnVuY3Rpb24oYXBpUmVxdWVzdCwgbW9tZW50KXtcblx0dmFyIHZtID0gdGhpcztcblxuXHR2bS5saXN0ID0gW107XG5cdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXG5cdHZtLmZpZWxkcyA9IHtcblx0XHQnU2hvdyBhbGwgcmVxdWVzdHMnOiB7ZGF5c19hZ286IC0xLCBzaG93X2FyY2hpdmVkOiBmYWxzZX0sXG5cdFx0J1Nob3cgbGFzdCAxNSBkYXlzJzoge2RheXNfYWdvOiAxNSwgc2hvd19hcmNoaXZlZDogZmFsc2V9LFxuXHRcdCdTaG93IGxhc3QgNjAgZGF5cyc6IHtkYXlzX2FnbzogNjAsIHNob3dfYXJjaGl2ZWQ6IGZhbHNlfSxcblx0XHQnU2hvdyBsYXN0IDkwIGRheXMnOiB7ZGF5c19hZ286IDkwLCBzaG93X2FyY2hpdmVkOiBmYWxzZX0sXG5cdFx0J1Nob3cgYXJjaGl2ZWQgcmVxdWVzdHMnOiB7ZGF5c19hZ286IC0xLCBzaG93X2FyY2hpdmVkOiB0cnVlfVxuXHR9O1xuXG5cdHZhciBkZWZhdWx0UGFyYW1zID0gdm0uZmllbGRzWydTaG93IGxhc3QgMTUgZGF5cyddO1xuXG5cdHZhciBwYXlsb2FkID0ge1xuXHRcdG1ldGhvZDogJ0dFVCcsXG5cdFx0dXJsOiAnL3N0dWRlbnRhbGwuanNvbidcblx0fTtcblx0dm0ucGFyYW1zID0gZGVmYXVsdFBhcmFtcztcblxuXHQodm0ucmVmcmVzaCA9IGZ1bmN0aW9uKCl7XG5cdFx0cGF5bG9hZC5wYXJhbXMgPSBhbmd1bGFyLmNvcHkodm0ucGFyYW1zKTtcblx0XHRhcGlSZXF1ZXN0KHBheWxvYWQpXG5cdFx0XHQudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHR2bS5saXN0ID0gW107XG5cdFx0XHRcdGFuZ3VsYXIuZm9yRWFjaChyZXMuZGF0YSwgZnVuY3Rpb24oZCl7XG5cdFx0XHRcdFx0dm0ubGlzdC5wdXNoKG5vcm1hbGl6ZURhdGEoZCkpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH0pXG5cdFx0XHQuZmluYWxseShmdW5jdGlvbigpe1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdH0pO1xuXHRcdHZtLmlzTG9hZGluZyA9IHRydWU7XG5cdH0pKCk7XG5cblx0dm0ucmVzZXQgPSBmdW5jdGlvbihmb3JtKXtcblx0XHR2bS5wYXJhbXMgPSBkZWZhdWx0UGFyYW1zO1xuXHRcdGZvcm0uJHNldFByaXN0aW5lKHRydWUpO1xuXHRcdHZtLnN1Ym1pdChmb3JtKTtcblx0fTtcblx0dm0uc3VibWl0ID0gZnVuY3Rpb24oZm9ybSl7XG5cdFx0aWYgKCAhISBmb3JtICkge1xuXHRcdFx0YW5ndWxhci5lbGVtZW50KCdmb3JtW25hbWU9XCInKyBmb3JtLiRuYW1lICsnXCJdJykucGFyZW50cygnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblx0XHRcdHZtLnJlc2V0LiRkaXJ0eSA9IGZvcm0uJGRpcnR5O1xuXHRcdH1cblx0XHR2bS5yZWZyZXNoKCk7XG5cdH07XG5cblxuXHRmdW5jdGlvbiBub3JtYWxpemVEYXRhKGRhdGEpe1xuXHRcdGlmICggZGF0YS50YWJsZXR5cGUgPT09ICdzb2x1dGlvbnJlcXVlc3RzJyApIHtcblx0XHRcdGRhdGEudGFibGV0eXBlID0gJ1NvbHV0aW9ucyBSZXF1ZXN0Jztcblx0XHRcdGRhdGEuaWRzID0gJ1NSJysgZGF0YS5pZDtcblx0XHR9IGVsc2Vcblx0XHRpZiAoIGRhdGEudGFibGV0eXBlID09PSAnbGl2ZXJlcXVlc3RzJyApIHtcblx0XHRcdGRhdGEudGFibGV0eXBlID0gJ0xpdmUgUmVxdWVzdCc7XG5cdFx0XHRkYXRhLmlkcyA9ICdMUicrIGRhdGEuaWQ7XG5cdFx0fVxuXG5cdFx0ZGF0YS5kYXRlID0gbW9tZW50KGRhdGEuZGF0ZSk7XG5cdFx0ZGF0YS5kdWVEYXRlID0gbW9tZW50KGRhdGEuZHVlRGF0ZSk7XG5cdFx0ZGF0YS5sYXN0VHV0b3JQb3N0ID0gbW9tZW50KGRhdGEubGFzdFR1dG9yUG9zdCk7XG5cdFx0cmV0dXJuIGRhdGE7XG5cdH1cbn1cbl0pO1xuIiwiYXBwLmNvbnRyb2xsZXIoJ3JlcXVlc3RzVHV0b3JDdHJsJywgWydhcGlSZXF1ZXN0JywgJ21vbWVudCcsXG5mdW5jdGlvbihhcGlSZXF1ZXN0LCBtb21lbnQpe1xuXHR2YXIgdm0gPSB0aGlzO1xuXG5cdHZtLmxpc3QgPSBbXTtcblx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cblx0dmFyIGRlZmF1bHRQYXJhbXMgPSB7XG5cdFx0b3JkZXJfYnk6ICdkdWVEYXRlJyxcblx0XHRvcmRlcl9ieV9kaXJlY3Rpb246ICdERVNDJyxcblx0XHRwaXBlbGluZTogJ25vLXBpcGVsaW5lJyxcblx0XHR0YWJsZTogJ2FsbCcsXG5cdFx0c3RhdHVzOiAnYWN0aXZlJyxcblx0XHRhcmNoaXZlZDogJ3VuYXJjaGl2ZWQnLFxuXHRcdGFjY3Q6IC0xLFxuXHRcdGtleXdvcmQ6ICcnXG5cdH07XG5cblx0dmFyIHBheWxvYWQgPSB7XG5cdFx0bWV0aG9kOiAnR0VUJyxcblx0XHR1cmw6ICcvdHV0b3JhbGwuanNvbicsXG5cdFx0cGFyYW1zOiB2bS5wYXJhbXMgPSBhbmd1bGFyLmNvcHkoZGVmYXVsdFBhcmFtcylcblx0fTtcblxuXHQodm0ucmVmcmVzaCA9IGZ1bmN0aW9uKCl7XG5cdFx0cGF5bG9hZC5wYXJhbXMua2V5d29yZCA9IHBheWxvYWQucGFyYW1zLmtleXdvcmQudHJpbSgpO1xuXG5cdFx0YXBpUmVxdWVzdChwYXlsb2FkKVxuXHRcdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0dm0ubGlzdCA9IFtdO1xuXHRcdFx0XHRhbmd1bGFyLmZvckVhY2gocmVzLmRhdGEucmVxdWVzdHMgfHwgW10sIGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHRcdHZtLmxpc3QucHVzaChub3JtYWxpemVEYXRhKGQpKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KVxuXHRcdFx0LmZpbmFsbHkoZnVuY3Rpb24oKXtcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHR2bS5pc0xvYWRpbmcgPSB0cnVlO1xuXHR9KSgpO1xuXG5cblx0dm0ucGlwZWxpbmUgPSBmYWxzZTtcblx0dm0udG9nZ2xlUGlwZWxpbmUgPSBmdW5jdGlvbigpe1xuXHRcdHZtLnBpcGVsaW5lID0gIXZtLnBpcGVsaW5lO1xuXHRcdHZtLnBhcmFtcy5waXBlbGluZSA9IHZtLnBpcGVsaW5lID8gJ3BpcGVsaW5lLW9ubHknIDogJ25vLXBpcGVsaW5lJztcblx0XHR2bS5yZWZyZXNoKCk7XG5cdH07XG5cdHZtLnNvcnQgPSB7XG5cdFx0b3JkZXJfYnk6IHtcblx0XHRcdCd0aW1lX3VuYXR0ZW5kZWQnOiAnVGltZSBVbmF0dGVuZGVkJyxcblx0XHRcdCdkYXRlJzogJ0RhdGUnLFxuXHRcdFx0J2R1ZURhdGUnOiAnRHVlIERhdGUnXG5cdFx0fSxcblx0XHRvcmRlcl9ieV9kaXJlY3Rpb246IHtcblx0XHRcdCdERVNDJzogJ0Rlc2NlbmRpbmcnLFxuXHRcdFx0J0FTQyc6ICdBc2NlbmRpbmcnXG5cdFx0fVxuXHR9O1xuXHR2bS5maWx0ZXIgPSB7XG5cdFx0c3RhdHVzOiB7XG5cdFx0XHQnYWN0aXZlJzogJ0FjdGl2ZScsXG5cdFx0XHQnUGVuZGluZyBRdW90ZSc6ICdQZW5kaW5nIFF1b3RlJyxcblx0XHRcdCdQZW5kaW5nIEF1dGhvcml6YXRpb24nOiAnUGVuZGluZyBBdXRob3JpemF0aW9uJyxcblx0XHRcdCdQZW5kaW5nIENvbXBsZXRpb24nOiAnUGVuZGluZyBDb21wbGV0aW9uJyxcblx0XHRcdCdDb21wbGV0ZWQnOiAnQ29tcGxldGVkJyxcblx0XHRcdCdDYW5jZWxlZCc6ICdDYW5jZWxlZCdcblx0XHR9LFxuXHRcdHRhYmxlOiB7XG5cdFx0XHQnYWxsJzogJ0FsbCcsXG5cdFx0XHQnc29sdXRpb25yZXF1ZXN0cyc6ICdTb2x1dGlvbnMgUmVxdWVzdHMnLFxuXHRcdFx0J2xpdmVyZXF1ZXN0cyc6ICdMaXZlIFJlcXVlc3RzJ1xuXHRcdH0sXG5cdFx0YXJjaGl2ZWQ6IHtcblx0XHRcdCdhbGwnOiAnQXJjaGl2ZWQgJiBVbmFyY2hpdmVkJyxcblx0XHRcdCdhcmNoaXZlZCc6ICdBcmNoaXZlZCcsXG5cdFx0XHQndW5hcmNoaXZlZCc6ICdVbmFyY2hpdmVkJyxcblx0XHR9XG5cdH07XG5cblx0dm0uc29ydFJlc2V0ID0gZnVuY3Rpb24oZm9ybSl7XG5cdFx0Zm9yKCB2YXIgayBpbiB2bS5zb3J0IClcblx0XHRcdGlmICggdm0uc29ydC5oYXNPd25Qcm9wZXJ0eShrKSApXG5cdFx0XHRcdHZtLnBhcmFtc1trXSA9IGRlZmF1bHRQYXJhbXNba107XG5cdFx0Zm9ybS4kc2V0UHJpc3RpbmUodHJ1ZSk7XG5cdFx0dm0uc3VibWl0KGZvcm0pO1xuXHR9O1xuXHR2bS5maWx0ZXJSZXNldCA9IGZ1bmN0aW9uKGZvcm0pe1xuXHRcdGZvciggdmFyIGsgaW4gdm0uZmlsdGVyIClcblx0XHRcdGlmICggdm0uZmlsdGVyLmhhc093blByb3BlcnR5KGspIClcblx0XHRcdFx0dm0ucGFyYW1zW2tdID0gZGVmYXVsdFBhcmFtc1trXTtcblx0XHRmb3JtLiRzZXRQcmlzdGluZSh0cnVlKTtcblx0XHR2bS5zdWJtaXQoZm9ybSk7XG5cdH07XG5cdHZtLnN1Ym1pdCA9IGZ1bmN0aW9uKGZvcm0pe1xuXHRcdGlmICggISEgZm9ybSApIHtcblx0XHRcdGFuZ3VsYXIuZWxlbWVudCgnZm9ybVtuYW1lPVwiJysgZm9ybS4kbmFtZSArJ1wiXScpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHR2bVtmb3JtLiRuYW1lICsnUmVzZXQnXS4kZGlydHkgPSBmb3JtLiRkaXJ0eTtcblx0XHR9XG5cdFx0dm0ucmVmcmVzaCgpO1xuXHR9O1xuXG5cblx0ZnVuY3Rpb24gbm9ybWFsaXplRGF0YShkYXRhKXtcblx0XHRpZiAoIGRhdGEudGFibGV0eXBlID09PSAnc29sdXRpb25yZXF1ZXN0cycgKSB7XG5cdFx0XHRkYXRhLnRhYmxldHlwZSA9ICdTb2x1dGlvbnMgUmVxdWVzdCc7XG5cdFx0XHRkYXRhLmlkcyA9ICdTUicrIGRhdGEuaWQ7XG5cdFx0fSBlbHNlXG5cdFx0aWYgKCBkYXRhLnRhYmxldHlwZSA9PT0gJ2xpdmVyZXF1ZXN0cycgKSB7XG5cdFx0XHRkYXRhLnRhYmxldHlwZSA9ICdMaXZlIFJlcXVlc3QnO1xuXHRcdFx0ZGF0YS5pZHMgPSAnTFInKyBkYXRhLmlkO1xuXHRcdH1cblxuXHRcdGRhdGEuZGF0ZSA9IG1vbWVudChkYXRhLmRhdGUpO1xuXHRcdGRhdGEuZHVlRGF0ZSA9IG1vbWVudChkYXRhLmR1ZURhdGUpO1xuXHRcdGRhdGEubGFzdFR1dG9yUG9zdCA9IG1vbWVudChkYXRhLmxhc3RUdXRvclBvc3QpO1xuXHRcdGRhdGEudW5hdHRlbmRlZCA9IE1hdGguY2VpbChkYXRhLnRpbWVfdW5hdHRlbmRlZCAvIDYwIC8gMjQpO1xuXHRcdGlmICggZGF0YS51bmF0dGVuZGVkID4gMSApXG5cdFx0XHRkYXRhLnVuYXR0ZW5kZWQgKz0gJyBkYXlzJztcblx0XHRlbHNlXG5cdFx0XHRkYXRhLnVuYXR0ZW5kZWQgKz0gJyBkYXknO1xuXHRcdHJldHVybiBkYXRhO1xuXHR9XG59XG5dKTtcbiIsImFwcC5jb250cm9sbGVyKCd6b25lQ3RybCcsIGZ1bmN0aW9uKCRzY29wZSkge1xuICAgJHNjb3BlLmN1cnJlbnRUWiA9IG1vbWVudCgpLmZvcm1hdCgnWicpO1xuIH0pO1xuIiwiYXBwLmRpcmVjdGl2ZSgnYWRkUG9zdCcsIFsnYXBpUmVxdWVzdCcsIGZ1bmN0aW9uKGFwaVJlcXVlc3Qpe1xuXHRyZXR1cm4ge1xuXHRcdHJlc3RyaWN0OiAnRUEnLFxuXHRcdHNjb3BlOiB7fSxcblx0XHR0ZW1wbGF0ZVVybDogJ2FwcC9wYWdlcy9tb2RhbC9hZGRQb3N0LnRwbC5odG1sJyxcblx0XHRsaW5rOiBmdW5jdGlvbigkc2NvcGUsIGVsZW0sIGF0dHIpe1xuXHRcdFx0ZWxlbS5hdHRyKHtcblx0XHRcdFx0J2lkJzogJ21vZGFsLWFkZC1wb3N0Jyxcblx0XHRcdFx0J2NsYXNzJzogJ21vZGFsIGZhZGUnLFxuXHRcdFx0XHQndGFiaW5kZXgnOiAnLTEnLFxuXHRcdFx0XHQncm9sZSc6ICdkaWFsb2cnLFxuXHRcdFx0XHQnYXJpYS1sYWJlbGxlZGJ5JzogJ2FkZFBvc3QnLFxuXHRcdFx0XHQnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcblx0XHRcdH0pLm9uKCdzaG93LmJzLm1vZGFsIGhpZGRlbi5icy5tb2RhbCcsIHJlc2V0KTtcblxuXHRcdFx0dmFyIHZtID0gJHNjb3BlLnZtID0ge3N1Ym1pdDogc3VibWl0fSxcblx0XHRcdFx0cGF5bG9hZCA9IHtcblx0XHRcdFx0XHRtZXRob2Q6ICdQT1NUJyxcblx0XHRcdFx0XHR1cmw6ICcvYWRkcG9zdC5qc29uJyxcblx0XHRcdFx0XHRkYXRhOiB7dHlwZTogYXBpUmVxdWVzdC50eXBlLCByZXF1ZXN0aWQ6IGFwaVJlcXVlc3QuaWR9XG5cdFx0XHRcdH07XG5cblx0XHRcdGZ1bmN0aW9uIHJlc2V0KCl7XG5cdFx0XHRcdHZtLmZpbGUgPSB1bmRlZmluZWQ7XG5cdFx0XHRcdHZtLmNvbW1lbnQgPSAnJztcblx0XHRcdFx0dm0uZXJyb3JNc2cgPSBudWxsO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdH1cblx0XHRcdGZ1bmN0aW9uIHN1Ym1pdCgpe1xuXHRcdFx0XHRpZiAoIHZtLmNvbW1lbnQudHJpbSgpID09PSAnJyApLy8gJiYgISB2bS5maWxlIClcblx0XHRcdFx0XHRyZXR1cm4gZWxlbS5maW5kKCd0ZXh0YXJlYScpLmZvY3VzKCk7XG5cblx0XHRcdFx0cGF5bG9hZC5kYXRhLmNvbW1lbnQgPSB2bS5jb21tZW50LnRyaW0oKTtcblx0XHRcdFx0YXBpUmVxdWVzdChwYXlsb2FkKVxuXHRcdFx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdFx0XHQkc2NvcGUuJHBhcmVudC4kZXZhbChhdHRyLmFkZFBvc3RTdWNjZXNzKTtcblx0XHRcdFx0XHRcdGVsZW0ubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdHJlc2V0KCk7XG5cdFx0XHRcdFx0fSwgZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0XHRcdHZtLmVycm9yTXNnID0gJ0FuIGVycm9yIG9jY3VyZWQgd2hpbGUgcG9zdGluZyc7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuZmluYWxseShmdW5jdGlvbigpe1xuXHRcdFx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IHRydWU7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59XSk7IiwiYXBwLmRpcmVjdGl2ZSgnY2FuY2VsUmVxdWVzdCcsIFsnYXBpUmVxdWVzdCcsICdjb25zJywgZnVuY3Rpb24oYXBpUmVxdWVzdCwgY29ucyl7XG5cdHJldHVybiB7XG5cdFx0cmVzdHJpY3Q6ICdFQScsXG5cdFx0c2NvcGU6IHt9LFxuXHRcdHRlbXBsYXRlVXJsOiAnYXBwL3BhZ2VzL21vZGFsL2NhbmNlbFJlcXVlc3RSZWFzb24udHBsLmh0bWwnLFxuXHRcdGxpbms6IGZ1bmN0aW9uKCRzY29wZSwgZWxlbSwgYXR0cil7XG5cdFx0XHRlbGVtLmF0dHIoe1xuXHRcdFx0XHQnaWQnOiAnbW9kYWwtY2FuY2VsLXJlcXVlc3QnLFxuXHRcdFx0XHQnY2xhc3MnOiAnbW9kYWwgZmFkZScsXG5cdFx0XHRcdCd0YWJpbmRleCc6ICctMScsXG5cdFx0XHRcdCdyb2xlJzogJ2RpYWxvZycsXG5cdFx0XHRcdCdhcmlhLWxhYmVsbGVkYnknOiAnY2FuY2VsUmVxdWVzdCcsXG5cdFx0XHRcdCdhcmlhLWhpZGRlbic6ICd0cnVlJ1xuXHRcdFx0fSkub24oJ3Nob3cuYnMubW9kYWwgaGlkZGVuLmJzLm1vZGFsJywgcmVzZXQpO1xuXG5cdFx0XHR2YXIgdm0gPSAkc2NvcGUudm0gPSB7c3VibWl0OiBzdWJtaXR9LFxuXHRcdFx0XHRwYXlsb2FkID0ge1xuXHRcdFx0XHRcdG1ldGhvZDogJ1BVVCcsXG5cdFx0XHRcdFx0dXJsOiAnL2NhbmNlbC5qc29uJyxcblx0XHRcdFx0XHRkYXRhOiB7dHlwZTogYXBpUmVxdWVzdC50eXBlLCByZXF1ZXN0aWQ6IGFwaVJlcXVlc3QuaWR9XG5cdFx0XHRcdH07XG5cblx0XHRcdGZ1bmN0aW9uIHJlc2V0KCl7XG5cdFx0XHRcdHZtLnJlYXNvbiA9ICcnO1xuXHRcdFx0XHR2bS5lcnJvck1zZyA9IG51bGw7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0fVxuXHRcdFx0ZnVuY3Rpb24gc3VibWl0KCl7XG5cdFx0XHRcdGlmICggdm0ucmVhc29uLnRyaW0oKSA9PT0gJycgKS8vICYmICEgdm0uZmlsZSApXG5cdFx0XHRcdFx0cmV0dXJuIGVsZW0uZmluZCgndGV4dGFyZWEnKS5mb2N1cygpO1xuXG5cdFx0XHRcdHBheWxvYWQuZGF0YS5yZWFzb24gPSB2bS5yZWFzb24udHJpbSgpO1xuXHRcdFx0XHRhcGlSZXF1ZXN0KHBheWxvYWQpXG5cdFx0XHRcdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0XHRcdCRzY29wZS4kcGFyZW50LiRldmFsKGF0dHIuY2FuY2VsUmVxdWVzdFN1Y2Nlc3MpO1xuXHRcdFx0XHRcdFx0ZWxlbS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdFx0cmVzZXQoKTtcblx0XHRcdFx0XHR9LCBmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRcdFx0dm0uZXJyb3JNc2cgPSAnQW4gZXJyb3Igb2NjdXJlZCB3aGlsZSBwb3N0aW5nJztcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5maW5hbGx5KGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gdHJ1ZTtcblx0XHRcdH1cblx0XHR9XG5cdH1cbn1dKTtcbiIsIid1c2Ugc3RyaWN0JztcblxuYXBwLmRpcmVjdGl2ZSgnaGVhZGVybmF2JywgZnVuY3Rpb24gKGNvbnMpIHtcbiAgY29uc29sZS5sb2coY29ucyk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICByZXN0cmljdDogJ0EnLCAvL1RoaXMgbWVuYXMgdGhhdCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gYXR0cmlidXRlIGFuZCBOT1QgYXMgYW4gZWxlbWVudC4gSSBkb24ndCBsaWtlIGNyZWF0aW5nIGN1c3RvbSBIVE1MIGVsZW1lbnRzXG4gICAgICAgIHJlcGxhY2U6IHRydWUsXG4gICAgICAgIHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoK1wiaGVhZGVybmF2LnRwbC5odG1sXCIsXG4gICAgICAgIGNvbnRyb2xsZXI6IFsnJHNjb3BlJywgJyRmaWx0ZXInLCBmdW5jdGlvbiAoJHNjb3BlLCAkZmlsdGVyKSB7XG4gICAgICAgICAgICAvLyBZb3VyIGJlaGF2aW91ciBnb2VzIGhlcmUgOilcbiAgICAgICAgfV1cbiAgICB9XG59KTtcbiIsImFwcC5kaXJlY3RpdmUoJ3Bhc3N3b3JkTWF0Y2gnLCBbZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIHJlc3RyaWN0OiAnQScsXG4gICAgICAgIHNjb3BlOnRydWUsXG4gICAgICAgIHJlcXVpcmU6ICduZ01vZGVsJyxcbiAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbGVtICwgYXR0cnMsY29udHJvbCkge1xuICAgICAgICAgICAgdmFyIGNoZWNrZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgLy9nZXQgdGhlIHZhbHVlIG9mIHRoZSBmaXJzdCBwYXNzd29yZFxuICAgICAgICAgICAgICAgIHZhciBlMSA9IHNjb3BlLiRldmFsKGF0dHJzLm5nTW9kZWwpO1xuXG4gICAgICAgICAgICAgICAgLy9nZXQgdGhlIHZhbHVlIG9mIHRoZSBvdGhlciBwYXNzd29yZFxuICAgICAgICAgICAgICAgIHZhciBlMiA9IHNjb3BlLiRldmFsKGF0dHJzLnBhc3N3b3JkTWF0Y2gpO1xuICAgICAgICAgICAgICAgIHJldHVybiBlMSA9PSBlMjtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBzY29wZS4kd2F0Y2goY2hlY2tlciwgZnVuY3Rpb24gKG4pIHtcbiAgICAgICAgICAgICAgICAvL3NldCB0aGUgZm9ybSBjb250cm9sIHRvIHZhbGlkIGlmIGJvdGhcbiAgICAgICAgICAgICAgICAvL3Bhc3N3b3JkcyBhcmUgdGhlIHNhbWUsIGVsc2UgaW52YWxpZFxuICAgICAgICAgICAgICAgIGNvbnRyb2wuJHNldFZhbGlkaXR5KFwicHdtYXRjaFwiLCBuKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcbn1dKTtcbiIsImFwcC5mYWN0b3J5KCdhcGlSZXF1ZXN0JywgWyckaHR0cCcsICdjb25zJywgJ3VzZXJBY2N0JywgZnVuY3Rpb24oJGh0dHAsIGNvbnMsIHVzZXJBY2N0KXtcblx0cmV0dXJuIGZ1bmN0aW9uKHBheWxvYWQpe1xuXHRcdHBheWxvYWQgPSBhbmd1bGFyLmNvcHkocGF5bG9hZCk7XG5cdFx0cGF5bG9hZC51cmwgPSBjb25zLmFwaWJhc2UgKycvYXBpcmVxdWVzdCcrIHBheWxvYWQudXJsO1xuXG5cdFx0Y29uc29sZS5pbmZvKCBwYXlsb2FkLm1ldGhvZCwgcGF5bG9hZC51cmwsIHBheWxvYWQucGFyYW1zLCBwYXlsb2FkLmRhdGEgKTtcblxuXHRcdC8vIHRvZG86IHJlbW92ZSB0aGlzIHdoZW4gYmFzaWMgYXV0aCBpcyBmaXhlZCBmcm9tIGFwaVxuXHRcdHBheWxvYWQudXJsID0gcGF5bG9hZC51cmwucmVwbGFjZSgnaHR0cDovLycsICdodHRwOi8vJysgdXNlckFjY3QudXNlcm5hbWUoKSArJzonKyB1c2VyQWNjdC5wYXNzd29yZCgpICsnQCcpO1xuXG5cdFx0cmV0dXJuICRodHRwKHBheWxvYWQpXG5cdFx0XHQudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRjb25zb2xlLmluZm8oIHJlcy5zdGF0dXMsIHJlcy5jb25maWcudXJsLCByZXMuZGF0YSApO1xuXHRcdFx0XHRyZXR1cm4gcmVzO1xuXHRcdFx0fSwgZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0Y29uc29sZS5lcnJvciggcmVzLnN0YXR1cywgcmVzLmNvbmZpZy51cmwsIHJlcy5kYXRhICk7XG5cdFx0XHRcdHJlcy5kYXRhID0gcmVzLmRhdGEgfHwge2Vycm9yOiB7Y29kZTogcmVzLnN0YXR1cywgbWVzc2FnZTogcmVzLnN0YXR1c1RleHR9fVxuXHRcdFx0XHR0aHJvdyByZXM7XG5cdFx0XHR9KTtcblx0fVxufV0pOyIsImFwcC5mYWN0b3J5KCdnZXRTdWJqZWN0cycsIGZ1bmN0aW9uKCRodHRwLGNvbnMpe1xuICAgIHJldHVybiB7XG4gICAgICBsaXN0IDogZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgIHVybDogXHRjb25zLmFwaWJhc2UgKycvYXBpc3ViamVjdHMuanNvbi8nXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbn0pO1xuIiwiLyogVXNlciBBY2NvdW50IFNlcnZpY2VcblxudG8gY2hlY2sgZm9yIHVzZXIgY3JlZGVudGlhbHM6XG5cdGZ1bmN0aW9uKHVzZXJBY2N0KXtcblx0XHRjb25zb2xlLmxvZyggdXNlckFjY3QuaXNWYWxpZCgpICk7XG5cdFx0Y29uc29sZS5sb2coIHVzZXJBY2N0LmlzU3R1ZGVudCgpICk7XG5cdFx0Y29uc29sZS5sb2coIHVzZXJBY2N0LmlzVHV0b3IoKSApO1xuXHRcdGNvbnNvbGUubG9nKCB1c2VyQWNjdC51c2VybmFtZSgpICk7XG5cdFx0Y29uc29sZS5sb2coIHVzZXJBY2N0LnVzZXJ0eXBlKCkgKTtcblx0fVxuXG50byBzZXQgbG9naW4gY3JlZGVudGlhbHM6XG5cdGZ1bmN0aW9uKHVzZXJBY2N0KXtcblx0XHR1c2VyQWNjdCgpLmxvZ2luKCdqb2huJywgJ3N0dWRlbnQnKTtcblx0fVxuKi9cbmFwcC5mYWN0b3J5KCd1c2VyQWNjdCcsIFsnJHdpbmRvdycsIGZ1bmN0aW9uKCR3aW5kb3cpe1xuXHR2YXIgbG9jID0gKGZ1bmN0aW9uKCl7XG5cdFx0dmFyIHMgPSAkd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyJyk7XG5cdFx0cmV0dXJuIHMgPyBKU09OLnBhcnNlKHMpIDogbnVsbDtcblx0fSkoKSB8fCB7XG5cdFx0dXNlcm5hbWU6IG51bGwsXG5cdFx0dHlwZTogbnVsbFxuXHR9O1xuXG5cblx0ZnVuY3Rpb24gc3ZjKCl7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGxvZ2luOiBmdW5jdGlvbiggdXNlcm5hbWUsIHR5cGUsIHBhc3N3b3JkICl7Ly9mb3Igbm93XG5cdFx0XHRcdGxvYy51c2VybmFtZSA9IHVzZXJuYW1lO1xuXHRcdFx0XHRsb2MudHlwZSA9IHR5cGU7XG5cdFx0XHRcdGxvYy5wYXNzd29yZCA9IHBhc3N3b3JkOy8vdGVtcCBvbmx5XG5cdFx0XHRcdHRyeXtcblx0XHRcdFx0XHQkd2luZG93LmxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1c2VyJywgSlNPTi5zdHJpbmdpZnkobG9jKSk7XG5cdFx0XHRcdH1jYXRjaChlKXt9O1xuXHRcdFx0fSxcblx0XHRcdGNsZWFyOiBmdW5jdGlvbigpe1xuXHRcdFx0XHRsb2MudXNlcm5hbWUgPSBudWxsO1xuXHRcdFx0XHRsb2MudHlwZSA9IG51bGw7XG5cdFx0XHR9XG5cdFx0fTtcblx0fVxuXHRzdmMuaXNWYWxpZCA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuICEhIGxvYy51c2VybmFtZTtcblx0fTtcblx0c3ZjLmlzU3R1ZGVudCA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGxvYy50eXBlID09PSAnc3R1ZGVudCc7XG5cdH07XG5cdHN2Yy5pc1R1dG9yID0gZnVuY3Rpb24oKXtcblx0XHRyZXR1cm4gbG9jLnR5cGUgPT09ICd0dXRvcic7XG5cdH07XG5cdHN2Yy5pc0FkbWluID0gZnVuY3Rpb24oKXtcblx0XHRyZXR1cm4gbG9jLnR5cGUgPT09ICdhZG1pbic7XG5cdH07XG5cdHN2Yy51c2VybmFtZSA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGxvYy51c2VybmFtZTtcblx0fTtcblx0c3ZjLnBhc3N3b3JkID0gZnVuY3Rpb24oKXtcblx0XHRyZXR1cm4gbG9jLnBhc3N3b3JkO1xuXHR9O1xuXHRzdmMudXNlcnR5cGUgPSBmdW5jdGlvbigpe1xuXHRcdHJldHVybiBsb2MudHlwZTtcblx0fTtcblxuXHRyZXR1cm4gc3ZjO1xufV0pXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=

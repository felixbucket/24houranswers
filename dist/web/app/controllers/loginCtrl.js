app.controller('loginCtrl', ['$scope', '$http', '$location', 'userAcct', 'cons',
function($scope, $http, $location, userAcct, cons){
	var vm = this,
		form = $scope.form;

	vm.error = null;
	vm.loading = false;


  userAcct().clear();

	vm.submit = function(){
		var data = {
			username: vm.email,
			password: vm.password
		};

		$http({
			method: 'POST',
			url: cons.apibase +'/apilogin.json',
			data: data
		})
			.then(function(res){
				console.info('login response', res);
				var usertype = res.data;
				userAcct().login(data.username, usertype, data.password);
				if ( usertype === 'student' )
					$location.path('/requeststudent');
				else
					$location.path('/requesttutor');
			}, function(res){
				console.warn('login response', res);
				vm.error = 'Invalid username/email or password';
			})
			.finally(function(){
				vm.loading = false;
			});
		vm.error = null;
		vm.loading = true;
	};

}
]);

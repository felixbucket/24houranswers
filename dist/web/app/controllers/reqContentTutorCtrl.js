app.controller('reqContentTutorCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;
	vm.error = null;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	vm.isPendingQuote = false;
	vm.isPendingAuth = false;

	vm.dueDateDiff = function(){
		if ( vm.isLoading ) return '';
		if ( vm.data.request.dueDate.isAfter() )
			return '0 days 0 hours 0 minutes';

		var dur = moment.duration( moment().diff(vm.data.request.dueDate) );
		return dur.days() +' days '+ dur.hours() +' hours '+ dur.minutes() +' minutes';
	};


	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: reqType},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/archive.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};
	vm.releaseRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/release.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};

	vm.quote = {
		value: 0,
		visible: false,
		submit: function(){
			if ( ! /^[0-9]+$/.test(vm.quote.value) )
				return angular.element('[ng-model="vm.quote.value"]').focus() && 0;

			apiRequest({
					method: 'PUT',
					url: '/quote.json',
					data: {type: reqType, requestid: reqID, quote: parseInt(vm.quote.value)}
				}).then(function(res){
					vm.refresh();
				});
		}
	};


	function normalize(data){
		var req = data.request;
		if ( req.tabletype === 'solutionrequests' )
			req.ids = 'SR'+ req.id;
		else if ( req.tabletype === 'liverequests' )
			req.ids = 'LR'+ req.id;

		req.date = moment(req.date);
		req.dateArchived = moment(req.dateArchived);
		req.dateAssigned = moment(req.dateAssigned);
		req.dateCanceled = moment(req.dateCanceled);
		req.dateCompleted = moment(req.dateCompleted);
		req.dueDate = moment(req.dueDate);
		req.lastStudentPost = moment(req.lastStudentPost);
		req.lastTutorPost = moment(req.lastTutorPost);

		req.quote = parseFloat( req.quote );

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		req.additionalInfo = $sce.trustAsHtml(req.additionalInfo);

		$.each(data.posts, function(i, d){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);
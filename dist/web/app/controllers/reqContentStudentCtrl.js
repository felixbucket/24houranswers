app.controller('reqContentStudentCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: ids.substr(0, 2)},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		apiRequest({
				method: 'PUT',
				url: '/archive.json',
				data: {type: reqType, requestid: reqID}
			}).then(function(res){
				vm.refresh();
			});
	};


	function normalize(data){
		data = angular.copy(data);
		if ( data.request.tabletype === 'solutionrequests' )
			data.request.ids = 'SR'+ data.request.id;
		else if ( data.request.tabletype === 'liverequests' )
			data.request.ids = 'LR'+ data.request.id;

		data.request.date = moment(data.request.date);
		data.request.dateArchived = moment(data.request.dateArchived);
		data.request.dateAssigned = moment(data.request.dateAssigned);
		data.request.dateCanceled = moment(data.request.dateCanceled);
		data.request.dateCompleted = moment(data.request.dateCompleted);
		data.request.dueDate = moment(data.request.dueDate);
		data.request.lastStudentPost = moment(data.request.lastStudentPost);
		data.request.lastTutorPost = moment(data.request.lastTutorPost);

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		data.request.additionalInfo = $sce.trustAsHtml(data.request.additionalInfo);

		angular.forEach(data.posts, function(d, i){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);

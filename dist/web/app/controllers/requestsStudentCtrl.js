app.controller('requestsStudentCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	vm.fields = {
		'Show all requests': {days_ago: -1, show_archived: false},
		'Show last 15 days': {days_ago: 15, show_archived: false},
		'Show last 60 days': {days_ago: 60, show_archived: false},
		'Show last 90 days': {days_ago: 90, show_archived: false},
		'Show archived requests': {days_ago: -1, show_archived: true}
	};

	var defaultParams = vm.fields['Show last 15 days'];

	var payload = {
		method: 'GET',
		url: '/studentall.json'
	};
	vm.params = defaultParams;

	(vm.refresh = function(){
		payload.params = angular.copy(vm.params);
		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data, function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();

	vm.reset = function(form){
		vm.params = defaultParams;
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm.reset.$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		return data;
	}
}
]);

app.controller('hwHelpCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){
    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;

    $scope.hw = {};
    $scope.hw.type = "SR";
    $scope.hw.duedate = "";
    $scope.hw.timezone = jstz().timezone_name;
    $scope.hw.subjectid = "";
    $scope.hw.grade = "";
    $scope.hw.fast = "normal";
    $scope.hw.tutorname = "";
    $scope.hw.discountcode = "";
    $scope.hw.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.hw.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.hw.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendHw = function(){
        console.log($scope.hw);
        /*
        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.hw
    		}).then(function successCallback(response){

            //todo - proper redirection if student or tutor

            if(userAcct.usertype() === "student"){
              $location.path('/requeststudent');
            } else {
              $location.path('/requeststudent');
            }
        });*/
    }

    $("#datepickerHw").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.hw.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerHw").on("dp.change",function(e){
       var selTime =  moment($scope.inittime, ["h:mm A"]).format("HH:mm");
       //$scope.inittime = moment($(this).val(), ["h:mm A"]).format("HH:mm");
       $scope.hw.duedate = $scope.initdate+" "+selTime+":00";
    });


});

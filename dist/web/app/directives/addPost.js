app.directive('addPost', ['apiRequest', function(apiRequest){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/addPost.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-add-post',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'addPost',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'POST',
					url: '/addpost.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.file = undefined;
				vm.comment = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.comment.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.comment = vm.comment.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.addPostSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);
app.directive('cancelRequest', ['apiRequest', 'cons', function(apiRequest, cons){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/cancelRequestReason.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-cancel-request',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'cancelRequest',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'PUT',
					url: '/cancel.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.reason = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.reason.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.reason = vm.reason.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.cancelRequestSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);

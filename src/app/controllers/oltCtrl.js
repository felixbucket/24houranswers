app.controller('oltCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){

    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;
    $scope.lengthlist = cons.lengthSession;

    $scope.olt = {};
    $scope.olt.type = "LR";
    $scope.olt.duedate = "";
    $scope.olt.timezone = jstz().timezone_name;
    $scope.olt.length = "30 minutes";
    $scope.olt.subjectid = "";
    $scope.olt.grade = "";
    $scope.olt.fast = "normal";
    $scope.olt.tutorname = "";
    $scope.olt.discountcode = "";
    $scope.olt.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.olt.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.olt.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendOlt = function(){
        console.log($scope.olt);
        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.olt
    		}).then(function successCallback(response){
           
             //todo - proper redirection if student or tutor

           if(userAcct.usertype() === "student"){
             $location.path('/requeststudent');
           } else {
             $location.path('/requeststudent');
           }
        });
    }

    $("#datepickerOlt").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.olt.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerOlt").on("dp.change",function(e){
      $scope.inittime = $(this).val();

      var selTime =  moment($(this).val(), ["h:mm A"]).format("HH:mm");
      $scope.olt.duedate = $scope.initdate+" "+selTime+":00";
    });


});

app.controller('requestsTutorCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	var defaultParams = {
		order_by: 'dueDate',
		order_by_direction: 'DESC',
		pipeline: 'no-pipeline',
		table: 'all',
		status: 'active',
		archived: 'unarchived',
		acct: -1,
		keyword: ''
	};

	var payload = {
		method: 'GET',
		url: '/tutorall.json',
		params: vm.params = angular.copy(defaultParams)
	};

	(vm.refresh = function(){
		payload.params.keyword = payload.params.keyword.trim();

		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data.requests || [], function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();


	vm.pipeline = false;
	vm.togglePipeline = function(){
		vm.pipeline = !vm.pipeline;
		vm.params.pipeline = vm.pipeline ? 'pipeline-only' : 'no-pipeline';
		vm.refresh();
	};
	vm.sort = {
		order_by: {
			'time_unattended': 'Time Unattended',
			'date': 'Date',
			'dueDate': 'Due Date'
		},
		order_by_direction: {
			'DESC': 'Descending',
			'ASC': 'Ascending'
		}
	};
	vm.filter = {
		status: {
			'active': 'Active',
			'Pending Quote': 'Pending Quote',
			'Pending Authorization': 'Pending Authorization',
			'Pending Completion': 'Pending Completion',
			'Completed': 'Completed',
			'Canceled': 'Canceled'
		},
		table: {
			'all': 'All',
			'solutionrequests': 'Solutions Requests',
			'liverequests': 'Live Requests'
		},
		archived: {
			'all': 'Archived & Unarchived',
			'archived': 'Archived',
			'unarchived': 'Unarchived',
		}
	};

	vm.sortReset = function(form){
		for( var k in vm.sort )
			if ( vm.sort.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.filterReset = function(form){
		for( var k in vm.filter )
			if ( vm.filter.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm[form.$name +'Reset'].$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		data.unattended = Math.ceil(data.time_unattended / 60 / 24);
		if ( data.unattended > 1 )
			data.unattended += ' days';
		else
			data.unattended += ' day';
		return data;
	}
}
]);

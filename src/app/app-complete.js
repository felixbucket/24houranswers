'use strict';

var app = angular.module('appTwoFour', ['ngRoute', 'angularMoment']);

//Constant value declaration section
app.constant("cons",{
	"url":"",
	"port":"",
	"pagepath":"app/pages/",
	"apibase": "http://ryanec2.dev.24houranswers.com/api",
	"gradeLevel" : ['Graduate Student','College Undergraduate','High School','Junior High','Elementary'],
	"lengthSession" : ['30 minutes','45 minutes','1 hour','2 hours','3 hours','4 hours','5 hours']
});

app.constant('jquery', window.jQuery);
//app.constant('GradeLevel',['Graduate Student','College Undergraduate','High School','Junior High','Elementary']);

app.run(['moment', function(moment){
	moment.defaultFormat = 'MM/DD/YYYY h:ssA z';
	moment.tz.setDefault('EST');
}]);

//Module configuration includes routing of pages
app.config(['$routeProvider','cons',
	function($routeProvider,cons) {
		$routeProvider
			.when('/login', {
				templateUrl: cons.pagepath+'login.tpl.html',
				controller: 'loginCtrl',
				controllerAs: 'vm',
				resolve: {
					/*auth: ['$location', 'userAcct', function($location, userAcct){
						if ( userAcct.isValid() ) {
							if ( userAcct.isStudent() )
								$location.path('/requeststudent');
							else if ( userAcct.isTutor() )
								$location.path('/requesttutor');
						}
					}]*/
				}
			})
			.when('/createaccount', {
				templateUrl: cons.pagepath+'createaccount.tpl.html'
			})
			.when('/accountinfo', {
				templateUrl: cons.pagepath+'accountinfo.tpl.html'
			})
			.when('/contactus', {
				templateUrl: cons.pagepath+'contactus.tpl.html'
			})
			.when('/choosepayment', {
				templateUrl: cons.pagepath+'choosepayment.tpl.html'
			})
			.when('/confirmsavedpayment', {
				templateUrl: cons.pagepath+'confirmsavedpayment.tpl.html'
			})
			.when('/homeworkhelp', {
				templateUrl: cons.pagepath+'homeworkhelp.tpl.html',
				controller : "hwHelpCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/onlinetutoring', {
				templateUrl: cons.pagepath+'onlinetutoring.tpl.html',
				controller : "oltCtrl",
				resolve: {auth: requireAuth}
			})
			.when('/cardinfo', {
				templateUrl: cons.pagepath+'cardinfo.tpl.html'
			})
			.when('/requesttutor', {
				templateUrl: cons.pagepath+'requesttutor.tpl.html',
				controller: 'requestsTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontenttutor/:ids', {
				templateUrl: cons.pagepath+'reqcontenttutor.tpl.html',
				controller: 'reqContentTutorCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/requeststudent', {
				templateUrl: cons.pagepath+'requeststudent.tpl.html',
				controller: 'requestsStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.when('/reqcontentstudent/:ids', {
				templateUrl: cons.pagepath+'reqcontentstudent.tpl.html',
				controller: 'reqContentStudentCtrl',
				controllerAs: 'vm',
				resolve: {auth: requireAuth}
			})
			.otherwise({
				redirectTo: '/login'
			});

		requireAuth.$inject = ['$location', 'userAcct'];
		function requireAuth($location, userAcct){
			if (!userAcct.isValid() )
				$location.path('/login');
		}
	}
]);

app.run(['$rootScope', function($rootScope){
	$rootScope.$on('$routeChangeStart', function(e, next, cur){
		console.info( 'routeChangeStart', cur && cur.$$route && cur.$$route.originalPath, '->', next && next.$$route && next.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeSuccess', function(e, cur, prev){
		console.info( 'routeChangeSuccess', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath );
	});
	$rootScope.$on('$routeChangeError', function(e, cur, prev, reject){
		console.error( 'routeChangeError', prev && prev.$$route && prev.$$route.originalPath, '->', cur && cur.$$route && cur.$$route.originalPath, reject );
	});
}])

app.controller('hwHelpCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){
    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;

    $scope.hw = {};
    $scope.hw.type = "SR";
    $scope.hw.duedate = "";
    $scope.hw.timezone = jstz().timezone_name;
    $scope.hw.subjectid = "";
    $scope.hw.grade = "";
    $scope.hw.fast = "normal";
    $scope.hw.tutorname = "";
    $scope.hw.discountcode = "";
    $scope.hw.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.hw.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.hw.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendHw = function(){
        console.log($scope.hw);

        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.hw
    		}).then(function successCallback(response){

            //todo - proper redirection if student or tutor

            if(userAcct.usertype() === "student"){
              $location.path('/requeststudent');
            } else {
              $location.path('/requeststudent');
            }
        });
    }

    $("#datepickerHw").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.hw.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerHw").on("dp.change",function(e){
       $scope.inittime = $(this).val();

       var selTime =  moment($(this).val(), ["h:mm A"]).format("HH:mm");
       $scope.hw.duedate = $scope.initdate+" "+selTime+":00";
    });


});

app.controller('listCtrl',function($scope){
    $scope.isUsa = false;
    
    $scope.countrySelected = function(countryval){
      if(countryval=='US')$scope.isUsa = true;
      else $scope.isUsa = false;
    };
})

app.controller('loginCtrl', ['$scope', '$http', '$location', 'userAcct', 'cons',
function($scope, $http, $location, userAcct, cons){
	var vm = this,
		form = $scope.form;

	vm.error = null;
	vm.loading = false;


  userAcct().clear();

	vm.submit = function(){
		var data = {
			username: vm.email,
			password: vm.password
		};

		$http({
			method: 'POST',
			url: cons.apibase +'/apilogin.json',
			data: data
		})
			.then(function(res){
				console.info('login response', res);
				var usertype = res.data;
				userAcct().login(data.username, usertype, data.password);
				if ( usertype === 'student' )
					$location.path('/requeststudent');
				else
					$location.path('/requesttutor');
			}, function(res){
				console.warn('login response', res);
				vm.error = 'Invalid username/email or password';
			})
			.finally(function(){
				vm.loading = false;
			});
		vm.error = null;
		vm.loading = true;
	};

}
]);

app.controller('oltCtrl',function($scope,$http,$location,cons,getSubjects,userAcct){

    $scope.initdate = "";
    $scope.inittime = "";
    $scope.timezone = moment().format('Z')+" - "+jstz().timezone_name;
    $scope.gradelvllist = cons.gradeLevel;
    $scope.lengthlist = cons.lengthSession;

    $scope.olt = {};
    $scope.olt.type = "LR";
    $scope.olt.duedate = "";
    $scope.olt.timezone = jstz().timezone_name;
    $scope.olt.length = "30 minutes";
    $scope.olt.subjectid = "";
    $scope.olt.grade = "";
    $scope.olt.fast = "normal";
    $scope.olt.tutorname = "";
    $scope.olt.discountcode = "";
    $scope.olt.comment = "";

    $scope.subjectSel = "";
    $scope.subjectlist = [];

    getSubjects.list().then(function successCallback(response){
      $scope.subjectlist = response.data;
      $scope.formatsubj = [];

      angular.forEach($scope.subjectlist, function(items, key){
        for(var a=0;a < items.subjects.length;a++){
          items.subjects[a].groupid = items.groupid;
          items.subjects[a].grouptitle = items.grouptitle;
          $scope.formatsubj.push(items.subjects[a]);
        }
      });
    });

    $scope.switchSubject = function(){
      $scope.olt.subjectid = "";
      if($scope.subjectSel !== undefined){
        $scope.olt.subjectid = $scope.subjectSel.id;
      }
    };

    $scope.sendOlt = function(){
        console.log($scope.olt);
        $http({
    			method: 'POST',
    			url: cons.apibase +'/apirequest.json',
    			data: $scope.olt
    		}).then(function successCallback(response){
           
             //todo - proper redirection if student or tutor

           if(userAcct.usertype() === "student"){
             $location.path('/requeststudent');
           } else {
             $location.path('/requeststudent');
           }
        });
    }

    $("#datepickerOlt").on("dp.change",function(e){
       $scope.initdate = $(this).val();
       $scope.olt.duedate = $scope.initdate+" "+$scope.inittime+":00";
    });

    $("#timepickerOlt").on("dp.change",function(e){
      $scope.inittime = $(this).val();

      var selTime =  moment($(this).val(), ["h:mm A"]).format("HH:mm");
      $scope.olt.duedate = $scope.initdate+" "+selTime+":00";
    });


});

app.controller('reqContentStudentCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: ids.substr(0, 2)},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		apiRequest({
				method: 'PUT',
				url: '/archive.json',
				data: {type: reqType, requestid: reqID}
			}).then(function(res){
				vm.refresh();
			});
	};


	function normalize(data){
		data = angular.copy(data);
		if ( data.request.tabletype === 'solutionrequests' )
			data.request.ids = 'SR'+ data.request.id;
		else if ( data.request.tabletype === 'liverequests' )
			data.request.ids = 'LR'+ data.request.id;

		data.request.date = moment(data.request.date);
		data.request.dateArchived = moment(data.request.dateArchived);
		data.request.dateAssigned = moment(data.request.dateAssigned);
		data.request.dateCanceled = moment(data.request.dateCanceled);
		data.request.dateCompleted = moment(data.request.dateCompleted);
		data.request.dueDate = moment(data.request.dueDate);
		data.request.lastStudentPost = moment(data.request.lastStudentPost);
		data.request.lastTutorPost = moment(data.request.lastTutorPost);

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		data.request.additionalInfo = $sce.trustAsHtml(data.request.additionalInfo);

		angular.forEach(data.posts, function(d, i){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);

app.controller('reqContentTutorCtrl', ['$routeParams', '$sce', 'userAcct', 'apiRequest', 'moment',
function($routeParams, $sce, userAcct, apiRequest, moment){
	var vm = this;

	vm.data = null;
	vm.isLoading = true;
	vm.error = null;

	vm.localize = function(user){
		if ( user && user === userAcct.username() )
			return 'You';
		return user;
	};

	vm.isPendingQuote = false;
	vm.isPendingAuth = false;

	vm.dueDateDiff = function(){
		if ( vm.isLoading ) return '';
		if ( vm.data.request.dueDate.isAfter() )
			return '0 days 0 hours 0 minutes';

		var dur = moment.duration( moment().diff(vm.data.request.dueDate) );
		return dur.days() +' days '+ dur.hours() +' hours '+ dur.minutes() +' minutes';
	};


	var ids = apiRequest.ids = $routeParams.ids.toUpperCase(),
		reqID = apiRequest.id = ids.substr(2),
		reqType = apiRequest.type = ids.substr(0, 2),
		url = '.json/'+ reqID,
		params = {type: reqType},
		payload = {method: 'GET', url: url, params: params};


	(vm.refresh = function(){
		return apiRequest(payload)
			.then(function(res){
				vm.data = normalize(res.data);
				vm.isPendingQuote = vm.data.request.status.toLowerCase() === 'pending quote';
				vm.isPendingAuth = vm.data.request.status.toLowerCase() === 'pending authorization';
				vm.isCancelled = vm.data.request.status.toLowerCase() === 'canceled';
				vm.isCompleted = vm.data.request.status.toLowerCase() === 'completed';
				vm.isLoading = false;
				return res;
			}, function(res){
				vm.error = res.data.error;
				vm.isLoading = false;
				throw vm.error;
			});
	})();

	vm.archiveRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/archive.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};
	vm.releaseRequest = function(){
		if ( userAcct.username() === vm.data.request.tutor )
			apiRequest({
					method: 'PUT',
					url: '/release.json',
					data: {type: reqType, requestid: reqID}
				}).then(function(res){
					vm.refresh();
				});
	};

	vm.quote = {
		value: 0,
		visible: false,
		submit: function(){
			if ( ! /^[0-9]+$/.test(vm.quote.value) )
				return angular.element('[ng-model="vm.quote.value"]').focus() && 0;

			apiRequest({
					method: 'PUT',
					url: '/quote.json',
					data: {type: reqType, requestid: reqID, quote: parseInt(vm.quote.value)}
				}).then(function(res){
					vm.refresh();
				});
		}
	};


	function normalize(data){
		var req = data.request;
		if ( req.tabletype === 'solutionrequests' )
			req.ids = 'SR'+ req.id;
		else if ( req.tabletype === 'liverequests' )
			req.ids = 'LR'+ req.id;

		req.date = moment(req.date);
		req.dateArchived = moment(req.dateArchived);
		req.dateAssigned = moment(req.dateAssigned);
		req.dateCanceled = moment(req.dateCanceled);
		req.dateCompleted = moment(req.dateCompleted);
		req.dueDate = moment(req.dueDate);
		req.lastStudentPost = moment(req.lastStudentPost);
		req.lastTutorPost = moment(req.lastTutorPost);

		req.quote = parseFloat( req.quote );

		data.subjects = $sce.trustAsHtml(data.subjects.join('; '));
		req.additionalInfo = $sce.trustAsHtml(req.additionalInfo);

		$.each(data.posts, function(i, d){
			d.date = moment(d.date);
			d.is_note = (d.is_note === '1');
		});

		return data;
	}
}
]);
app.controller('requestsStudentCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	vm.fields = {
		'Show all requests': {days_ago: -1, show_archived: false},
		'Show last 15 days': {days_ago: 15, show_archived: false},
		'Show last 60 days': {days_ago: 60, show_archived: false},
		'Show last 90 days': {days_ago: 90, show_archived: false},
		'Show archived requests': {days_ago: -1, show_archived: true}
	};

	var defaultParams = vm.fields['Show last 15 days'];

	var payload = {
		method: 'GET',
		url: '/studentall.json'
	};
	vm.params = defaultParams;

	(vm.refresh = function(){
		payload.params = angular.copy(vm.params);
		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data, function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();

	vm.reset = function(form){
		vm.params = defaultParams;
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm.reset.$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		return data;
	}
}
]);

app.controller('requestsTutorCtrl', ['apiRequest', 'moment',
function(apiRequest, moment){
	var vm = this;

	vm.list = [];
	vm.isLoading = false;

	var defaultParams = {
		order_by: 'dueDate',
		order_by_direction: 'DESC',
		pipeline: 'no-pipeline',
		table: 'all',
		status: 'active',
		archived: 'unarchived',
		acct: -1,
		keyword: ''
	};

	var payload = {
		method: 'GET',
		url: '/tutorall.json',
		params: vm.params = angular.copy(defaultParams)
	};

	(vm.refresh = function(){
		payload.params.keyword = payload.params.keyword.trim();

		apiRequest(payload)
			.then(function(res){
				vm.list = [];
				angular.forEach(res.data.requests || [], function(d){
					vm.list.push(normalizeData(d));
				});
			})
			.finally(function(){
				vm.isLoading = false;
			});
		vm.isLoading = true;
	})();


	vm.pipeline = false;
	vm.togglePipeline = function(){
		vm.pipeline = !vm.pipeline;
		vm.params.pipeline = vm.pipeline ? 'pipeline-only' : 'no-pipeline';
		vm.refresh();
	};
	vm.sort = {
		order_by: {
			'time_unattended': 'Time Unattended',
			'date': 'Date',
			'dueDate': 'Due Date'
		},
		order_by_direction: {
			'DESC': 'Descending',
			'ASC': 'Ascending'
		}
	};
	vm.filter = {
		status: {
			'active': 'Active',
			'Pending Quote': 'Pending Quote',
			'Pending Authorization': 'Pending Authorization',
			'Pending Completion': 'Pending Completion',
			'Completed': 'Completed',
			'Canceled': 'Canceled'
		},
		table: {
			'all': 'All',
			'solutionrequests': 'Solutions Requests',
			'liverequests': 'Live Requests'
		},
		archived: {
			'all': 'Archived & Unarchived',
			'archived': 'Archived',
			'unarchived': 'Unarchived',
		}
	};

	vm.sortReset = function(form){
		for( var k in vm.sort )
			if ( vm.sort.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.filterReset = function(form){
		for( var k in vm.filter )
			if ( vm.filter.hasOwnProperty(k) )
				vm.params[k] = defaultParams[k];
		form.$setPristine(true);
		vm.submit(form);
	};
	vm.submit = function(form){
		if ( !! form ) {
			angular.element('form[name="'+ form.$name +'"]').parents('.modal').modal('hide');
			vm[form.$name +'Reset'].$dirty = form.$dirty;
		}
		vm.refresh();
	};


	function normalizeData(data){
		if ( data.tabletype === 'solutionrequests' ) {
			data.tabletype = 'Solutions Request';
			data.ids = 'SR'+ data.id;
		} else
		if ( data.tabletype === 'liverequests' ) {
			data.tabletype = 'Live Request';
			data.ids = 'LR'+ data.id;
		}

		data.date = moment(data.date);
		data.dueDate = moment(data.dueDate);
		data.lastTutorPost = moment(data.lastTutorPost);
		data.unattended = Math.ceil(data.time_unattended / 60 / 24);
		if ( data.unattended > 1 )
			data.unattended += ' days';
		else
			data.unattended += ' day';
		return data;
	}
}
]);

app.controller('zoneCtrl', function($scope) {
   $scope.currentTZ = moment().format('Z');
 });

app.directive('addPost', ['apiRequest', function(apiRequest){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/addPost.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-add-post',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'addPost',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'POST',
					url: '/addpost.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.file = undefined;
				vm.comment = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.comment.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.comment = vm.comment.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.addPostSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);
app.directive('cancelRequest', ['apiRequest', 'cons', function(apiRequest, cons){
	return {
		restrict: 'EA',
		scope: {},
		templateUrl: 'app/pages/modal/cancelRequestReason.tpl.html',
		link: function($scope, elem, attr){
			elem.attr({
				'id': 'modal-cancel-request',
				'class': 'modal fade',
				'tabindex': '-1',
				'role': 'dialog',
				'aria-labelledby': 'cancelRequest',
				'aria-hidden': 'true'
			}).on('show.bs.modal hidden.bs.modal', reset);

			var vm = $scope.vm = {submit: submit},
				payload = {
					method: 'PUT',
					url: '/cancel.json',
					data: {type: apiRequest.type, requestid: apiRequest.id}
				};

			function reset(){
				vm.reason = '';
				vm.errorMsg = null;
				vm.isLoading = false;
			}
			function submit(){
				if ( vm.reason.trim() === '' )// && ! vm.file )
					return elem.find('textarea').focus();

				payload.data.reason = vm.reason.trim();
				apiRequest(payload)
					.then(function(res){
						$scope.$parent.$eval(attr.cancelRequestSuccess);
						elem.modal('hide');
						reset();
					}, function(res){
						vm.errorMsg = 'An error occured while posting';
					})
					.finally(function(){
						vm.isLoading = false;
					});
				vm.isLoading = true;
			}
		}
	}
}]);

'use strict';

app.directive('headernav', function (cons) {
  console.log(cons);

    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        templateUrl: cons.pagepath+"headernav.tpl.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});

app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel);

                //get the value of the other password
                var e2 = scope.$eval(attrs.passwordMatch);
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {
                //set the form control to valid if both
                //passwords are the same, else invalid
                control.$setValidity("pwmatch", n);
            });
        }
    };
}]);

app.factory('apiRequest', ['$http', 'cons', 'userAcct', function($http, cons, userAcct){
	return function(payload){
		payload = angular.copy(payload);
		payload.url = cons.apibase +'/apirequest'+ payload.url;

		console.info( payload.method, payload.url, payload.params, payload.data );

		// todo: remove this when basic auth is fixed from api
		payload.url = payload.url.replace('http://', 'http://'+ userAcct.username() +':'+ userAcct.password() +'@');

		return $http(payload)
			.then(function(res){
				console.info( res.status, res.config.url, res.data );
				return res;
			}, function(res){
				console.error( res.status, res.config.url, res.data );
				res.data = res.data || {error: {code: res.status, message: res.statusText}}
				throw res;
			});
	}
}]);
app.factory('getSubjects', function($http,cons){
    return {
      list : function(){
        return $http({
          method: 'GET',
          url: 	cons.apibase +'/apisubjects.json/'
        });
      }
    }
});

/* User Account Service

to check for user credentials:
	function(userAcct){
		console.log( userAcct.isValid() );
		console.log( userAcct.isStudent() );
		console.log( userAcct.isTutor() );
		console.log( userAcct.username() );
		console.log( userAcct.usertype() );
	}

to set login credentials:
	function(userAcct){
		userAcct().login('john', 'student');
	}
*/
app.factory('userAcct', ['$window', function($window){
	var loc = (function(){
		var s = $window.localStorage.getItem('user');
		return s ? JSON.parse(s) : null;
	})() || {
		username: null,
		type: null
	};


	function svc(){
		return {
			login: function( username, type, password ){//for now
				loc.username = username;
				loc.type = type;
				loc.password = password;//temp only
				try{
					$window.localStorage.setItem('user', JSON.stringify(loc));
				}catch(e){};
			},
			clear: function(){
				loc.username = null;
				loc.type = null;
			}
		};
	}
	svc.isValid = function(){
		return !! loc.username;
	};
	svc.isStudent = function(){
		return loc.type === 'student';
	};
	svc.isTutor = function(){
		return loc.type === 'tutor';
	};
	svc.isAdmin = function(){
		return loc.type === 'admin';
	};
	svc.username = function(){
		return loc.username;
	};
	svc.password = function(){
		return loc.password;
	};
	svc.usertype = function(){
		return loc.type;
	};

	return svc;
}])

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImNvbnRyb2xsZXJzL2h3SGVscEN0cmwuanMiLCJjb250cm9sbGVycy9saXN0Q3RybC5qcyIsImNvbnRyb2xsZXJzL2xvZ2luQ3RybC5qcyIsImNvbnRyb2xsZXJzL29sdEN0cmwuanMiLCJjb250cm9sbGVycy9yZXFDb250ZW50U3R1ZGVudEN0cmwuanMiLCJjb250cm9sbGVycy9yZXFDb250ZW50VHV0b3JDdHJsLmpzIiwiY29udHJvbGxlcnMvcmVxdWVzdHNTdHVkZW50Q3RybC5qcyIsImNvbnRyb2xsZXJzL3JlcXVlc3RzVHV0b3JDdHJsLmpzIiwiY29udHJvbGxlcnMvem9uZUN0cmwuanMiLCJkaXJlY3RpdmVzL2FkZFBvc3QuanMiLCJkaXJlY3RpdmVzL2NhbmNlbFJlcXVlc3QuanMiLCJkaXJlY3RpdmVzL2hlYWRlcm5hdi5qcyIsImRpcmVjdGl2ZXMvcGFzc3dvcmRNYXRjaC5qcyIsInNlcnZpY2VzL2FwaVJlcXVlc3QuanMiLCJzZXJ2aWNlcy9nZXRTdWJqZWN0cy5qcyIsInNlcnZpY2VzL3VzZXJBY2N0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDN0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDeEhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDckVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1SEE7QUFDQTtBQUNBO0FBQ0E7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic3JjL2FwcC9hcHAtY29tcGxldGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnYXBwVHdvRm91cicsIFsnbmdSb3V0ZScsICdhbmd1bGFyTW9tZW50J10pO1xuXG4vL0NvbnN0YW50IHZhbHVlIGRlY2xhcmF0aW9uIHNlY3Rpb25cbmFwcC5jb25zdGFudChcImNvbnNcIix7XG5cdFwidXJsXCI6XCJcIixcblx0XCJwb3J0XCI6XCJcIixcblx0XCJwYWdlcGF0aFwiOlwiYXBwL3BhZ2VzL1wiLFxuXHRcImFwaWJhc2VcIjogXCJodHRwOi8vcnlhbmVjMi5kZXYuMjRob3VyYW5zd2Vycy5jb20vYXBpXCIsXG5cdFwiZ3JhZGVMZXZlbFwiIDogWydHcmFkdWF0ZSBTdHVkZW50JywnQ29sbGVnZSBVbmRlcmdyYWR1YXRlJywnSGlnaCBTY2hvb2wnLCdKdW5pb3IgSGlnaCcsJ0VsZW1lbnRhcnknXSxcblx0XCJsZW5ndGhTZXNzaW9uXCIgOiBbJzMwIG1pbnV0ZXMnLCc0NSBtaW51dGVzJywnMSBob3VyJywnMiBob3VycycsJzMgaG91cnMnLCc0IGhvdXJzJywnNSBob3VycyddXG59KTtcblxuYXBwLmNvbnN0YW50KCdqcXVlcnknLCB3aW5kb3cualF1ZXJ5KTtcbi8vYXBwLmNvbnN0YW50KCdHcmFkZUxldmVsJyxbJ0dyYWR1YXRlIFN0dWRlbnQnLCdDb2xsZWdlIFVuZGVyZ3JhZHVhdGUnLCdIaWdoIFNjaG9vbCcsJ0p1bmlvciBIaWdoJywnRWxlbWVudGFyeSddKTtcblxuYXBwLnJ1bihbJ21vbWVudCcsIGZ1bmN0aW9uKG1vbWVudCl7XG5cdG1vbWVudC5kZWZhdWx0Rm9ybWF0ID0gJ01NL0REL1lZWVkgaDpzc0Egeic7XG5cdG1vbWVudC50ei5zZXREZWZhdWx0KCdFU1QnKTtcbn1dKTtcblxuLy9Nb2R1bGUgY29uZmlndXJhdGlvbiBpbmNsdWRlcyByb3V0aW5nIG9mIHBhZ2VzXG5hcHAuY29uZmlnKFsnJHJvdXRlUHJvdmlkZXInLCdjb25zJyxcblx0ZnVuY3Rpb24oJHJvdXRlUHJvdmlkZXIsY29ucykge1xuXHRcdCRyb3V0ZVByb3ZpZGVyXG5cdFx0XHQud2hlbignL2xvZ2luJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnbG9naW4udHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAnbG9naW5DdHJsJyxcblx0XHRcdFx0Y29udHJvbGxlckFzOiAndm0nLFxuXHRcdFx0XHRyZXNvbHZlOiB7XG5cdFx0XHRcdFx0LyphdXRoOiBbJyRsb2NhdGlvbicsICd1c2VyQWNjdCcsIGZ1bmN0aW9uKCRsb2NhdGlvbiwgdXNlckFjY3Qpe1xuXHRcdFx0XHRcdFx0aWYgKCB1c2VyQWNjdC5pc1ZhbGlkKCkgKSB7XG5cdFx0XHRcdFx0XHRcdGlmICggdXNlckFjY3QuaXNTdHVkZW50KCkgKVxuXHRcdFx0XHRcdFx0XHRcdCRsb2NhdGlvbi5wYXRoKCcvcmVxdWVzdHN0dWRlbnQnKTtcblx0XHRcdFx0XHRcdFx0ZWxzZSBpZiAoIHVzZXJBY2N0LmlzVHV0b3IoKSApXG5cdFx0XHRcdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0dHV0b3InKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XSovXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NyZWF0ZWFjY291bnQnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydjcmVhdGVhY2NvdW50LnRwbC5odG1sJ1xuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvYWNjb3VudGluZm8nLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydhY2NvdW50aW5mby50cGwuaHRtbCdcblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NvbnRhY3R1cycsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ2NvbnRhY3R1cy50cGwuaHRtbCdcblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2Nob29zZXBheW1lbnQnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydjaG9vc2VwYXltZW50LnRwbC5odG1sJ1xuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvY29uZmlybXNhdmVkcGF5bWVudCcsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ2NvbmZpcm1zYXZlZHBheW1lbnQudHBsLmh0bWwnXG5cdFx0XHR9KVxuXHRcdFx0LndoZW4oJy9ob21ld29ya2hlbHAnLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydob21ld29ya2hlbHAudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyIDogXCJod0hlbHBDdHJsXCIsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL29ubGluZXR1dG9yaW5nJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnb25saW5ldHV0b3JpbmcudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyIDogXCJvbHRDdHJsXCIsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL2NhcmRpbmZvJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsnY2FyZGluZm8udHBsLmh0bWwnXG5cdFx0XHR9KVxuXHRcdFx0LndoZW4oJy9yZXF1ZXN0dHV0b3InLCB7XG5cdFx0XHRcdHRlbXBsYXRlVXJsOiBjb25zLnBhZ2VwYXRoKydyZXF1ZXN0dHV0b3IudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAncmVxdWVzdHNUdXRvckN0cmwnLFxuXHRcdFx0XHRjb250cm9sbGVyQXM6ICd2bScsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL3JlcWNvbnRlbnR0dXRvci86aWRzJywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsncmVxY29udGVudHR1dG9yLnRwbC5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ3JlcUNvbnRlbnRUdXRvckN0cmwnLFxuXHRcdFx0XHRjb250cm9sbGVyQXM6ICd2bScsXG5cdFx0XHRcdHJlc29sdmU6IHthdXRoOiByZXF1aXJlQXV0aH1cblx0XHRcdH0pXG5cdFx0XHQud2hlbignL3JlcXVlc3RzdHVkZW50Jywge1xuXHRcdFx0XHR0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCsncmVxdWVzdHN0dWRlbnQudHBsLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAncmVxdWVzdHNTdHVkZW50Q3RybCcsXG5cdFx0XHRcdGNvbnRyb2xsZXJBczogJ3ZtJyxcblx0XHRcdFx0cmVzb2x2ZToge2F1dGg6IHJlcXVpcmVBdXRofVxuXHRcdFx0fSlcblx0XHRcdC53aGVuKCcvcmVxY29udGVudHN0dWRlbnQvOmlkcycsIHtcblx0XHRcdFx0dGVtcGxhdGVVcmw6IGNvbnMucGFnZXBhdGgrJ3JlcWNvbnRlbnRzdHVkZW50LnRwbC5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ3JlcUNvbnRlbnRTdHVkZW50Q3RybCcsXG5cdFx0XHRcdGNvbnRyb2xsZXJBczogJ3ZtJyxcblx0XHRcdFx0cmVzb2x2ZToge2F1dGg6IHJlcXVpcmVBdXRofVxuXHRcdFx0fSlcblx0XHRcdC5vdGhlcndpc2Uoe1xuXHRcdFx0XHRyZWRpcmVjdFRvOiAnL2xvZ2luJ1xuXHRcdFx0fSk7XG5cblx0XHRyZXF1aXJlQXV0aC4kaW5qZWN0ID0gWyckbG9jYXRpb24nLCAndXNlckFjY3QnXTtcblx0XHRmdW5jdGlvbiByZXF1aXJlQXV0aCgkbG9jYXRpb24sIHVzZXJBY2N0KXtcblx0XHRcdGlmICghdXNlckFjY3QuaXNWYWxpZCgpIClcblx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9sb2dpbicpO1xuXHRcdH1cblx0fVxuXSk7XG5cbmFwcC5ydW4oWyckcm9vdFNjb3BlJywgZnVuY3Rpb24oJHJvb3RTY29wZSl7XG5cdCRyb290U2NvcGUuJG9uKCckcm91dGVDaGFuZ2VTdGFydCcsIGZ1bmN0aW9uKGUsIG5leHQsIGN1cil7XG5cdFx0Y29uc29sZS5pbmZvKCAncm91dGVDaGFuZ2VTdGFydCcsIGN1ciAmJiBjdXIuJCRyb3V0ZSAmJiBjdXIuJCRyb3V0ZS5vcmlnaW5hbFBhdGgsICctPicsIG5leHQgJiYgbmV4dC4kJHJvdXRlICYmIG5leHQuJCRyb3V0ZS5vcmlnaW5hbFBhdGggKTtcblx0fSk7XG5cdCRyb290U2NvcGUuJG9uKCckcm91dGVDaGFuZ2VTdWNjZXNzJywgZnVuY3Rpb24oZSwgY3VyLCBwcmV2KXtcblx0XHRjb25zb2xlLmluZm8oICdyb3V0ZUNoYW5nZVN1Y2Nlc3MnLCBwcmV2ICYmIHByZXYuJCRyb3V0ZSAmJiBwcmV2LiQkcm91dGUub3JpZ2luYWxQYXRoLCAnLT4nLCBjdXIgJiYgY3VyLiQkcm91dGUgJiYgY3VyLiQkcm91dGUub3JpZ2luYWxQYXRoICk7XG5cdH0pO1xuXHQkcm9vdFNjb3BlLiRvbignJHJvdXRlQ2hhbmdlRXJyb3InLCBmdW5jdGlvbihlLCBjdXIsIHByZXYsIHJlamVjdCl7XG5cdFx0Y29uc29sZS5lcnJvciggJ3JvdXRlQ2hhbmdlRXJyb3InLCBwcmV2ICYmIHByZXYuJCRyb3V0ZSAmJiBwcmV2LiQkcm91dGUub3JpZ2luYWxQYXRoLCAnLT4nLCBjdXIgJiYgY3VyLiQkcm91dGUgJiYgY3VyLiQkcm91dGUub3JpZ2luYWxQYXRoLCByZWplY3QgKTtcblx0fSk7XG59XSlcbiIsImFwcC5jb250cm9sbGVyKCdod0hlbHBDdHJsJyxmdW5jdGlvbigkc2NvcGUsJGh0dHAsJGxvY2F0aW9uLGNvbnMsZ2V0U3ViamVjdHMsdXNlckFjY3Qpe1xuICAgICRzY29wZS5pbml0ZGF0ZSA9IFwiXCI7XG4gICAgJHNjb3BlLmluaXR0aW1lID0gXCJcIjtcbiAgICAkc2NvcGUudGltZXpvbmUgPSBtb21lbnQoKS5mb3JtYXQoJ1onKStcIiAtIFwiK2pzdHooKS50aW1lem9uZV9uYW1lO1xuICAgICRzY29wZS5ncmFkZWx2bGxpc3QgPSBjb25zLmdyYWRlTGV2ZWw7XG5cbiAgICAkc2NvcGUuaHcgPSB7fTtcbiAgICAkc2NvcGUuaHcudHlwZSA9IFwiU1JcIjtcbiAgICAkc2NvcGUuaHcuZHVlZGF0ZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LnRpbWV6b25lID0ganN0eigpLnRpbWV6b25lX25hbWU7XG4gICAgJHNjb3BlLmh3LnN1YmplY3RpZCA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmdyYWRlID0gXCJcIjtcbiAgICAkc2NvcGUuaHcuZmFzdCA9IFwibm9ybWFsXCI7XG4gICAgJHNjb3BlLmh3LnR1dG9ybmFtZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmRpc2NvdW50Y29kZSA9IFwiXCI7XG4gICAgJHNjb3BlLmh3LmNvbW1lbnQgPSBcIlwiO1xuXG4gICAgJHNjb3BlLnN1YmplY3RTZWwgPSBcIlwiO1xuICAgICRzY29wZS5zdWJqZWN0bGlzdCA9IFtdO1xuXG4gICAgZ2V0U3ViamVjdHMubGlzdCgpLnRoZW4oZnVuY3Rpb24gc3VjY2Vzc0NhbGxiYWNrKHJlc3BvbnNlKXtcbiAgICAgICRzY29wZS5zdWJqZWN0bGlzdCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAkc2NvcGUuZm9ybWF0c3ViaiA9IFtdO1xuXG4gICAgICBhbmd1bGFyLmZvckVhY2goJHNjb3BlLnN1YmplY3RsaXN0LCBmdW5jdGlvbihpdGVtcywga2V5KXtcbiAgICAgICAgZm9yKHZhciBhPTA7YSA8IGl0ZW1zLnN1YmplY3RzLmxlbmd0aDthKyspe1xuICAgICAgICAgIGl0ZW1zLnN1YmplY3RzW2FdLmdyb3VwaWQgPSBpdGVtcy5ncm91cGlkO1xuICAgICAgICAgIGl0ZW1zLnN1YmplY3RzW2FdLmdyb3VwdGl0bGUgPSBpdGVtcy5ncm91cHRpdGxlO1xuICAgICAgICAgICRzY29wZS5mb3JtYXRzdWJqLnB1c2goaXRlbXMuc3ViamVjdHNbYV0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgICRzY29wZS5zd2l0Y2hTdWJqZWN0ID0gZnVuY3Rpb24oKXtcbiAgICAgICRzY29wZS5ody5zdWJqZWN0aWQgPSBcIlwiO1xuICAgICAgaWYoJHNjb3BlLnN1YmplY3RTZWwgIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICRzY29wZS5ody5zdWJqZWN0aWQgPSAkc2NvcGUuc3ViamVjdFNlbC5pZDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgJHNjb3BlLnNlbmRIdyA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5odyk7XG5cbiAgICAgICAgJGh0dHAoe1xuICAgIFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuICAgIFx0XHRcdHVybDogY29ucy5hcGliYXNlICsnL2FwaXJlcXVlc3QuanNvbicsXG4gICAgXHRcdFx0ZGF0YTogJHNjb3BlLmh3XG4gICAgXHRcdH0pLnRoZW4oZnVuY3Rpb24gc3VjY2Vzc0NhbGxiYWNrKHJlc3BvbnNlKXtcblxuICAgICAgICAgICAgLy90b2RvIC0gcHJvcGVyIHJlZGlyZWN0aW9uIGlmIHN0dWRlbnQgb3IgdHV0b3JcblxuICAgICAgICAgICAgaWYodXNlckFjY3QudXNlcnR5cGUoKSA9PT0gXCJzdHVkZW50XCIpe1xuICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlcXVlc3RzdHVkZW50Jyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlcXVlc3RzdHVkZW50Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgICQoXCIjZGF0ZXBpY2tlckh3XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgJHNjb3BlLmluaXRkYXRlID0gJCh0aGlzKS52YWwoKTtcbiAgICAgICAkc2NvcGUuaHcuZHVlZGF0ZSA9ICRzY29wZS5pbml0ZGF0ZStcIiBcIiskc2NvcGUuaW5pdHRpbWUrXCI6MDBcIjtcbiAgICB9KTtcblxuICAgICQoXCIjdGltZXBpY2tlckh3XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgJHNjb3BlLmluaXR0aW1lID0gJCh0aGlzKS52YWwoKTtcblxuICAgICAgIHZhciBzZWxUaW1lID0gIG1vbWVudCgkKHRoaXMpLnZhbCgpLCBbXCJoOm1tIEFcIl0pLmZvcm1hdChcIkhIOm1tXCIpO1xuICAgICAgICRzY29wZS5ody5kdWVkYXRlID0gJHNjb3BlLmluaXRkYXRlK1wiIFwiK3NlbFRpbWUrXCI6MDBcIjtcbiAgICB9KTtcblxuXG59KTtcbiIsImFwcC5jb250cm9sbGVyKCdsaXN0Q3RybCcsZnVuY3Rpb24oJHNjb3BlKXtcbiAgICAkc2NvcGUuaXNVc2EgPSBmYWxzZTtcbiAgICBcbiAgICAkc2NvcGUuY291bnRyeVNlbGVjdGVkID0gZnVuY3Rpb24oY291bnRyeXZhbCl7XG4gICAgICBpZihjb3VudHJ5dmFsPT0nVVMnKSRzY29wZS5pc1VzYSA9IHRydWU7XG4gICAgICBlbHNlICRzY29wZS5pc1VzYSA9IGZhbHNlO1xuICAgIH07XG59KVxuIiwiYXBwLmNvbnRyb2xsZXIoJ2xvZ2luQ3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICd1c2VyQWNjdCcsICdjb25zJyxcbmZ1bmN0aW9uKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgdXNlckFjY3QsIGNvbnMpe1xuXHR2YXIgdm0gPSB0aGlzLFxuXHRcdGZvcm0gPSAkc2NvcGUuZm9ybTtcblxuXHR2bS5lcnJvciA9IG51bGw7XG5cdHZtLmxvYWRpbmcgPSBmYWxzZTtcblxuXG4gIHVzZXJBY2N0KCkuY2xlYXIoKTtcblxuXHR2bS5zdWJtaXQgPSBmdW5jdGlvbigpe1xuXHRcdHZhciBkYXRhID0ge1xuXHRcdFx0dXNlcm5hbWU6IHZtLmVtYWlsLFxuXHRcdFx0cGFzc3dvcmQ6IHZtLnBhc3N3b3JkXG5cdFx0fTtcblxuXHRcdCRodHRwKHtcblx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuXHRcdFx0dXJsOiBjb25zLmFwaWJhc2UgKycvYXBpbG9naW4uanNvbicsXG5cdFx0XHRkYXRhOiBkYXRhXG5cdFx0fSlcblx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdGNvbnNvbGUuaW5mbygnbG9naW4gcmVzcG9uc2UnLCByZXMpO1xuXHRcdFx0XHR2YXIgdXNlcnR5cGUgPSByZXMuZGF0YTtcblx0XHRcdFx0dXNlckFjY3QoKS5sb2dpbihkYXRhLnVzZXJuYW1lLCB1c2VydHlwZSwgZGF0YS5wYXNzd29yZCk7XG5cdFx0XHRcdGlmICggdXNlcnR5cGUgPT09ICdzdHVkZW50JyApXG5cdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0c3R1ZGVudCcpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0JGxvY2F0aW9uLnBhdGgoJy9yZXF1ZXN0dHV0b3InKTtcblx0XHRcdH0sIGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdGNvbnNvbGUud2FybignbG9naW4gcmVzcG9uc2UnLCByZXMpO1xuXHRcdFx0XHR2bS5lcnJvciA9ICdJbnZhbGlkIHVzZXJuYW1lL2VtYWlsIG9yIHBhc3N3b3JkJztcblx0XHRcdH0pXG5cdFx0XHQuZmluYWxseShmdW5jdGlvbigpe1xuXHRcdFx0XHR2bS5sb2FkaW5nID0gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHR2bS5lcnJvciA9IG51bGw7XG5cdFx0dm0ubG9hZGluZyA9IHRydWU7XG5cdH07XG5cbn1cbl0pO1xuIiwiYXBwLmNvbnRyb2xsZXIoJ29sdEN0cmwnLGZ1bmN0aW9uKCRzY29wZSwkaHR0cCwkbG9jYXRpb24sY29ucyxnZXRTdWJqZWN0cyx1c2VyQWNjdCl7XG5cbiAgICAkc2NvcGUuaW5pdGRhdGUgPSBcIlwiO1xuICAgICRzY29wZS5pbml0dGltZSA9IFwiXCI7XG4gICAgJHNjb3BlLnRpbWV6b25lID0gbW9tZW50KCkuZm9ybWF0KCdaJykrXCIgLSBcIitqc3R6KCkudGltZXpvbmVfbmFtZTtcbiAgICAkc2NvcGUuZ3JhZGVsdmxsaXN0ID0gY29ucy5ncmFkZUxldmVsO1xuICAgICRzY29wZS5sZW5ndGhsaXN0ID0gY29ucy5sZW5ndGhTZXNzaW9uO1xuXG4gICAgJHNjb3BlLm9sdCA9IHt9O1xuICAgICRzY29wZS5vbHQudHlwZSA9IFwiTFJcIjtcbiAgICAkc2NvcGUub2x0LmR1ZWRhdGUgPSBcIlwiO1xuICAgICRzY29wZS5vbHQudGltZXpvbmUgPSBqc3R6KCkudGltZXpvbmVfbmFtZTtcbiAgICAkc2NvcGUub2x0Lmxlbmd0aCA9IFwiMzAgbWludXRlc1wiO1xuICAgICRzY29wZS5vbHQuc3ViamVjdGlkID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmdyYWRlID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmZhc3QgPSBcIm5vcm1hbFwiO1xuICAgICRzY29wZS5vbHQudHV0b3JuYW1lID0gXCJcIjtcbiAgICAkc2NvcGUub2x0LmRpc2NvdW50Y29kZSA9IFwiXCI7XG4gICAgJHNjb3BlLm9sdC5jb21tZW50ID0gXCJcIjtcblxuICAgICRzY29wZS5zdWJqZWN0U2VsID0gXCJcIjtcbiAgICAkc2NvcGUuc3ViamVjdGxpc3QgPSBbXTtcblxuICAgIGdldFN1YmplY3RzLmxpc3QoKS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjayhyZXNwb25zZSl7XG4gICAgICAkc2NvcGUuc3ViamVjdGxpc3QgPSByZXNwb25zZS5kYXRhO1xuICAgICAgJHNjb3BlLmZvcm1hdHN1YmogPSBbXTtcblxuICAgICAgYW5ndWxhci5mb3JFYWNoKCRzY29wZS5zdWJqZWN0bGlzdCwgZnVuY3Rpb24oaXRlbXMsIGtleSl7XG4gICAgICAgIGZvcih2YXIgYT0wO2EgPCBpdGVtcy5zdWJqZWN0cy5sZW5ndGg7YSsrKXtcbiAgICAgICAgICBpdGVtcy5zdWJqZWN0c1thXS5ncm91cGlkID0gaXRlbXMuZ3JvdXBpZDtcbiAgICAgICAgICBpdGVtcy5zdWJqZWN0c1thXS5ncm91cHRpdGxlID0gaXRlbXMuZ3JvdXB0aXRsZTtcbiAgICAgICAgICAkc2NvcGUuZm9ybWF0c3Viai5wdXNoKGl0ZW1zLnN1YmplY3RzW2FdKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAkc2NvcGUuc3dpdGNoU3ViamVjdCA9IGZ1bmN0aW9uKCl7XG4gICAgICAkc2NvcGUub2x0LnN1YmplY3RpZCA9IFwiXCI7XG4gICAgICBpZigkc2NvcGUuc3ViamVjdFNlbCAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICAgJHNjb3BlLm9sdC5zdWJqZWN0aWQgPSAkc2NvcGUuc3ViamVjdFNlbC5pZDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgJHNjb3BlLnNlbmRPbHQgPSBmdW5jdGlvbigpe1xuICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUub2x0KTtcbiAgICAgICAgJGh0dHAoe1xuICAgIFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuICAgIFx0XHRcdHVybDogY29ucy5hcGliYXNlICsnL2FwaXJlcXVlc3QuanNvbicsXG4gICAgXHRcdFx0ZGF0YTogJHNjb3BlLm9sdFxuICAgIFx0XHR9KS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjayhyZXNwb25zZSl7XG4gICAgICAgICAgIFxuICAgICAgICAgICAgIC8vdG9kbyAtIHByb3BlciByZWRpcmVjdGlvbiBpZiBzdHVkZW50IG9yIHR1dG9yXG5cbiAgICAgICAgICAgaWYodXNlckFjY3QudXNlcnR5cGUoKSA9PT0gXCJzdHVkZW50XCIpe1xuICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvcmVxdWVzdHN0dWRlbnQnKTtcbiAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlcXVlc3RzdHVkZW50Jyk7XG4gICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgJChcIiNkYXRlcGlja2VyT2x0XCIpLm9uKFwiZHAuY2hhbmdlXCIsZnVuY3Rpb24oZSl7XG4gICAgICAgJHNjb3BlLmluaXRkYXRlID0gJCh0aGlzKS52YWwoKTtcbiAgICAgICAkc2NvcGUub2x0LmR1ZWRhdGUgPSAkc2NvcGUuaW5pdGRhdGUrXCIgXCIrJHNjb3BlLmluaXR0aW1lK1wiOjAwXCI7XG4gICAgfSk7XG5cbiAgICAkKFwiI3RpbWVwaWNrZXJPbHRcIikub24oXCJkcC5jaGFuZ2VcIixmdW5jdGlvbihlKXtcbiAgICAgICRzY29wZS5pbml0dGltZSA9ICQodGhpcykudmFsKCk7XG5cbiAgICAgIHZhciBzZWxUaW1lID0gIG1vbWVudCgkKHRoaXMpLnZhbCgpLCBbXCJoOm1tIEFcIl0pLmZvcm1hdChcIkhIOm1tXCIpO1xuICAgICAgJHNjb3BlLm9sdC5kdWVkYXRlID0gJHNjb3BlLmluaXRkYXRlK1wiIFwiK3NlbFRpbWUrXCI6MDBcIjtcbiAgICB9KTtcblxuXG59KTtcbiIsImFwcC5jb250cm9sbGVyKCdyZXFDb250ZW50U3R1ZGVudEN0cmwnLCBbJyRyb3V0ZVBhcmFtcycsICckc2NlJywgJ3VzZXJBY2N0JywgJ2FwaVJlcXVlc3QnLCAnbW9tZW50JyxcbmZ1bmN0aW9uKCRyb3V0ZVBhcmFtcywgJHNjZSwgdXNlckFjY3QsIGFwaVJlcXVlc3QsIG1vbWVudCl7XG5cdHZhciB2bSA9IHRoaXM7XG5cblx0dm0uZGF0YSA9IG51bGw7XG5cdHZtLmlzTG9hZGluZyA9IHRydWU7XG5cblx0dm0ubG9jYWxpemUgPSBmdW5jdGlvbih1c2VyKXtcblx0XHRpZiAoIHVzZXIgJiYgdXNlciA9PT0gdXNlckFjY3QudXNlcm5hbWUoKSApXG5cdFx0XHRyZXR1cm4gJ1lvdSc7XG5cdFx0cmV0dXJuIHVzZXI7XG5cdH07XG5cblx0dmFyIGlkcyA9IGFwaVJlcXVlc3QuaWRzID0gJHJvdXRlUGFyYW1zLmlkcy50b1VwcGVyQ2FzZSgpLFxuXHRcdHJlcUlEID0gYXBpUmVxdWVzdC5pZCA9IGlkcy5zdWJzdHIoMiksXG5cdFx0cmVxVHlwZSA9IGFwaVJlcXVlc3QudHlwZSA9IGlkcy5zdWJzdHIoMCwgMiksXG5cdFx0dXJsID0gJy5qc29uLycrIHJlcUlELFxuXHRcdHBhcmFtcyA9IHt0eXBlOiBpZHMuc3Vic3RyKDAsIDIpfSxcblx0XHRwYXlsb2FkID0ge21ldGhvZDogJ0dFVCcsIHVybDogdXJsLCBwYXJhbXM6IHBhcmFtc307XG5cblxuXHQodm0ucmVmcmVzaCA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGFwaVJlcXVlc3QocGF5bG9hZClcblx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdHZtLmRhdGEgPSBub3JtYWxpemUocmVzLmRhdGEpO1xuXHRcdFx0XHR2bS5pc1BlbmRpbmdRdW90ZSA9IHZtLmRhdGEucmVxdWVzdC5zdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gJ3BlbmRpbmcgcXVvdGUnO1xuXHRcdFx0XHR2bS5pc1BlbmRpbmdBdXRoID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAncGVuZGluZyBhdXRob3JpemF0aW9uJztcblx0XHRcdFx0dm0uaXNDYW5jZWxsZWQgPSB2bS5kYXRhLnJlcXVlc3Quc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09ICdjYW5jZWxlZCc7XG5cdFx0XHRcdHZtLmlzQ29tcGxldGVkID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAnY29tcGxldGVkJztcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHRcdHJldHVybiByZXM7XG5cdFx0XHR9LCBmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHR2bS5lcnJvciA9IHJlcy5kYXRhLmVycm9yO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdFx0dGhyb3cgdm0uZXJyb3I7XG5cdFx0XHR9KTtcblx0fSkoKTtcblxuXHR2bS5hcmNoaXZlUmVxdWVzdCA9IGZ1bmN0aW9uKCl7XG5cdFx0YXBpUmVxdWVzdCh7XG5cdFx0XHRcdG1ldGhvZDogJ1BVVCcsXG5cdFx0XHRcdHVybDogJy9hcmNoaXZlLmpzb24nLFxuXHRcdFx0XHRkYXRhOiB7dHlwZTogcmVxVHlwZSwgcmVxdWVzdGlkOiByZXFJRH1cblx0XHRcdH0pLnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0dm0ucmVmcmVzaCgpO1xuXHRcdFx0fSk7XG5cdH07XG5cblxuXHRmdW5jdGlvbiBub3JtYWxpemUoZGF0YSl7XG5cdFx0ZGF0YSA9IGFuZ3VsYXIuY29weShkYXRhKTtcblx0XHRpZiAoIGRhdGEucmVxdWVzdC50YWJsZXR5cGUgPT09ICdzb2x1dGlvbnJlcXVlc3RzJyApXG5cdFx0XHRkYXRhLnJlcXVlc3QuaWRzID0gJ1NSJysgZGF0YS5yZXF1ZXN0LmlkO1xuXHRcdGVsc2UgaWYgKCBkYXRhLnJlcXVlc3QudGFibGV0eXBlID09PSAnbGl2ZXJlcXVlc3RzJyApXG5cdFx0XHRkYXRhLnJlcXVlc3QuaWRzID0gJ0xSJysgZGF0YS5yZXF1ZXN0LmlkO1xuXG5cdFx0ZGF0YS5yZXF1ZXN0LmRhdGUgPSBtb21lbnQoZGF0YS5yZXF1ZXN0LmRhdGUpO1xuXHRcdGRhdGEucmVxdWVzdC5kYXRlQXJjaGl2ZWQgPSBtb21lbnQoZGF0YS5yZXF1ZXN0LmRhdGVBcmNoaXZlZCk7XG5cdFx0ZGF0YS5yZXF1ZXN0LmRhdGVBc3NpZ25lZCA9IG1vbWVudChkYXRhLnJlcXVlc3QuZGF0ZUFzc2lnbmVkKTtcblx0XHRkYXRhLnJlcXVlc3QuZGF0ZUNhbmNlbGVkID0gbW9tZW50KGRhdGEucmVxdWVzdC5kYXRlQ2FuY2VsZWQpO1xuXHRcdGRhdGEucmVxdWVzdC5kYXRlQ29tcGxldGVkID0gbW9tZW50KGRhdGEucmVxdWVzdC5kYXRlQ29tcGxldGVkKTtcblx0XHRkYXRhLnJlcXVlc3QuZHVlRGF0ZSA9IG1vbWVudChkYXRhLnJlcXVlc3QuZHVlRGF0ZSk7XG5cdFx0ZGF0YS5yZXF1ZXN0Lmxhc3RTdHVkZW50UG9zdCA9IG1vbWVudChkYXRhLnJlcXVlc3QubGFzdFN0dWRlbnRQb3N0KTtcblx0XHRkYXRhLnJlcXVlc3QubGFzdFR1dG9yUG9zdCA9IG1vbWVudChkYXRhLnJlcXVlc3QubGFzdFR1dG9yUG9zdCk7XG5cblx0XHRkYXRhLnN1YmplY3RzID0gJHNjZS50cnVzdEFzSHRtbChkYXRhLnN1YmplY3RzLmpvaW4oJzsgJykpO1xuXHRcdGRhdGEucmVxdWVzdC5hZGRpdGlvbmFsSW5mbyA9ICRzY2UudHJ1c3RBc0h0bWwoZGF0YS5yZXF1ZXN0LmFkZGl0aW9uYWxJbmZvKTtcblxuXHRcdGFuZ3VsYXIuZm9yRWFjaChkYXRhLnBvc3RzLCBmdW5jdGlvbihkLCBpKXtcblx0XHRcdGQuZGF0ZSA9IG1vbWVudChkLmRhdGUpO1xuXHRcdFx0ZC5pc19ub3RlID0gKGQuaXNfbm90ZSA9PT0gJzEnKTtcblx0XHR9KTtcblxuXHRcdHJldHVybiBkYXRhO1xuXHR9XG59XG5dKTtcbiIsImFwcC5jb250cm9sbGVyKCdyZXFDb250ZW50VHV0b3JDdHJsJywgWyckcm91dGVQYXJhbXMnLCAnJHNjZScsICd1c2VyQWNjdCcsICdhcGlSZXF1ZXN0JywgJ21vbWVudCcsXG5mdW5jdGlvbigkcm91dGVQYXJhbXMsICRzY2UsIHVzZXJBY2N0LCBhcGlSZXF1ZXN0LCBtb21lbnQpe1xuXHR2YXIgdm0gPSB0aGlzO1xuXG5cdHZtLmRhdGEgPSBudWxsO1xuXHR2bS5pc0xvYWRpbmcgPSB0cnVlO1xuXHR2bS5lcnJvciA9IG51bGw7XG5cblx0dm0ubG9jYWxpemUgPSBmdW5jdGlvbih1c2VyKXtcblx0XHRpZiAoIHVzZXIgJiYgdXNlciA9PT0gdXNlckFjY3QudXNlcm5hbWUoKSApXG5cdFx0XHRyZXR1cm4gJ1lvdSc7XG5cdFx0cmV0dXJuIHVzZXI7XG5cdH07XG5cblx0dm0uaXNQZW5kaW5nUXVvdGUgPSBmYWxzZTtcblx0dm0uaXNQZW5kaW5nQXV0aCA9IGZhbHNlO1xuXG5cdHZtLmR1ZURhdGVEaWZmID0gZnVuY3Rpb24oKXtcblx0XHRpZiAoIHZtLmlzTG9hZGluZyApIHJldHVybiAnJztcblx0XHRpZiAoIHZtLmRhdGEucmVxdWVzdC5kdWVEYXRlLmlzQWZ0ZXIoKSApXG5cdFx0XHRyZXR1cm4gJzAgZGF5cyAwIGhvdXJzIDAgbWludXRlcyc7XG5cblx0XHR2YXIgZHVyID0gbW9tZW50LmR1cmF0aW9uKCBtb21lbnQoKS5kaWZmKHZtLmRhdGEucmVxdWVzdC5kdWVEYXRlKSApO1xuXHRcdHJldHVybiBkdXIuZGF5cygpICsnIGRheXMgJysgZHVyLmhvdXJzKCkgKycgaG91cnMgJysgZHVyLm1pbnV0ZXMoKSArJyBtaW51dGVzJztcblx0fTtcblxuXG5cdHZhciBpZHMgPSBhcGlSZXF1ZXN0LmlkcyA9ICRyb3V0ZVBhcmFtcy5pZHMudG9VcHBlckNhc2UoKSxcblx0XHRyZXFJRCA9IGFwaVJlcXVlc3QuaWQgPSBpZHMuc3Vic3RyKDIpLFxuXHRcdHJlcVR5cGUgPSBhcGlSZXF1ZXN0LnR5cGUgPSBpZHMuc3Vic3RyKDAsIDIpLFxuXHRcdHVybCA9ICcuanNvbi8nKyByZXFJRCxcblx0XHRwYXJhbXMgPSB7dHlwZTogcmVxVHlwZX0sXG5cdFx0cGF5bG9hZCA9IHttZXRob2Q6ICdHRVQnLCB1cmw6IHVybCwgcGFyYW1zOiBwYXJhbXN9O1xuXG5cblx0KHZtLnJlZnJlc2ggPSBmdW5jdGlvbigpe1xuXHRcdHJldHVybiBhcGlSZXF1ZXN0KHBheWxvYWQpXG5cdFx0XHQudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHR2bS5kYXRhID0gbm9ybWFsaXplKHJlcy5kYXRhKTtcblx0XHRcdFx0dm0uaXNQZW5kaW5nUXVvdGUgPSB2bS5kYXRhLnJlcXVlc3Quc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09ICdwZW5kaW5nIHF1b3RlJztcblx0XHRcdFx0dm0uaXNQZW5kaW5nQXV0aCA9IHZtLmRhdGEucmVxdWVzdC5zdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gJ3BlbmRpbmcgYXV0aG9yaXphdGlvbic7XG5cdFx0XHRcdHZtLmlzQ2FuY2VsbGVkID0gdm0uZGF0YS5yZXF1ZXN0LnN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSAnY2FuY2VsZWQnO1xuXHRcdFx0XHR2bS5pc0NvbXBsZXRlZCA9IHZtLmRhdGEucmVxdWVzdC5zdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gJ2NvbXBsZXRlZCc7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0XHRyZXR1cm4gcmVzO1xuXHRcdFx0fSwgZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0dm0uZXJyb3IgPSByZXMuZGF0YS5lcnJvcjtcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHRcdHRocm93IHZtLmVycm9yO1xuXHRcdFx0fSk7XG5cdH0pKCk7XG5cblx0dm0uYXJjaGl2ZVJlcXVlc3QgPSBmdW5jdGlvbigpe1xuXHRcdGlmICggdXNlckFjY3QudXNlcm5hbWUoKSA9PT0gdm0uZGF0YS5yZXF1ZXN0LnR1dG9yIClcblx0XHRcdGFwaVJlcXVlc3Qoe1xuXHRcdFx0XHRcdG1ldGhvZDogJ1BVVCcsXG5cdFx0XHRcdFx0dXJsOiAnL2FyY2hpdmUuanNvbicsXG5cdFx0XHRcdFx0ZGF0YToge3R5cGU6IHJlcVR5cGUsIHJlcXVlc3RpZDogcmVxSUR9XG5cdFx0XHRcdH0pLnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0XHR2bS5yZWZyZXNoKCk7XG5cdFx0XHRcdH0pO1xuXHR9O1xuXHR2bS5yZWxlYXNlUmVxdWVzdCA9IGZ1bmN0aW9uKCl7XG5cdFx0aWYgKCB1c2VyQWNjdC51c2VybmFtZSgpID09PSB2bS5kYXRhLnJlcXVlc3QudHV0b3IgKVxuXHRcdFx0YXBpUmVxdWVzdCh7XG5cdFx0XHRcdFx0bWV0aG9kOiAnUFVUJyxcblx0XHRcdFx0XHR1cmw6ICcvcmVsZWFzZS5qc29uJyxcblx0XHRcdFx0XHRkYXRhOiB7dHlwZTogcmVxVHlwZSwgcmVxdWVzdGlkOiByZXFJRH1cblx0XHRcdFx0fSkudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRcdHZtLnJlZnJlc2goKTtcblx0XHRcdFx0fSk7XG5cdH07XG5cblx0dm0ucXVvdGUgPSB7XG5cdFx0dmFsdWU6IDAsXG5cdFx0dmlzaWJsZTogZmFsc2UsXG5cdFx0c3VibWl0OiBmdW5jdGlvbigpe1xuXHRcdFx0aWYgKCAhIC9eWzAtOV0rJC8udGVzdCh2bS5xdW90ZS52YWx1ZSkgKVxuXHRcdFx0XHRyZXR1cm4gYW5ndWxhci5lbGVtZW50KCdbbmctbW9kZWw9XCJ2bS5xdW90ZS52YWx1ZVwiXScpLmZvY3VzKCkgJiYgMDtcblxuXHRcdFx0YXBpUmVxdWVzdCh7XG5cdFx0XHRcdFx0bWV0aG9kOiAnUFVUJyxcblx0XHRcdFx0XHR1cmw6ICcvcXVvdGUuanNvbicsXG5cdFx0XHRcdFx0ZGF0YToge3R5cGU6IHJlcVR5cGUsIHJlcXVlc3RpZDogcmVxSUQsIHF1b3RlOiBwYXJzZUludCh2bS5xdW90ZS52YWx1ZSl9XG5cdFx0XHRcdH0pLnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0XHR2bS5yZWZyZXNoKCk7XG5cdFx0XHRcdH0pO1xuXHRcdH1cblx0fTtcblxuXG5cdGZ1bmN0aW9uIG5vcm1hbGl6ZShkYXRhKXtcblx0XHR2YXIgcmVxID0gZGF0YS5yZXF1ZXN0O1xuXHRcdGlmICggcmVxLnRhYmxldHlwZSA9PT0gJ3NvbHV0aW9ucmVxdWVzdHMnIClcblx0XHRcdHJlcS5pZHMgPSAnU1InKyByZXEuaWQ7XG5cdFx0ZWxzZSBpZiAoIHJlcS50YWJsZXR5cGUgPT09ICdsaXZlcmVxdWVzdHMnIClcblx0XHRcdHJlcS5pZHMgPSAnTFInKyByZXEuaWQ7XG5cblx0XHRyZXEuZGF0ZSA9IG1vbWVudChyZXEuZGF0ZSk7XG5cdFx0cmVxLmRhdGVBcmNoaXZlZCA9IG1vbWVudChyZXEuZGF0ZUFyY2hpdmVkKTtcblx0XHRyZXEuZGF0ZUFzc2lnbmVkID0gbW9tZW50KHJlcS5kYXRlQXNzaWduZWQpO1xuXHRcdHJlcS5kYXRlQ2FuY2VsZWQgPSBtb21lbnQocmVxLmRhdGVDYW5jZWxlZCk7XG5cdFx0cmVxLmRhdGVDb21wbGV0ZWQgPSBtb21lbnQocmVxLmRhdGVDb21wbGV0ZWQpO1xuXHRcdHJlcS5kdWVEYXRlID0gbW9tZW50KHJlcS5kdWVEYXRlKTtcblx0XHRyZXEubGFzdFN0dWRlbnRQb3N0ID0gbW9tZW50KHJlcS5sYXN0U3R1ZGVudFBvc3QpO1xuXHRcdHJlcS5sYXN0VHV0b3JQb3N0ID0gbW9tZW50KHJlcS5sYXN0VHV0b3JQb3N0KTtcblxuXHRcdHJlcS5xdW90ZSA9IHBhcnNlRmxvYXQoIHJlcS5xdW90ZSApO1xuXG5cdFx0ZGF0YS5zdWJqZWN0cyA9ICRzY2UudHJ1c3RBc0h0bWwoZGF0YS5zdWJqZWN0cy5qb2luKCc7ICcpKTtcblx0XHRyZXEuYWRkaXRpb25hbEluZm8gPSAkc2NlLnRydXN0QXNIdG1sKHJlcS5hZGRpdGlvbmFsSW5mbyk7XG5cblx0XHQkLmVhY2goZGF0YS5wb3N0cywgZnVuY3Rpb24oaSwgZCl7XG5cdFx0XHRkLmRhdGUgPSBtb21lbnQoZC5kYXRlKTtcblx0XHRcdGQuaXNfbm90ZSA9IChkLmlzX25vdGUgPT09ICcxJyk7XG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gZGF0YTtcblx0fVxufVxuXSk7IiwiYXBwLmNvbnRyb2xsZXIoJ3JlcXVlc3RzU3R1ZGVudEN0cmwnLCBbJ2FwaVJlcXVlc3QnLCAnbW9tZW50JyxcbmZ1bmN0aW9uKGFwaVJlcXVlc3QsIG1vbWVudCl7XG5cdHZhciB2bSA9IHRoaXM7XG5cblx0dm0ubGlzdCA9IFtdO1xuXHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblxuXHR2bS5maWVsZHMgPSB7XG5cdFx0J1Nob3cgYWxsIHJlcXVlc3RzJzoge2RheXNfYWdvOiAtMSwgc2hvd19hcmNoaXZlZDogZmFsc2V9LFxuXHRcdCdTaG93IGxhc3QgMTUgZGF5cyc6IHtkYXlzX2FnbzogMTUsIHNob3dfYXJjaGl2ZWQ6IGZhbHNlfSxcblx0XHQnU2hvdyBsYXN0IDYwIGRheXMnOiB7ZGF5c19hZ286IDYwLCBzaG93X2FyY2hpdmVkOiBmYWxzZX0sXG5cdFx0J1Nob3cgbGFzdCA5MCBkYXlzJzoge2RheXNfYWdvOiA5MCwgc2hvd19hcmNoaXZlZDogZmFsc2V9LFxuXHRcdCdTaG93IGFyY2hpdmVkIHJlcXVlc3RzJzoge2RheXNfYWdvOiAtMSwgc2hvd19hcmNoaXZlZDogdHJ1ZX1cblx0fTtcblxuXHR2YXIgZGVmYXVsdFBhcmFtcyA9IHZtLmZpZWxkc1snU2hvdyBsYXN0IDE1IGRheXMnXTtcblxuXHR2YXIgcGF5bG9hZCA9IHtcblx0XHRtZXRob2Q6ICdHRVQnLFxuXHRcdHVybDogJy9zdHVkZW50YWxsLmpzb24nXG5cdH07XG5cdHZtLnBhcmFtcyA9IGRlZmF1bHRQYXJhbXM7XG5cblx0KHZtLnJlZnJlc2ggPSBmdW5jdGlvbigpe1xuXHRcdHBheWxvYWQucGFyYW1zID0gYW5ndWxhci5jb3B5KHZtLnBhcmFtcyk7XG5cdFx0YXBpUmVxdWVzdChwYXlsb2FkKVxuXHRcdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0dm0ubGlzdCA9IFtdO1xuXHRcdFx0XHRhbmd1bGFyLmZvckVhY2gocmVzLmRhdGEsIGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHRcdHZtLmxpc3QucHVzaChub3JtYWxpemVEYXRhKGQpKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KVxuXHRcdFx0LmZpbmFsbHkoZnVuY3Rpb24oKXtcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHR2bS5pc0xvYWRpbmcgPSB0cnVlO1xuXHR9KSgpO1xuXG5cdHZtLnJlc2V0ID0gZnVuY3Rpb24oZm9ybSl7XG5cdFx0dm0ucGFyYW1zID0gZGVmYXVsdFBhcmFtcztcblx0XHRmb3JtLiRzZXRQcmlzdGluZSh0cnVlKTtcblx0XHR2bS5zdWJtaXQoZm9ybSk7XG5cdH07XG5cdHZtLnN1Ym1pdCA9IGZ1bmN0aW9uKGZvcm0pe1xuXHRcdGlmICggISEgZm9ybSApIHtcblx0XHRcdGFuZ3VsYXIuZWxlbWVudCgnZm9ybVtuYW1lPVwiJysgZm9ybS4kbmFtZSArJ1wiXScpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHR2bS5yZXNldC4kZGlydHkgPSBmb3JtLiRkaXJ0eTtcblx0XHR9XG5cdFx0dm0ucmVmcmVzaCgpO1xuXHR9O1xuXG5cblx0ZnVuY3Rpb24gbm9ybWFsaXplRGF0YShkYXRhKXtcblx0XHRpZiAoIGRhdGEudGFibGV0eXBlID09PSAnc29sdXRpb25yZXF1ZXN0cycgKSB7XG5cdFx0XHRkYXRhLnRhYmxldHlwZSA9ICdTb2x1dGlvbnMgUmVxdWVzdCc7XG5cdFx0XHRkYXRhLmlkcyA9ICdTUicrIGRhdGEuaWQ7XG5cdFx0fSBlbHNlXG5cdFx0aWYgKCBkYXRhLnRhYmxldHlwZSA9PT0gJ2xpdmVyZXF1ZXN0cycgKSB7XG5cdFx0XHRkYXRhLnRhYmxldHlwZSA9ICdMaXZlIFJlcXVlc3QnO1xuXHRcdFx0ZGF0YS5pZHMgPSAnTFInKyBkYXRhLmlkO1xuXHRcdH1cblxuXHRcdGRhdGEuZGF0ZSA9IG1vbWVudChkYXRhLmRhdGUpO1xuXHRcdGRhdGEuZHVlRGF0ZSA9IG1vbWVudChkYXRhLmR1ZURhdGUpO1xuXHRcdGRhdGEubGFzdFR1dG9yUG9zdCA9IG1vbWVudChkYXRhLmxhc3RUdXRvclBvc3QpO1xuXHRcdHJldHVybiBkYXRhO1xuXHR9XG59XG5dKTtcbiIsImFwcC5jb250cm9sbGVyKCdyZXF1ZXN0c1R1dG9yQ3RybCcsIFsnYXBpUmVxdWVzdCcsICdtb21lbnQnLFxuZnVuY3Rpb24oYXBpUmVxdWVzdCwgbW9tZW50KXtcblx0dmFyIHZtID0gdGhpcztcblxuXHR2bS5saXN0ID0gW107XG5cdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXG5cdHZhciBkZWZhdWx0UGFyYW1zID0ge1xuXHRcdG9yZGVyX2J5OiAnZHVlRGF0ZScsXG5cdFx0b3JkZXJfYnlfZGlyZWN0aW9uOiAnREVTQycsXG5cdFx0cGlwZWxpbmU6ICduby1waXBlbGluZScsXG5cdFx0dGFibGU6ICdhbGwnLFxuXHRcdHN0YXR1czogJ2FjdGl2ZScsXG5cdFx0YXJjaGl2ZWQ6ICd1bmFyY2hpdmVkJyxcblx0XHRhY2N0OiAtMSxcblx0XHRrZXl3b3JkOiAnJ1xuXHR9O1xuXG5cdHZhciBwYXlsb2FkID0ge1xuXHRcdG1ldGhvZDogJ0dFVCcsXG5cdFx0dXJsOiAnL3R1dG9yYWxsLmpzb24nLFxuXHRcdHBhcmFtczogdm0ucGFyYW1zID0gYW5ndWxhci5jb3B5KGRlZmF1bHRQYXJhbXMpXG5cdH07XG5cblx0KHZtLnJlZnJlc2ggPSBmdW5jdGlvbigpe1xuXHRcdHBheWxvYWQucGFyYW1zLmtleXdvcmQgPSBwYXlsb2FkLnBhcmFtcy5rZXl3b3JkLnRyaW0oKTtcblxuXHRcdGFwaVJlcXVlc3QocGF5bG9hZClcblx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdHZtLmxpc3QgPSBbXTtcblx0XHRcdFx0YW5ndWxhci5mb3JFYWNoKHJlcy5kYXRhLnJlcXVlc3RzIHx8IFtdLCBmdW5jdGlvbihkKXtcblx0XHRcdFx0XHR2bS5saXN0LnB1c2gobm9ybWFsaXplRGF0YShkKSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSlcblx0XHRcdC5maW5hbGx5KGZ1bmN0aW9uKCl7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0fSk7XG5cdFx0dm0uaXNMb2FkaW5nID0gdHJ1ZTtcblx0fSkoKTtcblxuXG5cdHZtLnBpcGVsaW5lID0gZmFsc2U7XG5cdHZtLnRvZ2dsZVBpcGVsaW5lID0gZnVuY3Rpb24oKXtcblx0XHR2bS5waXBlbGluZSA9ICF2bS5waXBlbGluZTtcblx0XHR2bS5wYXJhbXMucGlwZWxpbmUgPSB2bS5waXBlbGluZSA/ICdwaXBlbGluZS1vbmx5JyA6ICduby1waXBlbGluZSc7XG5cdFx0dm0ucmVmcmVzaCgpO1xuXHR9O1xuXHR2bS5zb3J0ID0ge1xuXHRcdG9yZGVyX2J5OiB7XG5cdFx0XHQndGltZV91bmF0dGVuZGVkJzogJ1RpbWUgVW5hdHRlbmRlZCcsXG5cdFx0XHQnZGF0ZSc6ICdEYXRlJyxcblx0XHRcdCdkdWVEYXRlJzogJ0R1ZSBEYXRlJ1xuXHRcdH0sXG5cdFx0b3JkZXJfYnlfZGlyZWN0aW9uOiB7XG5cdFx0XHQnREVTQyc6ICdEZXNjZW5kaW5nJyxcblx0XHRcdCdBU0MnOiAnQXNjZW5kaW5nJ1xuXHRcdH1cblx0fTtcblx0dm0uZmlsdGVyID0ge1xuXHRcdHN0YXR1czoge1xuXHRcdFx0J2FjdGl2ZSc6ICdBY3RpdmUnLFxuXHRcdFx0J1BlbmRpbmcgUXVvdGUnOiAnUGVuZGluZyBRdW90ZScsXG5cdFx0XHQnUGVuZGluZyBBdXRob3JpemF0aW9uJzogJ1BlbmRpbmcgQXV0aG9yaXphdGlvbicsXG5cdFx0XHQnUGVuZGluZyBDb21wbGV0aW9uJzogJ1BlbmRpbmcgQ29tcGxldGlvbicsXG5cdFx0XHQnQ29tcGxldGVkJzogJ0NvbXBsZXRlZCcsXG5cdFx0XHQnQ2FuY2VsZWQnOiAnQ2FuY2VsZWQnXG5cdFx0fSxcblx0XHR0YWJsZToge1xuXHRcdFx0J2FsbCc6ICdBbGwnLFxuXHRcdFx0J3NvbHV0aW9ucmVxdWVzdHMnOiAnU29sdXRpb25zIFJlcXVlc3RzJyxcblx0XHRcdCdsaXZlcmVxdWVzdHMnOiAnTGl2ZSBSZXF1ZXN0cydcblx0XHR9LFxuXHRcdGFyY2hpdmVkOiB7XG5cdFx0XHQnYWxsJzogJ0FyY2hpdmVkICYgVW5hcmNoaXZlZCcsXG5cdFx0XHQnYXJjaGl2ZWQnOiAnQXJjaGl2ZWQnLFxuXHRcdFx0J3VuYXJjaGl2ZWQnOiAnVW5hcmNoaXZlZCcsXG5cdFx0fVxuXHR9O1xuXG5cdHZtLnNvcnRSZXNldCA9IGZ1bmN0aW9uKGZvcm0pe1xuXHRcdGZvciggdmFyIGsgaW4gdm0uc29ydCApXG5cdFx0XHRpZiAoIHZtLnNvcnQuaGFzT3duUHJvcGVydHkoaykgKVxuXHRcdFx0XHR2bS5wYXJhbXNba10gPSBkZWZhdWx0UGFyYW1zW2tdO1xuXHRcdGZvcm0uJHNldFByaXN0aW5lKHRydWUpO1xuXHRcdHZtLnN1Ym1pdChmb3JtKTtcblx0fTtcblx0dm0uZmlsdGVyUmVzZXQgPSBmdW5jdGlvbihmb3JtKXtcblx0XHRmb3IoIHZhciBrIGluIHZtLmZpbHRlciApXG5cdFx0XHRpZiAoIHZtLmZpbHRlci5oYXNPd25Qcm9wZXJ0eShrKSApXG5cdFx0XHRcdHZtLnBhcmFtc1trXSA9IGRlZmF1bHRQYXJhbXNba107XG5cdFx0Zm9ybS4kc2V0UHJpc3RpbmUodHJ1ZSk7XG5cdFx0dm0uc3VibWl0KGZvcm0pO1xuXHR9O1xuXHR2bS5zdWJtaXQgPSBmdW5jdGlvbihmb3JtKXtcblx0XHRpZiAoICEhIGZvcm0gKSB7XG5cdFx0XHRhbmd1bGFyLmVsZW1lbnQoJ2Zvcm1bbmFtZT1cIicrIGZvcm0uJG5hbWUgKydcIl0nKS5wYXJlbnRzKCcubW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0dm1bZm9ybS4kbmFtZSArJ1Jlc2V0J10uJGRpcnR5ID0gZm9ybS4kZGlydHk7XG5cdFx0fVxuXHRcdHZtLnJlZnJlc2goKTtcblx0fTtcblxuXG5cdGZ1bmN0aW9uIG5vcm1hbGl6ZURhdGEoZGF0YSl7XG5cdFx0aWYgKCBkYXRhLnRhYmxldHlwZSA9PT0gJ3NvbHV0aW9ucmVxdWVzdHMnICkge1xuXHRcdFx0ZGF0YS50YWJsZXR5cGUgPSAnU29sdXRpb25zIFJlcXVlc3QnO1xuXHRcdFx0ZGF0YS5pZHMgPSAnU1InKyBkYXRhLmlkO1xuXHRcdH0gZWxzZVxuXHRcdGlmICggZGF0YS50YWJsZXR5cGUgPT09ICdsaXZlcmVxdWVzdHMnICkge1xuXHRcdFx0ZGF0YS50YWJsZXR5cGUgPSAnTGl2ZSBSZXF1ZXN0Jztcblx0XHRcdGRhdGEuaWRzID0gJ0xSJysgZGF0YS5pZDtcblx0XHR9XG5cblx0XHRkYXRhLmRhdGUgPSBtb21lbnQoZGF0YS5kYXRlKTtcblx0XHRkYXRhLmR1ZURhdGUgPSBtb21lbnQoZGF0YS5kdWVEYXRlKTtcblx0XHRkYXRhLmxhc3RUdXRvclBvc3QgPSBtb21lbnQoZGF0YS5sYXN0VHV0b3JQb3N0KTtcblx0XHRkYXRhLnVuYXR0ZW5kZWQgPSBNYXRoLmNlaWwoZGF0YS50aW1lX3VuYXR0ZW5kZWQgLyA2MCAvIDI0KTtcblx0XHRpZiAoIGRhdGEudW5hdHRlbmRlZCA+IDEgKVxuXHRcdFx0ZGF0YS51bmF0dGVuZGVkICs9ICcgZGF5cyc7XG5cdFx0ZWxzZVxuXHRcdFx0ZGF0YS51bmF0dGVuZGVkICs9ICcgZGF5Jztcblx0XHRyZXR1cm4gZGF0YTtcblx0fVxufVxuXSk7XG4iLCJhcHAuY29udHJvbGxlcignem9uZUN0cmwnLCBmdW5jdGlvbigkc2NvcGUpIHtcbiAgICRzY29wZS5jdXJyZW50VFogPSBtb21lbnQoKS5mb3JtYXQoJ1onKTtcbiB9KTtcbiIsImFwcC5kaXJlY3RpdmUoJ2FkZFBvc3QnLCBbJ2FwaVJlcXVlc3QnLCBmdW5jdGlvbihhcGlSZXF1ZXN0KXtcblx0cmV0dXJuIHtcblx0XHRyZXN0cmljdDogJ0VBJyxcblx0XHRzY29wZToge30sXG5cdFx0dGVtcGxhdGVVcmw6ICdhcHAvcGFnZXMvbW9kYWwvYWRkUG9zdC50cGwuaHRtbCcsXG5cdFx0bGluazogZnVuY3Rpb24oJHNjb3BlLCBlbGVtLCBhdHRyKXtcblx0XHRcdGVsZW0uYXR0cih7XG5cdFx0XHRcdCdpZCc6ICdtb2RhbC1hZGQtcG9zdCcsXG5cdFx0XHRcdCdjbGFzcyc6ICdtb2RhbCBmYWRlJyxcblx0XHRcdFx0J3RhYmluZGV4JzogJy0xJyxcblx0XHRcdFx0J3JvbGUnOiAnZGlhbG9nJyxcblx0XHRcdFx0J2FyaWEtbGFiZWxsZWRieSc6ICdhZGRQb3N0Jyxcblx0XHRcdFx0J2FyaWEtaGlkZGVuJzogJ3RydWUnXG5cdFx0XHR9KS5vbignc2hvdy5icy5tb2RhbCBoaWRkZW4uYnMubW9kYWwnLCByZXNldCk7XG5cblx0XHRcdHZhciB2bSA9ICRzY29wZS52bSA9IHtzdWJtaXQ6IHN1Ym1pdH0sXG5cdFx0XHRcdHBheWxvYWQgPSB7XG5cdFx0XHRcdFx0bWV0aG9kOiAnUE9TVCcsXG5cdFx0XHRcdFx0dXJsOiAnL2FkZHBvc3QuanNvbicsXG5cdFx0XHRcdFx0ZGF0YToge3R5cGU6IGFwaVJlcXVlc3QudHlwZSwgcmVxdWVzdGlkOiBhcGlSZXF1ZXN0LmlkfVxuXHRcdFx0XHR9O1xuXG5cdFx0XHRmdW5jdGlvbiByZXNldCgpe1xuXHRcdFx0XHR2bS5maWxlID0gdW5kZWZpbmVkO1xuXHRcdFx0XHR2bS5jb21tZW50ID0gJyc7XG5cdFx0XHRcdHZtLmVycm9yTXNnID0gbnVsbDtcblx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHR9XG5cdFx0XHRmdW5jdGlvbiBzdWJtaXQoKXtcblx0XHRcdFx0aWYgKCB2bS5jb21tZW50LnRyaW0oKSA9PT0gJycgKS8vICYmICEgdm0uZmlsZSApXG5cdFx0XHRcdFx0cmV0dXJuIGVsZW0uZmluZCgndGV4dGFyZWEnKS5mb2N1cygpO1xuXG5cdFx0XHRcdHBheWxvYWQuZGF0YS5jb21tZW50ID0gdm0uY29tbWVudC50cmltKCk7XG5cdFx0XHRcdGFwaVJlcXVlc3QocGF5bG9hZClcblx0XHRcdFx0XHQudGhlbihmdW5jdGlvbihyZXMpe1xuXHRcdFx0XHRcdFx0JHNjb3BlLiRwYXJlbnQuJGV2YWwoYXR0ci5hZGRQb3N0U3VjY2Vzcyk7XG5cdFx0XHRcdFx0XHRlbGVtLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRcdFx0XHRyZXNldCgpO1xuXHRcdFx0XHRcdH0sIGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdFx0XHR2bS5lcnJvck1zZyA9ICdBbiBlcnJvciBvY2N1cmVkIHdoaWxlIHBvc3RpbmcnO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmZpbmFsbHkoZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRcdHZtLmlzTG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSB0cnVlO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxufV0pOyIsImFwcC5kaXJlY3RpdmUoJ2NhbmNlbFJlcXVlc3QnLCBbJ2FwaVJlcXVlc3QnLCAnY29ucycsIGZ1bmN0aW9uKGFwaVJlcXVlc3QsIGNvbnMpe1xuXHRyZXR1cm4ge1xuXHRcdHJlc3RyaWN0OiAnRUEnLFxuXHRcdHNjb3BlOiB7fSxcblx0XHR0ZW1wbGF0ZVVybDogJ2FwcC9wYWdlcy9tb2RhbC9jYW5jZWxSZXF1ZXN0UmVhc29uLnRwbC5odG1sJyxcblx0XHRsaW5rOiBmdW5jdGlvbigkc2NvcGUsIGVsZW0sIGF0dHIpe1xuXHRcdFx0ZWxlbS5hdHRyKHtcblx0XHRcdFx0J2lkJzogJ21vZGFsLWNhbmNlbC1yZXF1ZXN0Jyxcblx0XHRcdFx0J2NsYXNzJzogJ21vZGFsIGZhZGUnLFxuXHRcdFx0XHQndGFiaW5kZXgnOiAnLTEnLFxuXHRcdFx0XHQncm9sZSc6ICdkaWFsb2cnLFxuXHRcdFx0XHQnYXJpYS1sYWJlbGxlZGJ5JzogJ2NhbmNlbFJlcXVlc3QnLFxuXHRcdFx0XHQnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcblx0XHRcdH0pLm9uKCdzaG93LmJzLm1vZGFsIGhpZGRlbi5icy5tb2RhbCcsIHJlc2V0KTtcblxuXHRcdFx0dmFyIHZtID0gJHNjb3BlLnZtID0ge3N1Ym1pdDogc3VibWl0fSxcblx0XHRcdFx0cGF5bG9hZCA9IHtcblx0XHRcdFx0XHRtZXRob2Q6ICdQVVQnLFxuXHRcdFx0XHRcdHVybDogJy9jYW5jZWwuanNvbicsXG5cdFx0XHRcdFx0ZGF0YToge3R5cGU6IGFwaVJlcXVlc3QudHlwZSwgcmVxdWVzdGlkOiBhcGlSZXF1ZXN0LmlkfVxuXHRcdFx0XHR9O1xuXG5cdFx0XHRmdW5jdGlvbiByZXNldCgpe1xuXHRcdFx0XHR2bS5yZWFzb24gPSAnJztcblx0XHRcdFx0dm0uZXJyb3JNc2cgPSBudWxsO1xuXHRcdFx0XHR2bS5pc0xvYWRpbmcgPSBmYWxzZTtcblx0XHRcdH1cblx0XHRcdGZ1bmN0aW9uIHN1Ym1pdCgpe1xuXHRcdFx0XHRpZiAoIHZtLnJlYXNvbi50cmltKCkgPT09ICcnICkvLyAmJiAhIHZtLmZpbGUgKVxuXHRcdFx0XHRcdHJldHVybiBlbGVtLmZpbmQoJ3RleHRhcmVhJykuZm9jdXMoKTtcblxuXHRcdFx0XHRwYXlsb2FkLmRhdGEucmVhc29uID0gdm0ucmVhc29uLnRyaW0oKTtcblx0XHRcdFx0YXBpUmVxdWVzdChwYXlsb2FkKVxuXHRcdFx0XHRcdC50aGVuKGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdFx0XHQkc2NvcGUuJHBhcmVudC4kZXZhbChhdHRyLmNhbmNlbFJlcXVlc3RTdWNjZXNzKTtcblx0XHRcdFx0XHRcdGVsZW0ubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdHJlc2V0KCk7XG5cdFx0XHRcdFx0fSwgZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0XHRcdHZtLmVycm9yTXNnID0gJ0FuIGVycm9yIG9jY3VyZWQgd2hpbGUgcG9zdGluZyc7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuZmluYWxseShmdW5jdGlvbigpe1xuXHRcdFx0XHRcdFx0dm0uaXNMb2FkaW5nID0gZmFsc2U7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdHZtLmlzTG9hZGluZyA9IHRydWU7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59XSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbmFwcC5kaXJlY3RpdmUoJ2hlYWRlcm5hdicsIGZ1bmN0aW9uIChjb25zKSB7XG4gIGNvbnNvbGUubG9nKGNvbnMpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgcmVzdHJpY3Q6ICdBJywgLy9UaGlzIG1lbmFzIHRoYXQgaXQgd2lsbCBiZSB1c2VkIGFzIGFuIGF0dHJpYnV0ZSBhbmQgTk9UIGFzIGFuIGVsZW1lbnQuIEkgZG9uJ3QgbGlrZSBjcmVhdGluZyBjdXN0b20gSFRNTCBlbGVtZW50c1xuICAgICAgICByZXBsYWNlOiB0cnVlLFxuICAgICAgICB0ZW1wbGF0ZVVybDogY29ucy5wYWdlcGF0aCtcImhlYWRlcm5hdi50cGwuaHRtbFwiLFxuICAgICAgICBjb250cm9sbGVyOiBbJyRzY29wZScsICckZmlsdGVyJywgZnVuY3Rpb24gKCRzY29wZSwgJGZpbHRlcikge1xuICAgICAgICAgICAgLy8gWW91ciBiZWhhdmlvdXIgZ29lcyBoZXJlIDopXG4gICAgICAgIH1dXG4gICAgfVxufSk7XG4iLCJhcHAuZGlyZWN0aXZlKCdwYXNzd29yZE1hdGNoJywgW2Z1bmN0aW9uICgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgICByZXN0cmljdDogJ0EnLFxuICAgICAgICBzY29wZTp0cnVlLFxuICAgICAgICByZXF1aXJlOiAnbmdNb2RlbCcsXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbSAsIGF0dHJzLGNvbnRyb2wpIHtcbiAgICAgICAgICAgIHZhciBjaGVja2VyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIC8vZ2V0IHRoZSB2YWx1ZSBvZiB0aGUgZmlyc3QgcGFzc3dvcmRcbiAgICAgICAgICAgICAgICB2YXIgZTEgPSBzY29wZS4kZXZhbChhdHRycy5uZ01vZGVsKTtcblxuICAgICAgICAgICAgICAgIC8vZ2V0IHRoZSB2YWx1ZSBvZiB0aGUgb3RoZXIgcGFzc3dvcmRcbiAgICAgICAgICAgICAgICB2YXIgZTIgPSBzY29wZS4kZXZhbChhdHRycy5wYXNzd29yZE1hdGNoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZTEgPT0gZTI7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgc2NvcGUuJHdhdGNoKGNoZWNrZXIsIGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgICAgICAgLy9zZXQgdGhlIGZvcm0gY29udHJvbCB0byB2YWxpZCBpZiBib3RoXG4gICAgICAgICAgICAgICAgLy9wYXNzd29yZHMgYXJlIHRoZSBzYW1lLCBlbHNlIGludmFsaWRcbiAgICAgICAgICAgICAgICBjb250cm9sLiRzZXRWYWxpZGl0eShcInB3bWF0Y2hcIiwgbik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG59XSk7XG4iLCJhcHAuZmFjdG9yeSgnYXBpUmVxdWVzdCcsIFsnJGh0dHAnLCAnY29ucycsICd1c2VyQWNjdCcsIGZ1bmN0aW9uKCRodHRwLCBjb25zLCB1c2VyQWNjdCl7XG5cdHJldHVybiBmdW5jdGlvbihwYXlsb2FkKXtcblx0XHRwYXlsb2FkID0gYW5ndWxhci5jb3B5KHBheWxvYWQpO1xuXHRcdHBheWxvYWQudXJsID0gY29ucy5hcGliYXNlICsnL2FwaXJlcXVlc3QnKyBwYXlsb2FkLnVybDtcblxuXHRcdGNvbnNvbGUuaW5mbyggcGF5bG9hZC5tZXRob2QsIHBheWxvYWQudXJsLCBwYXlsb2FkLnBhcmFtcywgcGF5bG9hZC5kYXRhICk7XG5cblx0XHQvLyB0b2RvOiByZW1vdmUgdGhpcyB3aGVuIGJhc2ljIGF1dGggaXMgZml4ZWQgZnJvbSBhcGlcblx0XHRwYXlsb2FkLnVybCA9IHBheWxvYWQudXJsLnJlcGxhY2UoJ2h0dHA6Ly8nLCAnaHR0cDovLycrIHVzZXJBY2N0LnVzZXJuYW1lKCkgKyc6JysgdXNlckFjY3QucGFzc3dvcmQoKSArJ0AnKTtcblxuXHRcdHJldHVybiAkaHR0cChwYXlsb2FkKVxuXHRcdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdFx0Y29uc29sZS5pbmZvKCByZXMuc3RhdHVzLCByZXMuY29uZmlnLnVybCwgcmVzLmRhdGEgKTtcblx0XHRcdFx0cmV0dXJuIHJlcztcblx0XHRcdH0sIGZ1bmN0aW9uKHJlcyl7XG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IoIHJlcy5zdGF0dXMsIHJlcy5jb25maWcudXJsLCByZXMuZGF0YSApO1xuXHRcdFx0XHRyZXMuZGF0YSA9IHJlcy5kYXRhIHx8IHtlcnJvcjoge2NvZGU6IHJlcy5zdGF0dXMsIG1lc3NhZ2U6IHJlcy5zdGF0dXNUZXh0fX1cblx0XHRcdFx0dGhyb3cgcmVzO1xuXHRcdFx0fSk7XG5cdH1cbn1dKTsiLCJhcHAuZmFjdG9yeSgnZ2V0U3ViamVjdHMnLCBmdW5jdGlvbigkaHR0cCxjb25zKXtcbiAgICByZXR1cm4ge1xuICAgICAgbGlzdCA6IGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgICB1cmw6IFx0Y29ucy5hcGliYXNlICsnL2FwaXN1YmplY3RzLmpzb24vJ1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG59KTtcbiIsIi8qIFVzZXIgQWNjb3VudCBTZXJ2aWNlXG5cbnRvIGNoZWNrIGZvciB1c2VyIGNyZWRlbnRpYWxzOlxuXHRmdW5jdGlvbih1c2VyQWNjdCl7XG5cdFx0Y29uc29sZS5sb2coIHVzZXJBY2N0LmlzVmFsaWQoKSApO1xuXHRcdGNvbnNvbGUubG9nKCB1c2VyQWNjdC5pc1N0dWRlbnQoKSApO1xuXHRcdGNvbnNvbGUubG9nKCB1c2VyQWNjdC5pc1R1dG9yKCkgKTtcblx0XHRjb25zb2xlLmxvZyggdXNlckFjY3QudXNlcm5hbWUoKSApO1xuXHRcdGNvbnNvbGUubG9nKCB1c2VyQWNjdC51c2VydHlwZSgpICk7XG5cdH1cblxudG8gc2V0IGxvZ2luIGNyZWRlbnRpYWxzOlxuXHRmdW5jdGlvbih1c2VyQWNjdCl7XG5cdFx0dXNlckFjY3QoKS5sb2dpbignam9obicsICdzdHVkZW50Jyk7XG5cdH1cbiovXG5hcHAuZmFjdG9yeSgndXNlckFjY3QnLCBbJyR3aW5kb3cnLCBmdW5jdGlvbigkd2luZG93KXtcblx0dmFyIGxvYyA9IChmdW5jdGlvbigpe1xuXHRcdHZhciBzID0gJHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcicpO1xuXHRcdHJldHVybiBzID8gSlNPTi5wYXJzZShzKSA6IG51bGw7XG5cdH0pKCkgfHwge1xuXHRcdHVzZXJuYW1lOiBudWxsLFxuXHRcdHR5cGU6IG51bGxcblx0fTtcblxuXG5cdGZ1bmN0aW9uIHN2Yygpe1xuXHRcdHJldHVybiB7XG5cdFx0XHRsb2dpbjogZnVuY3Rpb24oIHVzZXJuYW1lLCB0eXBlLCBwYXNzd29yZCApey8vZm9yIG5vd1xuXHRcdFx0XHRsb2MudXNlcm5hbWUgPSB1c2VybmFtZTtcblx0XHRcdFx0bG9jLnR5cGUgPSB0eXBlO1xuXHRcdFx0XHRsb2MucGFzc3dvcmQgPSBwYXNzd29yZDsvL3RlbXAgb25seVxuXHRcdFx0XHR0cnl7XG5cdFx0XHRcdFx0JHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndXNlcicsIEpTT04uc3RyaW5naWZ5KGxvYykpO1xuXHRcdFx0XHR9Y2F0Y2goZSl7fTtcblx0XHRcdH0sXG5cdFx0XHRjbGVhcjogZnVuY3Rpb24oKXtcblx0XHRcdFx0bG9jLnVzZXJuYW1lID0gbnVsbDtcblx0XHRcdFx0bG9jLnR5cGUgPSBudWxsO1xuXHRcdFx0fVxuXHRcdH07XG5cdH1cblx0c3ZjLmlzVmFsaWQgPSBmdW5jdGlvbigpe1xuXHRcdHJldHVybiAhISBsb2MudXNlcm5hbWU7XG5cdH07XG5cdHN2Yy5pc1N0dWRlbnQgPSBmdW5jdGlvbigpe1xuXHRcdHJldHVybiBsb2MudHlwZSA9PT0gJ3N0dWRlbnQnO1xuXHR9O1xuXHRzdmMuaXNUdXRvciA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGxvYy50eXBlID09PSAndHV0b3InO1xuXHR9O1xuXHRzdmMuaXNBZG1pbiA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGxvYy50eXBlID09PSAnYWRtaW4nO1xuXHR9O1xuXHRzdmMudXNlcm5hbWUgPSBmdW5jdGlvbigpe1xuXHRcdHJldHVybiBsb2MudXNlcm5hbWU7XG5cdH07XG5cdHN2Yy5wYXNzd29yZCA9IGZ1bmN0aW9uKCl7XG5cdFx0cmV0dXJuIGxvYy5wYXNzd29yZDtcblx0fTtcblx0c3ZjLnVzZXJ0eXBlID0gZnVuY3Rpb24oKXtcblx0XHRyZXR1cm4gbG9jLnR5cGU7XG5cdH07XG5cblx0cmV0dXJuIHN2Yztcbn1dKVxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9

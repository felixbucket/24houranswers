var gulp = require('gulp'),
	clean = require('gulp-clean'),
	zip = require('gulp-zip'),
	concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	notify = require('gulp-notify');


var globs = {
	web: ['src/**/*', '!src/config.xml'],
	appjs: ['src/app/**/*.js', '!src/app/app-complete.js'],
	completejs: 'src/app/app-complete.js',
	config: ['src/config.xml']
};
var paths = {
	dist: {
		_: 'dist',
		web: 'dist/web',
		phonegap: 'dist/phonegap'
	},
	src: {
		_: 'src',
		app: 'src/app'
	}
};


gulp.task('build', ['build:appjs', 'build:web', 'build:phonegap']);

gulp.task('build:web', ['clean:web', 'build:appjs'], function(){
	return gulp.src(globs.web)
		.pipe(gulp.dest(paths.dist.web));
});
gulp.task('build:phonegap', ['clean:phonegap', 'build:appjs', 'build:phonegap:config'], function(){
	return gulp.src("src/**/*")
		.pipe(zip('www.zip'))
			.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(gulp.dest(paths.dist.phonegap));
});
gulp.task('build:phonegap:config', ['clean:phonegap'], function(){
	return gulp.src(globs.config)
		.pipe(gulp.dest(paths.dist.phonegap));
});

gulp.task('clean', ['clean:appjs', 'clean:web', 'clean:phonegap'], function(){
	return gulp.src(paths.dist._)
		.pipe(clean());
});
gulp.task('clean:web', function(){
	return gulp.src(paths.dist.web)
		.pipe(clean());
});
gulp.task('clean:phonegap', function(){
	return gulp.src(paths.dist.phonegap)
		.pipe(clean());
});



gulp.task('build:appjs', ['clean:appjs'], function(){
	return gulp.src(globs.appjs)
		.pipe(sourcemaps.init())
			.pipe(concat(globs.completejs))
				.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(sourcemaps.write())
			.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(gulp.dest('./')); //use relative dest since globs.completejs is already absolute
});
gulp.task('clean:appjs', function(){
	return gulp.src(globs.completejs)
		.pipe(clean());
});


gulp.task('watch', function(){
	gulp.watch(globs.appjs, ['build:appjs']);
});
